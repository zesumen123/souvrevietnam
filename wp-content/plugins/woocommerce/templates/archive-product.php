<?php

/* Template Name: PRODUCT PAGE */

get_header();

?>
<div class="row th-search px-4">
	<div class="col-4">
		<div class="mb-4">
			<div class="th-search-title">Danh mục</div>
			<?php
			$taxonomy     = 'product_cat';
			$orderby      = 'slug';
			$show_count   = 0;      // 1 for yes, 0 for no
			$pad_counts   = 0;      // 1 for yes, 0 for no
			$hierarchical = 1;      // 1 for yes, 0 for no  
			$title        = '';
			$empty        = 0;

			$args = array(
				'taxonomy'     => $taxonomy,
				'orderby'      => $orderby,
				'show_count'   => $show_count,
				'pad_counts'   => $pad_counts,
				'hierarchical' => $hierarchical,
				'title_li'     => $title,
				'hide_empty'   => $empty
			);
			echo "<div class='d-flex flex-column'>";
			$all_categories = get_categories($args);
			foreach ($all_categories as $cat) {
				if ($cat->category_parent == 0) {
					$category_id = $cat->term_id;
					echo '<div class="th-search-filter-item" href="' . get_term_link($cat->slug, 'product_cat') . '">' . $cat->name . '</div>';
				}
			}
			echo "</div>";
			?>
		</div>
		<div class="col-8 p-0 mt-4">
			<div>
				<div class="th-search-title">Hot Deals </div>
				<div class="search-slider">
					<?php
					$args = array(
						'post_type'      => 'product',
						'posts_per_page' => 10,

					);

					$query = new WP_Query($args);
					?>
					<?php
					if ($query->have_posts()) :
					?>
					<?php
						// Start the Loop.
						while ($query->have_posts()) :
							$query->the_post();
							echo "<div>";
							get_template_part('template-parts/product/single',  $query->get_post_format());
							echo "</div>";
						endwhile;
					endif;
					?>
				</div>
			</div>

		</div>
	</div>
	<div class="col-8">
		<div class="row">
			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
				'post_type'      => 'product',
				'posts_per_page' => 3,
				'paged' => $paged,
			);

			$query = new WP_Query($args);
			?>
			<?php
			if ($query->have_posts()) :
			?>
			<?php
				// Start the Loop.
				while ($query->have_posts()) :
					$query->the_post();
					echo "<div class='col-4'>";
					get_template_part('template-parts/product/single',  $query->get_post_format());
					echo "</div>";
				endwhile;

				$total_pages = $query->max_num_pages;
				if ($total_pages > 1) {

					$current_page = max(1, get_query_var('paged'));
					echo "<div class='th-search-pagination'>";
					echo paginate_links(array(
						'base' => get_pagenum_link(1) . '%_%',
						'format' => '/page/%#%',
						'current' => $current_page,
						'total' => $total_pages,
						'prev_text'    => __('Trang trước'),
						'next_text'    => __('Trang sau'),
					));
					echo "</div>";
				}
			endif;
			?>
		</div>
	</div>

</div>
<?php
get_footer();
