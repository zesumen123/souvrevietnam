<?php defined('ABSPATH') or die("No script kiddies please!");
/**
    Plugin Name: TabWoo - Extra Tabs Plugin for WooCommerce
    Plugin URI:  https://accesspressthemes.com/wordpress-plugins/tabwoo/
    Description: Extra Tabs Plugin for WooCommerce
    Version:     1.0.2
    Author:      AccessPress Themes
    Author URI:  http://accesspressthemes.com/
    Domain Path: /languages/
    Text Domain: tabwoo
*/
if (! class_exists('TW_Class')) {

    class TW_Class {

        function __construct() {
            $this->define_constants();
            $this->include_files();
        }

        public function define_constants() {

            defined( 'TW_VERSION' ) or define( 'TW_VERSION', '1.0.2' );
            defined( 'TW_TD' ) or define( 'TW_TD', 'tabwoo' );
            defined( 'TW_IMG_DIR' ) or define( 'TW_IMG_DIR', plugin_dir_url( __FILE__ ) . 'images' );
            defined( 'TW_JS_DIR' ) or define( 'TW_JS_DIR', plugin_dir_url( __FILE__ ) . 'js' );
            defined( 'TW_CSS_DIR' ) or define( 'TW_CSS_DIR', plugin_dir_url( __FILE__ ) . 'css' );
            defined( 'TW_DYNAMIC_CSS_PATH' ) or define( 'TW_DYNAMIC_CSS_PATH', plugin_dir_path( __FILE__ ) . 'css/dynamic_css' ); 
            defined( 'TW_DYNAMIC_CSS_DIR' ) or define( 'TW_DYNAMIC_CSS_DIR', plugin_dir_url( __FILE__ ) . 'css/dynamic_css' ); 
            defined( 'TW_PATH' ) or define( 'TW_PATH', plugin_dir_path( __FILE__ ) );
            defined( 'TW_URL' ) or define( 'TW_URL', plugin_dir_url( __FILE__ ) );
            
        }

        public function include_files() {
            include( TW_PATH . 'inc/admin/classes/class-tw-activation.php' );
            include( TW_PATH . 'inc/admin/classes/class-tw-library.php' );
            include( TW_PATH . 'inc/admin/classes/class-tw-enqueue.php' );
            include( TW_PATH . 'inc/admin/classes/class-tw-register-post-types.php' );
            include( TW_PATH . 'inc/admin/classes/class-tw-admin-menu.php' );
            include( TW_PATH . 'inc/admin/classes/class-tw-admin-ajax.php' );
            include( TW_PATH . '/inc/frontend/mobile-detect.php' );
            include( TW_PATH . 'inc/frontend/classes/class-tw-hooks.php' );
            include( TW_PATH . 'inc/frontend/classes/class-tw-dynamic-css.php' );

        }
    }

    $tw_obj = new TW_Class();

}