<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$tw_tab_settings = get_post_meta( $post_id, 'tw_tab_settings', true );

$enable_custom_option = (isset($tw_tab_settings['main']['custom_settings']['enable_custom_option']) && $tw_tab_settings['main']['custom_settings']['enable_custom_option'] == true) ? true : false;

// Normal
$bg_color = (isset($tw_tab_settings['main']['custom_settings']['bg_color']) && $tw_tab_settings['main']['custom_settings']['bg_color'] != '') ? $tw_tab_settings['main']['custom_settings']['bg_color'] : '';
$font_color = (isset($tw_tab_settings['main']['custom_settings']['font_color']) && $tw_tab_settings['main']['custom_settings']['font_color'] != '')?$tw_tab_settings['main']['custom_settings']['font_color'] : '';

// Hover
$bg_hover_color = (isset($tw_tab_settings['main']['custom_settings']['bg_hover_color']) && $tw_tab_settings['main']['custom_settings']['bg_hover_color'] != '') ? $tw_tab_settings['main']['custom_settings']['bg_hover_color'] : '';
$font_hover_color = (isset($tw_tab_settings['main']['custom_settings']['font_hover_color']) && $tw_tab_settings['main']['custom_settings']['font_hover_color'] != '') ? $tw_tab_settings['main']['custom_settings']['font_hover_color'] : '';

// Active
$bg_active_color = (isset($tw_tab_settings['main']['custom_settings']['bg_active_color']) && $tw_tab_settings['main']['custom_settings']['bg_active_color'] != '') ? $tw_tab_settings['main']['custom_settings']['bg_active_color'] : '';
$font_active_color = (isset($tw_tab_settings['main']['custom_settings']['font_active_color']) && $tw_tab_settings['main']['custom_settings']['font_active_color'] != '') ? $tw_tab_settings['main']['custom_settings']['font_active_color'] : '';

// OTHERES

$bg_tab_content_color  = (isset($tw_tab_settings['main']['custom_settings']['bg_tab_content_color']) && $tw_tab_settings['main']['custom_settings']['bg_tab_content_color'] != '') ? $tw_tab_settings['main']['custom_settings']['bg_tab_content_color'] : '';

$font_family = (isset($tw_tab_settings['main']['custom_settings']['font_family']) && $tw_tab_settings['main']['custom_settings']['font_family'] != '') ? $tw_tab_settings['main']['custom_settings']['font_family'] : '';

$font_size = (isset($tw_tab_settings['main']['custom_settings']['font_size']) && $tw_tab_settings['main']['custom_settings']['font_size'] != '') ? $tw_tab_settings['main']['custom_settings']['font_size'] : '';
