<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );

// Plugin Settings

$tw_settings = get_option('tw_settings', true);

$enable = isset($tw_settings['enable']) ? 'yes':'no';

$default_tab_set = (isset($tw_settings['default_tab_set']) && $tw_settings['default_tab_set'] != ' ') ? $tw_settings['default_tab_set']:'none';
$tw_tab_settings = get_post_meta( $post_id, 'tw_tab_settings', true );


$basis = (isset($tw_tab_settings['assigned_to']['basis']) && $tw_tab_settings['assigned_to']['basis'] != '') ? esc_attr($tw_tab_settings['assigned_to']['basis']):'';

$category_list = (isset($tw_tab_settings['assigned_to']['category']) && $tw_tab_settings['assigned_to']['category'] != '') ? $tw_tab_settings['assigned_to']['category']:'';

$tag_list = (isset($tw_tab_settings['assigned_to']['tag']) && $tw_tab_settings['assigned_to']['tag'] != '') ? $tw_tab_settings['assigned_to']['tag']:'';

$product_list = (isset($tw_tab_settings['assigned_to']['product']) && $tw_tab_settings['assigned_to']['product'] != '') ? $tw_tab_settings['assigned_to']['product']:array();


// Main Settings:

$display_format = (isset($tw_tab_settings['main']['general_settings']['display_format_type']) && $tw_tab_settings['main']['general_settings']['display_format_type'] != '')?$tw_tab_settings['main']['general_settings']['display_format_type']:'show_title_only';

$icon_position  = (isset($tw_tab_settings['main']['general_settings']['icon_position']) && $tw_tab_settings['main']['general_settings']['icon_position'] != '')?$tw_tab_settings['main']['general_settings']['icon_position']:'left';

$show_tab_on = (isset($tw_tab_settings['main']['general_settings']['show_tab_on']) && $tw_tab_settings['main']['general_settings']['show_tab_on'] != '')?$tw_tab_settings['main']['general_settings']['show_tab_on']:'on_click';


$column = (isset($tw_tab_settings['main']['general_settings']['column']) && $tw_tab_settings['main']['general_settings']['column'] != '')?intval($tw_tab_settings['main']['general_settings']['column']):'1';

$column_width = (isset($tw_tab_settings['main']['general_settings']['column_width']) && $tw_tab_settings['main']['general_settings']['column_width'] != '')?intval($tw_tab_settings['main']['general_settings']['column_width']):'150';

$enable_accordion = (isset($tw_tab_settings['main']['general_settings']['enable_accordion']) && $tw_tab_settings['main']['general_settings']['enable_accordion'] == true)? 'true' :'false';

// Display 
$orientation = (isset($tw_tab_settings['main']['display_settings']['orientation_type']) && $tw_tab_settings['main']['display_settings']['orientation_type'] == 'vertical') ? 'vertical' : 'horizontal';


$orientation_class = 'tw-tab-orientation-'.$orientation;

$h_template = (isset($tw_tab_settings['main']['display_settings']['h_template']) && $tw_tab_settings['main']['display_settings']['h_template'] !='') ? $tw_tab_settings['main']['display_settings']['h_template'] : 'template1';

$v_template = (isset($tw_tab_settings['main']['display_settings']['v_template']) && $tw_tab_settings['main']['display_settings']['v_template'] !='') ? $tw_tab_settings['main']['display_settings']['v_template'] : 'template1';

$template_class = ($orientation == 'vertical') ? 'tw-tab-template-'.$v_template.' tw-tab-v-template-'.$v_template : 'tw-tab-template-'.$h_template.' tw-tab-h-template-'.$h_template ;


//include(TW_PATH . '/inc/frontend/demo_variable.php');