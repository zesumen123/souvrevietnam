<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );
switch ( $basis ) {
    case 'category':

        if ( $category_list != '' && has_term( $category_list, 'product_cat', $product_id ) && $tab_priority < 1 ) {
            $tab_priority = 1;
            include( TW_PATH . '/inc/frontend/each_tab.php' );
        }

        break;
    case 'tag':

        if ( $tag_list != '' && has_term( $tag_list, 'product_tag', $product_id ) && $tab_priority < 2) {
            $tab_priority = 2;
            include( TW_PATH . '/inc/frontend/each_tab.php' );
        }

        break;
    case 'product':

        if(in_array($product_id, $product_list) && $tab_priority < 3){
            $tab_priority = 3;
            include( TW_PATH . '/inc/frontend/each_tab.php' );
        }

        break;
}
