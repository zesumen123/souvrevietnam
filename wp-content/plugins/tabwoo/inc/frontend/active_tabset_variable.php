<?php

defined('ABSPATH') or die('No script kiddies please!!');

$applied_post_id = $post_id;
$applied_orientation_class = $orientation_class;
$applied_template_class = $template_class;
$applied_enable_accordion = $enable_accordion;
$applied_enable_accordion_class = ($applied_enable_accordion=='true')?'tw-accordion_enabled':'';

$applied_width_class = ($column == '3')?'tw-width-full':'';


$applied_tab_width = ($column == '2') ? $column_width.'px' :'';