<?php defined('ABSPATH') or die('No script kiddies please!!');

$tabs = $default_tabs;
$priority = 10 ;
$applied_post_id = $post_id;

foreach ($tw_tab_settings['tab'] as $key => $value) {

    $tab_label = isset($value['tab_label']) && $value['tab_label'] != " " ? $value['tab_label'] : "Custom Tab";

    if ( $key == 'description' || $key == 'additional_information' || $key == 'reviews' ) {

        $enable_tab = isset( $value['enable_tab'] ) && $value['enable_tab'] == "yes" ? true : false ; 
        $tab_label = isset( $value['tab_label']) && $value['tab_label'] != " " ? $value['tab_label'] : "Additional Information" ;

        switch($key) {

            case 'description':

                $tab_label = isset($value['tab_label']) && $value['tab_label'] != " " ? $value['tab_label'] : "Description";

                $tabs['description']['title'] = __( $tab_label );
                $tabs['description']['priority'] = $priority; 

            break;
            case 'additional_information':

                if( $product->has_attributes() || $product->has_dimensions() || $product->has_weight() ) { 
                    $tab_label = isset($value['tab_label']) && $value['tab_label'] != " " ? $value['tab_label'] : "Additional Information";

                    $tabs['additional_information']['title'] = __( $tab_label );
                    $tabs['additional_information']['priority'] = $priority;
                }

            break;
            case 'reviews':

                $tab_label = isset($value['tab_label']) && $value['tab_label'] != " " ? $value['tab_label'] : "Review";

                $tabs['reviews']['title'] = __( $tab_label ); 
                $tabs['reviews']['priority'] = $priority; 

            break;
        }

    } else {

        $tabs[$key] = array(

            'title' => __( $tab_label, TW_TD ),
            'callback' => array( $this->tw_lib, 'tw_new_product_tab_content'),
            'priority' => $priority,
            'tab_set_id' => $post_id,
            'product_id' => $product_id,
            'orientation' => $orientation_class,
            'template' => $template_class

        );

    }

    include(TW_PATH . 'inc/frontend/custom_tabs/tab_title.php');

    $priority = $priority + 10 ;
}

include(TW_PATH .'inc/frontend/active_tabset_variable.php');