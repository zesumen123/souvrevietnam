<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );

add_filter( 'woocommerce_product_'.$key.'_tab_title', function () use ( $tw_tab_settings, $value, $post_id) { 

	$tab_label = (isset($value['tab_label']) && $value['tab_label'] != '') ? $value['tab_label'] : '';
	$tab_icon_type = (isset($value['icon_type']) && $value['icon_type'] != '') ? $value['icon_type']:'available_icon';
	$icon_code = (isset($value['icon']['code']) && $value['icon']['code'] != '') ? $value['icon']['code']:'';
	$icon_url = (isset($value['icon']['url']) && $value['icon']['url'] != '') ? $value['icon']['url']:'';
	$icon_width = (isset($value['icon']['width']) && $value['icon']['width'] != '') ? $value['icon']['width']:'';
	$icon_height = (isset($value['icon']['height']) && $value['icon']['height'] != '') ? $value['icon']['height']:'';
	$pre = '';
	$post = ''; 
	if($icon_code != ''){
		$icon_name = explode( '|', $icon_code);
		$pre = $icon_name[0];
		$post = $icon_name[1]; 
	}
	$display_format = (isset($tw_tab_settings['main']['general_settings']['display_format_type']) && $tw_tab_settings['main']['general_settings']['display_format_type'] != '')?$tw_tab_settings['main']['general_settings']['display_format_type']:'show_title_only';

	$icon_position = (isset($tw_tab_settings['main']['general_settings']['icon_position']) && $tw_tab_settings['main']['general_settings']['icon_position'] != '')?$tw_tab_settings['main']['general_settings']['icon_position']:'left';

	$tab_label= '<span class="tw-label" data-tab-id="'.esc_attr($post_id).'">'. esc_attr($tab_label).'</span>';

	switch ( $display_format ) {
		
        case 'show_title_only':

			return $tab_label;
            
            break;

        case 'show_both':
        	if ($tab_icon_type == 'available_icon'){
        		$icon = '<span class="tw-icon '. esc_attr($pre) . ' ' . esc_attr($post) .'"> </span>';
        	} else {
        		$style = 'style= "height:'.esc_attr($icon_height).'px; '. 'width:'.esc_attr($icon_width).'px;"';
        		$icon = '<span class="tw-icon"><img src="'.esc_attr($icon_url).'" '.esc_attr($style).'></span>';
        	}
    		if ( (isset ($icon_name) && $icon_name[0] !='') || esc_attr($icon_url)!=''  ) {

    			return '<div class="tw-text-icon-both tw-icon-left">'.$icon.' '. $tab_label. '</div>';
			} else {
				return $tab_label;
			}
        
            break;

        case 'show_icon_only':

        	if ($tab_icon_type == 'available_icon'){
        		$icon = '<span class="tw-icon '. esc_attr($pre) . ' ' . esc_attr($post) .'"> </span>';
        	} else {
        		$style= 'style= "height:'.esc_attr($icon_height).'px; '. 'width:'.esc_attr($icon_width).'px;"';
        		$icon = '<span class="tw-icon"><img src="'.esc_attr($icon_url).'" '.esc_attr($style).'></span>';
        	}

            if ( isset ($icon_name) && $icon_name[0] !='' || $icon_url !=''  ) {
				return $icon;
			} else {
				return $tab_label;
			}

            break;
    }
    
});