<?php defined('ABSPATH') or die('No script kiddies please!!'); ?>

<div class = "tw_tab_content_download" >
<?php 

if (isset($single_product_settings['download']) && !empty($single_product_settings['download'])) { 
    foreach ($single_product_settings['download'] as $items => $item) { ?>

        <div class="tw-downloadable-file-preview">
            <div class="tw-setting-image">
                <?php 
                    $filename = basename($item);
                ?>
                <a class="tw-downloadable-file" download="<?php echo esc_attr( $filename ); ?> ">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    <span> <?php echo esc_attr( $filename ); ?> </span>
                </a>
               
            </div>
        </div>
        <?php
    }
}
else if ( isset($tw_tab_settings['tab'][$key]['download']) && !empty($tw_tab_settings['tab'][$key]['download']) ) {

    foreach ($tw_tab_settings['tab'][$key]['download']as $items => $item) { ?>

        <div class="tw-downloadable-file-preview">
            <div class="tw-setting-image">
                <?php 
                    $filename = basename($item);
                ?>
                <a class="tw-downloadable-file" download="<?php echo esc_attr( $filename ); ?> ">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    <span> <?php echo esc_attr( $filename ); ?> </span>
                </a>
           </div>
        </div>
        <?php
    }
} 
   
?>

</div>
