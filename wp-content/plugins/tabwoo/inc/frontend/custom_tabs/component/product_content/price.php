<?php defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$product_price = isset($tw_tab_settings['tab']['product']['product_price']) && $tw_tab_settings['tab']['product']['product_price']!= '' ? $tw_tab_settings['tab']['product']['product_price'] : 'actual_price' ;

?>
<div class="tw-price">
    <?php
    if(esc_attr($product_price) == 'sale_price') {
         woocommerce_template_loop_price();

    } else {
        $product = new WC_Product( $product_item_id );
        echo wc_price( $product -> get_price() );
    }
    ?>
</div>