<?php defined( 'ABSPATH' ) or die( "No script kiddies please!" ); 

$image_size = isset($tw_tab_settings['tab'][$key]['product']['carousel']['image_size']) && $tw_tab_settings['tab'][$key]['product']['carousel']['image_size']!= '' ? $tw_tab_settings['tab'][$key]['product']['carousel']['image_size'] : 'thumbnail' ;

$thumbnail_url = get_the_post_thumbnail_url( $product_item_id, esc_attr($image_size) );

$link_option = isset($tw_tab_settings['tab'][$key]['product']['carousel']['link_option']) && $tw_tab_settings['tab'][$key]['product']['carousel']['link_option']== 'same_window' ? 'same_window' : 'new_window' ;


$target = ( esc_attr($link_option) =='new_window' )? 'target="_blank"' : '' ;

?>

<div class="tw-image">
    <?php

    if ( has_post_thumbnail( $product_item_id ) ) { ?>
        <a href="<?php echo get_permalink( $product_item_id ); ?>" target="_blank">
            <img src="<?php echo $thumbnail_url; ?>">
        </a>
        <?php
    }
    ?>
</div>