<?php defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$link_option = isset($tw_tab_settings['tab'][$key]['product']['carousel']['link_option']) && $tw_tab_settings['tab'][$key]['product']['carousel']['link_option']== 'same_window' ? 'same_window' : 'new_window' ;

$target = ( $link_option =='new_window' )? 'target="_blank"' : '' ;

?>
<div class="tw-buton-one-wrapper tw-button">
	<?php woocommerce_template_loop_add_to_cart() ; ?>
</div>

<div class="tw-buton-two-wrapper tw-button" >
	<a class="tw-button-design" href="<?php the_permalink(); ?>" <?php echo $target; ?>>
		<?php echo esc_attr( 'Detail' ); ?>
	</a>
</div>