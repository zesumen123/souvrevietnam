<?php defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$show_link_title = isset($tw_tab_settings['tab'][$key]['product']['carousel']['show_link_title']) && $tw_tab_settings['tab'][$key]['product']['carousel']['show_link_title']== 'true' ? true : false ;

$link_option = isset($tw_tab_settings['tab'][$key]['product']['carousel']['link_option']) && $tw_tab_settings['tab'][$key]['product']['carousel']['link_option']== 'same_window' ? 'same_window' : 'new_window' ;

$target = ( $link_option =='new_window' )? 'target="_blank"' : '' ;

?>
<div class="tw-title">
    <?php
    if ( $show_link_title ) { ?>
        <a href="<?php the_permalink(); ?>" <?php echo esc_attr($target); ?>>
            <?php the_title(); ?></a><?php
    } else {
        the_title();
    }
    ?>
</div>