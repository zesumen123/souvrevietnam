<?php defined('ABSPATH') or die('No script kiddies please!!'); 

if (isset($single_product_settings['gallery']) && !empty($single_product_settings['gallery'])){
	$i=0;
	foreach ($single_product_settings['gallery'] as $image => $img) { 
		$i++;
    }
} elseif (isset($tw_tab_settings['tab'][$key]['gallery']) && !empty($tw_tab_settings['tab'][$key]['gallery'])){
	$i=0;
    foreach ($tw_tab_settings['tab'][$key]['gallery']as $image => $img) { 
		$i++;
    }
}

if ($i<=3) {
	$class = "tw-three";
} else {
	$class = " ";
}

?>

<div class = "tw_tab_content_photo_gallery <?php echo $class; ?>" >

<?php 

	if (isset($single_product_settings['gallery']) && !empty($single_product_settings['gallery'])){
		$i=1;
		foreach ($single_product_settings['gallery'] as $image => $img) { ?>
	        <div class="tw-pro-gallery-wrapper" >
	            <img class="tw-picture-image" src="<?php echo esc_attr( $img ); ?>" alt="" >
	        </div>
	        <?php
	    }
	}elseif (isset($tw_tab_settings['tab'][$key]['gallery']) && !empty($tw_tab_settings['tab'][$key]['gallery'])){
		$i=1;
	    foreach ($tw_tab_settings['tab'][$key]['gallery']as $image => $img) { ?>
	        <div class="tw-pro-gallery-wrapper" >
	            <img class="tw-picture-image" src="<?php echo esc_attr( $img ); ?>" alt="">
	        </div>
	        <?php
	    }
	}
 ?>

</div>
