<?php  	defined('ABSPATH') or die('No script kiddies please!!'); ?>

<div class = "tw_tab_content_faq" >
<?php 

if (isset($single_product_settings['faq']['question']) && !empty($single_product_settings['faq']['question'])) { 
	$array_value = 0;
    foreach ($single_product_settings['faq']['question'] as $option) {
        ?>
        <div class="tw-faq-wrap">

            <?php 
            	$question = (!empty($option) ? $option : ""  );
              	$answer = (!empty($single_product_settings['faq']['answer'][$array_value]) ? $single_product_settings['faq']['answer'][$array_value] : "" ) ;
            ?>

            <div class="tw-faq-question-wrapper">
                <span><i class="fa fa-chevron-down"></i> </span>
				<span><?php echo esc_attr($question) ?></span>
			</div>

			<div class="tw-faq-answer-wrapper" style="display:none;">
				<?php echo esc_attr($answer) ?>
			</div>

        </div>
        <?php
        $array_value++;
    }
} else {
	if (isset($tw_tab_settings['tab'][$key]['faq']['question']) && !empty($tw_tab_settings['tab'][$key]['faq']['question'])) { 
		$array_value = 0;
        foreach ($tw_tab_settings['tab'][$key]['faq']['question'] as $option) {
            ?>
            <div class="tw-faq-wrap">

                <?php
                	$question = (!empty($option) ? $option : "" ); 
                	$answer = (!empty($tw_tab_settings['tab'][$key]['faq']['answer'][$array_value]) ? $tw_tab_settings['tab'][$key]['faq']['answer'][$array_value] : "" ); 
                ?>

                <div class="tw-faq-question-wrapper">
				    <span> <i class="fa fa-chevron-down"></i> </span>
                    <span><?php echo esc_attr($question) ?></span>
				</div>

				<div class="tw-faq-answer-wrapper" style="display:none;">
					<?php echo esc_attr($answer) ?>
				</div>

            </div>
            <?php
            $array_value++;
        }
	} 
}

	
?>

</div>
