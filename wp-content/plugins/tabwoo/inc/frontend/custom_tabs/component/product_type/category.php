<?php
defined('ABSPATH') or die('No script kiddies please!!');

$tw_product_layout = isset($tw_tab_settings['tab'][$key]['product']['tw_product_layout']) && $tw_tab_settings['tab'][$key]['product']['tw_product_layout']!="" ? $tw_tab_settings['tab'][$key]['product']['tw_product_layout'] : "list" ;
$tw_slide_column = isset($tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_column']) && $tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_column']!="" ? $tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_column'] : "3" ;
$tw_slide_width = isset($tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_width']) && $tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_width']!="" ? $tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_width'] : "350" ;
$tw_nav_controls = isset($tw_tab_settings['tab'][$key]['product']['carousel']['tw_nav_controls']) && $tw_tab_settings['tab'][$key]['product']['carousel']['tw_nav_controls']!="" ? $tw_tab_settings['tab'][$key]['product']['carousel']['tw_nav_controls'] : "true" ;
$tw_slide_auto = isset($tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_auto']) && $tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_auto']!="" ? $tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_auto'] : "true" ;
$tw_slide_speed = isset($tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_speed']) && $tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_speed']!="" ? $tw_tab_settings['tab'][$key]['product']['carousel']['tw_slide_speed'] : "1000" ;
$desktop_column = isset($tw_tab_settings['tab'][$key]['product']['grid']['desktop_column']) && $tw_tab_settings['tab'][$key]['product']['grid']['desktop_column']!="" ? $tw_tab_settings['tab'][$key]['product']['grid']['desktop_column'] : "3" ;
$tablet_column = isset($tw_tab_settings['tab'][$key]['product']['grid']['tablet_column']) && $tw_tab_settings['tab'][$key]['product']['grid']['tablet_column']!="" ? $tw_tab_settings['tab'][$key]['product']['grid']['tablet_column'] : "2" ;
$mobile_column = isset($tw_tab_settings['tab'][$key]['product']['grid']['mobile_column']) && $tw_tab_settings['tab'][$key]['product']['grid']['mobile_column']!="" ? $tw_tab_settings['tab'][$key]['product']['grid']['mobile_column'] : "1" ;
$img_position = isset($tw_tab_settings['tab'][$key]['product']['list']['img_position']) && $tw_tab_settings['tab'][$key]['product']['list']['img_position']!="" ? $tw_tab_settings['tab'][$key]['product']['list']['img_position'] : "left" ;

if ( $tw_product_layout == 'list' ) {

    if ( $img_position == 'left' ) {
        $tw_layout_class = 'tw-list tw-left-image';
    } else {
        $tw_layout_class = 'tw-list tw-right-image';
    }

} elseif ($tw_product_layout == 'grid' ) {

    global $tw_mobile_detector;
    $desktop = esc_attr( $desktop_column );
    $mobile = esc_attr( $mobile_column );
    $tablet = esc_attr( $tablet_column );

    if ( $tw_mobile_detector -> isMobile() && ! $tw_mobile_detector -> isTablet() ) {
        $tw_layout_class = ' tw-grid' . ' tw-mobile-col-' . $mobile;
    } else if ( $tw_mobile_detector -> isTablet() ) {
        $tw_layout_class = ' tw-grid' . ' tw-tablet-col-' . $tablet;
    } else {
        $tw_layout_class = ' tw-grid' . ' tw-desktop-col-' . $desktop;
    }

} else {

    $tw_layout_class = 'tw-carousel';

}

?>

<div class = "tw_tab_content_products" >
	<div class="<?php echo ($tw_nav_controls == 'true') ? 'tw-controls-true':'tw-controls-false';?> tw-main-product-wrapper" data-id="tw_<?php echo rand( 1111111, 9999999 ); ?>" >
	    <div class="<?php echo $tw_layout_class ?>" 
        <?php if ( $tw_product_layout == 'carousel' ) { ?>
            data-column = "<?php echo esc_attr( $tw_slide_column ); ?>"
            data-controls = "<?php echo esc_attr( $tw_nav_controls ); ?>"
            data-auto = "<?php echo esc_attr( $tw_slide_auto ); ?>"
            data-speed = "<?php echo esc_attr( $tw_slide_speed ); ?>"
            data-pager = "true"
            data-width = "<?php echo esc_attr($tw_slide_width); ?>"
            <?php
        }
        ?>>
        <?php include(TW_PATH . '/inc/frontend/custom_tabs/component/product_query/product_query.php'); ?>
        </div> 
	</div>
</div>