<?php 
defined('ABSPATH') or die('No script kiddies please!!'); 

$content = (isset($tw_tab_settings['tab'][$key]['html_text']) && $tw_tab_settings['tab'][$key]['html_text'] != '')?$tw_tab_settings['tab'][$key]['html_text']:'';

if(isset($single_product_settings) && !empty($single_product_settings) ){
	
	$content = (isset($single_product_settings['html_text']) && $single_product_settings['html_text'] != '') ? $single_product_settings['html_text']: $content;
}

?>

<div class = "tw_tab_content_editor" >

<?php 

echo do_shortcode( $content);
   
?>

</div>