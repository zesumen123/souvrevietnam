<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$latitude = isset($tw_tab_settings['tab'][$key]['gmap']['latitude']) && $tw_tab_settings['tab'][$key]['gmap']['latitude']!=""? $tw_tab_settings['tab'][$key]['gmap']['latitude'] :"";
$longitude = isset($tw_tab_settings['tab'][$key]['gmap']['longitude']) && $tw_tab_settings['tab'][$key]['gmap']['longitude']!=""? $tw_tab_settings['tab'][$key]['gmap']['longitude'] :"";
$zoom_level = isset($tw_tab_settings['tab'][$key]['gmap']['zoom_level']) && $tw_tab_settings['tab'][$key]['gmap']['zoom_level']!=""? $tw_tab_settings['tab'][$key]['gmap']['zoom_level'] :"";


if(isset($single_product_settings) && !empty($single_product_settings) ){
	
	$latitude = (isset($single_product_settings['latitude']) && $single_product_settings['latitude'] != '') ? esc_attr($single_product_settings['latitude']): esc_attr($latitude);

	$longitude = (isset($single_product_settings['longitude']) && $single_product_settings['longitude'] != '') ? esc_attr($single_product_settings['longitude']): esc_attr($longitude);

	$zoom_level = (isset($single_product_settings['zoom_level']) && $single_product_settings['zoom_level'] != '') ? esc_attr($single_product_settings['zoom_level']): esc_attr($zoom_level);
}

?>

<div class = "tw_tab_content_map" >

	<div class="tw-google-map" id="tw-google-map-<?php echo $key;?>" data-latitude="<?php echo esc_attr($latitude); ?>" data-longitude="<?php echo esc_attr($longitude); ?>" data-zoomlevel="<?php echo esc_attr($zoom_level)?>"></div>

</div>