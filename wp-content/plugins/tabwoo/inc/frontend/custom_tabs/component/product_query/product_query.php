<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$select_post_taxonomy = isset($tw_tab_settings['tab'][$key]['product']['select_post_taxonomy']) && $tw_tab_settings['tab'][$key]['product']['select_post_taxonomy']!="" ? $tw_tab_settings['tab'][$key]['product']['select_post_taxonomy'] : "" ;
$simple_taxonomy_terms = isset($tw_tab_settings['tab'][$key]['product']['simple_taxonomy_terms']) && $tw_tab_settings['tab'][$key]['product']['simple_taxonomy_terms']!="" ? $tw_tab_settings['tab'][$key]['product']['simple_taxonomy_terms'] : "" ;
$tw_select_orderby = isset($tw_tab_settings['tab'][$key]['product']['tw_select_orderby']) && $tw_tab_settings['tab'][$key]['product']['tw_select_orderby']!="" ? $tw_tab_settings['tab'][$key]['product']['tw_select_orderby'] : "none" ;
$tw_select_order = isset($tw_tab_settings['tab'][$key]['product']['tw_select_order']) && $tw_tab_settings['tab'][$key]['product']['tw_select_order']!="" ? $tw_tab_settings['tab'][$key]['product']['tw_select_order'] : "ASC" ;
$tw_post_number = isset($tw_tab_settings['tab'][$key]['product']['tw_post_number']) && $tw_tab_settings['tab'][$key]['product']['tw_post_number']!="" ? $tw_tab_settings['tab'][$key]['product']['tw_post_number'] : "10" ;

$tax = $select_post_taxonomy;

if ( $simple_taxonomy_terms == '' ) {
    $terms = get_terms( $tax, array( 'hide_empty' => false ) );
    $term_ids = wp_list_pluck( $terms, 'term_id' );
    $id = implode( ", ", array_keys( $term_ids ) );
    $tax_query = array( array(
        'taxonomy' => $tax,
        'field' => 'term_id',
        'terms' => array( $id )
    ), );
} else {
    $simple_term = $simple_taxonomy_terms;
    $tax_query = array( array(
        'taxonomy' => $tax,
        'field' => 'term_id',
        'terms' => $simple_term
    ), );
}

$args = array(
    'post_type' => 'product',
    'orderby' => $tw_select_orderby,
    'order' => $tw_select_order,
    'posts_per_page' => $tw_post_number,
    'post_status' => 'publish'
);
if ( !empty( $tax_query ) ) {
    $args[ 'tax_query' ] = $tax_query;
}

$query = new WP_Query( $args );

$rowCount = $query -> found_posts;

if ( $query -> have_posts() ) {
    $tw_row = 1;
    while ( $query -> have_posts() ) {
        $query -> the_post();

        $product_item_id = get_the_ID ();
        include(TW_PATH . 'inc/frontend/custom_tabs/component/product_layout/tw_list.php');
    }
} else {
     _e( 'No post found', tw_TD );
}
wp_reset_postdata();

?>