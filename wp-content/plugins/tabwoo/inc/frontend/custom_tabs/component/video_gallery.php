<?php defined('ABSPATH') or die('No script kiddies please!!'); 

$video_type = isset($tw_tab_settings['tab'][$key]['video']['type']) && $tw_tab_settings['tab'][$key]['video']['type']!="" ? $tw_tab_settings['tab'][$key]['video']['type'] :"";
$video_youtube = isset($tw_tab_settings['tab'][$key]['video']['youtube']) && $tw_tab_settings['tab'][$key]['video']['youtube']!="" ? $tw_tab_settings['tab'][$key]['video']['youtube'] :"";
$video_viemo = isset($tw_tab_settings['tab'][$key]['video']['viemo']) && $tw_tab_settings['tab'][$key]['video']['viemo']!="" ? $tw_tab_settings['tab'][$key]['video']['viemo'] :"";

if(isset($single_product_settings['video']) && !empty($single_product_settings['video']) ){
	$video_type = isset($single_product_settings['video']['type']) && $single_product_settings['video']['type']!="" ? $single_product_settings['video']['type'] :$video_type ;
	$video_youtube = isset($single_product_settings['video']['youtube']) && $single_product_settings['video']['youtube']!="" ? $single_product_settings['video']['youtube'] :"$video_youtube";
	$video_viemo = isset($single_product_settings['video']['viemo']) && $single_product_settings['video']['viemo']!="" ? $single_product_settings['video']['viemo'] :"$video_viemo";
}

if (isset($video_type) && $video_type == 'youtube') { 
	?>
	<iframe src="<?php echo esc_url($video_youtube) ?>" ></iframe>
	<?php
} else { ?>
	<iframe src="<?php echo esc_url($video_viemo) ?>" ></iframe>
	<?php 
}