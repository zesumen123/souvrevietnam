<?php defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$show_content = isset($tw_tab_settings['tab'][$key]['product']['carousel']['show_content']) && $tw_tab_settings['tab'][$key]['product']['carousel']['show_content'] == true ? true : false ;
$excerpt_length = isset($tw_tab_settings['tab'][$key]['product']['carousel']['excerpt_length']) && $tw_tab_settings['tab'][$key]['product']['carousel']['excerpt_length'] != '' ? $tw_tab_settings['tab'][$key]['product']['carousel']['excerpt_length'] : 30 ;

?>
<div class="tw-list tw-inner-wrap" data-post_id="<?php echo $product_item_id; ?>">

	<div class="tw-image-wrap">
		<div class="tw-image-second-container">
			<?php include(TW_PATH . 'inc/frontend/custom_tabs/component/product_content/image.php'); ?>
		</div>
	</div>

	<div class="tw-product-content-wrap">
	    <div class="tw-inner-wrap-contain">
	        <?php
	        $tw_fetch_category = $this->tw_fetch_category($post_id, $product_item_id, $select_post_taxonomy);
	        $tw_fetch_content = $this->tw_fetch_content($product_item_id, $excerpt_length);
	        ?>
	        <div class="tw-top-wrap">
				<?php echo $tw_fetch_category; ?>
			</div>
			<?php include (TW_PATH . '/inc/frontend/custom_tabs/component/product_content/title.php'); ?>
		</div>

		<div class="tw-details-wrap"> <?php 
			if ($show_content == 'true') { ?>
				<div class="tw-content"> 
					<?php echo $tw_fetch_content; ?>
				</div>
				<?php
			}

			include (TW_PATH . '/inc/frontend/custom_tabs/component/product_content/price.php');
		    include (TW_PATH . '/inc/frontend/custom_tabs/component/product_content/review.php');
			?>

			<div class="tw-buttons-collection tw-clearfix"> 
				<?php include (TW_PATH . '/inc/frontend/custom_tabs/component/product_content/button.php'); ?>
			</div>

		</div>
	</div>

</div>