<?php defined('ABSPATH') or die('No script kiddies please!!'); 

$ex_shortcode = isset($tw_tab_settings['tab'][$key]['ex_shortcode']) && $tw_tab_settings['tab'][$key]['ex_shortcode']!="" ? $tw_tab_settings['tab'][$key]['ex_shortcode'] :"";


if(isset($single_product_settings) && !empty($single_product_settings) ){
	
	$ex_shortcode = (isset($single_product_settings['ex_shortcode']) && $single_product_settings['ex_shortcode'] != '') ? ($single_product_settings['ex_shortcode']): ($ex_shortcode);
}

?>

<div class = "tw_tab_content_external_shortcode" >
<?php 

echo do_shortcode($ex_shortcode);

?>

</div>
