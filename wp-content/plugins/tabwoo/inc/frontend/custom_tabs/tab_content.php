<?php defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$post_id = $tab['tab_set_id'];

$product_id = $tab['product_id'];

$orientation = $tab['orientation'];

$template = $tab['template'];

$tw_tab_settings = get_post_meta( $post_id, 'tw_tab_settings', true );

$tab_type = (isset($tw_tab_settings['tab'][$key]['default_tab_name']) && $tw_tab_settings['tab'][$key]['default_tab_name'] != '')?$tw_tab_settings['tab'][$key]['default_tab_name']:'descp'; 

$tab_label = (isset($tw_tab_settings['tab'][$key]['tab_label']) && $tw_tab_settings['tab'][$key]['tab_label'] != '')?$tw_tab_settings['tab'][$key]['tab_label']:'';

$enable_desc = (isset($tw_tab_settings['tab'][$key]['enable_desc']) && $tw_tab_settings['tab'][$key]['enable_desc'] == true)?true:false;
$tab_desc = (isset($tw_tab_settings['tab'][$key]['description']) && $tw_tab_settings['tab'][$key]['description'] != '')?$tw_tab_settings['tab'][$key]['description']:'';

$tab_icon_type = (isset($tw_tab_settings['tab'][$key]['icon_type']) && $tw_tab_settings['tab'][$key]['icon_type'] != '')?$tw_tab_settings['tab'][$key]['icon_type']:'';

$icon_code = (isset($tw_tab_settings['tab'][$key]['icon']['code']) && $tw_tab_settings['tab'][$key]['icon']['code'] != '')?$tw_tab_settings['tab'][$key]['icon']['code']:'';

$icon_url = (isset($tw_tab_settings['tab'][$key]['icon']['url']) && $tw_tab_settings['tab'][$key]['icon']['url'] != '')?$tw_tab_settings['tab'][$key]['icon']['url']:'';

$icon_width = (isset($tw_tab_settings['tab'][$key]['icon']['width']) && $tw_tab_settings['tab'][$key]['icon']['width'] != '')?$tw_tab_settings['tab'][$key]['icon']['width']:'';

$icon_height = (isset($tw_tab_settings['tab'][$key]['icon']['height']) && $tw_tab_settings['tab'][$key]['icon']['height'] != '')?$tw_tab_settings['tab'][$key]['icon']['height']:'';

$tab_components_type = (isset($tw_tab_settings['tab'][$key]['components']) && $tw_tab_settings['tab'][$key]['components'] != '')?esc_attr($tw_tab_settings['tab'][$key]['components']):'editor';

$single_product_settings = get_post_meta($product_id, 'single_product_settings', true);

$custom_link_url = '';
$link_target = '';

if( $tab_components_type == 'custom_link' ){

	$custom_link_url = (isset($tw_tab_settings['tab'][$key]['clink']['custom_link_url']) && $tw_tab_settings['tab'][$key]['clink']['custom_link_url'] != '')?$tw_tab_settings['tab'][$key]['clink']['custom_link_url']:'';
	$link_target = (isset($tw_tab_settings['tab'][$key]['clink']['custom_link_target']) && $tw_tab_settings['tab'][$key]['clink']['custom_link_target'] != '')?esc_attr($tw_tab_settings['tab'][$key]['clink']['custom_link_target']):'';

}

?>

<div class = "tw_tab_content_main_wrapper tw_<?php echo esc_attr($tab_components_type); ?>" data-custom-link-url="<?php echo esc_attr($custom_link_url); ?>" data-custom-link-target="<?php echo esc_attr($link_target); ?>">

	<?php

	switch ($tab_components_type) {

		case 'editor':

		include(TW_PATH. 'inc/frontend/custom_tabs/component/editor.php');

		break;
		case 'external_shortcode':

		include(TW_PATH. 'inc/frontend/custom_tabs/component/external_shortcode.php');

		break;
		case 'product':

		include(TW_PATH. 'inc/frontend/custom_tabs/component/products.php');

		break;
		case 'faq':

		include(TW_PATH. 'inc/frontend/custom_tabs/component/faq.php');

		break;
		case 'photo_gallery':

		include(TW_PATH. 'inc/frontend/custom_tabs/component/photo_gallery.php');

		break;
		case 'video_gallery':

		include(TW_PATH. 'inc/frontend/custom_tabs/component/video_gallery.php');

		break;
		case 'custom_link':

		include(TW_PATH. 'inc/frontend/custom_tabs/component/custom_link.php');

		break;
		case 'map':

		include(TW_PATH. 'inc/frontend/custom_tabs/component/map.php');

		break;
		case 'download':
		include(TW_PATH. 'inc/frontend/custom_tabs/component/download.php');

		break;
		default:
		break;
	}
	?>

</div>