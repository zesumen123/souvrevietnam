<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );

// ON HOVER
$content.='.woocommerce div.product .tw-tab-template-template5 .woocommerce-tabs ul.tabs li > a:hover{
	color: '.$bg_hover_color .';
	
}';

// ON ACTIVE
$content.='.woocommerce div.product .tw-tab-template-template5 .woocommerce-tabs ul.tabs li.active > a {
	color: '.$font_active_color.';
	border: 1px solid '.$font_active_color.';
	
}';

// NORMAL 
$content.='.woocommerce div.product .tw-tab-template-template5 .woocommerce-tabs ul.tabs li > a {
	color:'. $font_color.';
    font-size: '.$font_size .';
}';

// FONT FAMILY
$content.='.tw-tab-template-template5 {
	font-family: '. $font_family .';
}';