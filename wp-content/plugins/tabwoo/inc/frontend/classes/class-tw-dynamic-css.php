<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!!' );

if ( !class_exists( 'TW_Dynamic_Css' ) ) {

    class TW_Dynamic_Css{

        public function __construct() {
            add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_dynamic_css' ), 99);
        }

        public function enqueue_dynamic_css() {
            wp_enqueue_style( 'tw-frontend-style', TW_CSS_DIR . '/tw-frontend.css', TW_VERSION, true);

            if ( 'file' == $this->addCSSMode() ) {
                wp_enqueue_style( 'TW-dynamic-css', $this->file( 'uri') );
            }
        }

        public function addCSSMode(){
            $mode = ( $this->can_write() && $this->make_css() ) ? 'file' : 'inline';
            // Does again if the file exists.
            if ( 'file' == $mode ) {
                $mode = ( file_exists( $this->file( 'path' ) ) ) ? 'file' : 'inline';
            }
            return $mode;
        }

        public function can_write(){

            global $wp_filesystem;
            // Initialize the Wordpress filesystem.
            if ( empty( $wp_filesystem ) ) {
                // We will probably need to load this file
                require_once( ABSPATH . '/wp-admin/includes/file.php' );
                WP_Filesystem();  // Initial WP file system
            }

            // TW_DYNAMIC_CSS_PATH; //dir path
            //TW_DYNAMIC_CSS_DIR; //url
            $upload_directory =  TW_DYNAMIC_CSS_PATH;
            $folder_path = $upload_directory;
            
            $file_name   = '/tw-custom-style.css';

            $dir = trailingslashit( $upload_directory ); // Set storage directory path

            if( ! $wp_filesystem->is_dir( $dir ) ) {
                $wp_filesystem->mkdir( $dir ); // Make a new folder for storing our file
            }

            // Does the folder exist?
            if ( file_exists( $folder_path ) ) {
                // Folder exists, but is the folder writable?
                if ( ! is_writable( $folder_path ) ) {
                    // Folder is not writable.
                    // Does the file exist?
                    if ( ! file_exists( $folder_path . $file_name ) ) {
                        // File does not exist, therefore it can't be created
                        // since the parent folder is not writable.
                        return false;
                    } else {
                        // File exists, but is it writable?
                        if ( ! is_writable( $folder_path . $file_name ) ) {
                            // Nope, it's not writable.
                            return false;
                        }
                    }
                } else {
                    // The folder is writable.
                    // Does the file exist?
                    if ( file_exists( $folder_path . $file_name ) ) {
                        // File exists.
                        // Is it writable?
                        if ( ! is_writable( $folder_path . $file_name ) ) {
                            // Nope, it's not writable
                            return false;
                        }
                    }
                }
            } else {
            // Can we create the folder?
            // returns true if yes and false if not.
                return wp_mkdir_p( $folder_path ); 
            }

          // all is well!
            return true;
        }

        public function make_css(){
            global $wp_filesystem;
            // Initialize the Wordpress filesystem.
            if ( empty( $wp_filesystem ) ) {
                require_once( ABSPATH . '/wp-admin/includes/file.php' );
                WP_Filesystem();
            }
            $content = '';

            $tw_settings = get_option('tw_settings', true) ;
            $enable = isset($tw_settings['enable']) ? 'yes':'no' ;
            $default_tab_set = isset($tw_settings['default_tab_set']) && $tw_settings['default_tab_set']!='' ? $tw_settings['default_tab_set'] : 'none';

            if ( $enable == 'yes' ) {

                $tabwoo = new WP_Query( array (
                    'post_type' => 'tabwoo',
                    'posts_per_page' => -1 
                ));

                if ( $tabwoo->have_posts() ) {
                    $content = $this->get_dynamic_css($tabwoo);
                } 
            }

            // Strip protocols
            $content = str_replace( 'https://', '//', $content );
            $content = str_replace( 'http://', '//', $content );

            if ( is_writable( $this->file( 'path' ) ) || ( ! file_exists( $this->file( 'path'  ) ) && is_writable( dirname( $this->file( 'path' ) ) ) ) ) {

                if ( ! $wp_filesystem->put_contents( $this->file( 'path' ), $content) ) {
                    // Fail!
                    return false;
                } else {
                    // Finally, store the file
                    return true;
                }

            }
        }

        public function get_dynamic_css($tabwoo){
            $posts_array = $tabwoo->posts ;

            global $product;
            $product_id = method_exists( $product, 'get_id' ) ? $product->get_id() : get_the_ID($product) ;

            $content = '';
            $tab_priority = 0;
            foreach ($posts_array as $tabwoo_key => $tabwoo_value) {
                $post_id = $tabwoo_value->ID ;
                $use_custom_settings = false;

                $tw_settings = get_option('tw_settings', true) ;
                $default_tab_set = isset($tw_settings['default_tab_set']) && $tw_settings['default_tab_set']!='' ? $tw_settings['default_tab_set'] : 'none';

                include(TW_PATH . '/inc/frontend/variable.php' );
                include(TW_PATH . '/inc/frontend/custom_css_variable.php' );
                include(TW_PATH.'/inc/frontend/assigned_custom_css_variable.php');

                if ($default_tab_set == $post_id && $tab_priority == 0){
                    $use_custom_settings = true;
                } elseif ($basis == 'category' && $category_list != '' && has_term( $category_list, 'product_cat', $product_id ) && $tab_priority < 1 ) {
                    $tab_priority = 1;
                    $use_custom_settings = true;
                } elseif ($basis == 'tag' && $tag_list != '' && has_term( $tag_list, 'product_tag', $product_id ) && $tab_priority < 2) {
                    $tab_priority = 2;
                    $use_custom_settings = true;
                } elseif ($basis == 'product' && in_array($product_id, $product_list) && $tab_priority < 3 ) {
                    $tab_priority = 3;
                    $use_custom_settings = true;
                }

                if ($use_custom_settings) {
                    $content = $this->get_dynamic_css_content($post_id, $assigned_custom_settings);
                }
            }
            return $content;
        }
        
        public function get_dynamic_css_content($post_id, $assigned_custom_settings){
            $content = '';
            if (isset($assigned_custom_settings) && !empty($assigned_custom_settings)){
                foreach ($assigned_custom_settings as $key => $value) {
                    $$key = $value;
                }
            }

            if($enable_custom_option ) {
                include(TW_PATH.'/inc/frontend/generated_css/responsive_custom_css.php');
            }

            return $content;

        }       

        public function file( $target = 'path') {
            $upload_directory =  TW_DYNAMIC_CSS_PATH;
            $folder_path = $upload_directory;

            $file_name   = '/tw-custom-style.css';

            // The complete path to the file.
            $file_path = $folder_path . $file_name;


            $css_uri_folder = TW_DYNAMIC_CSS_DIR;
            $css_uri = trailingslashit( $css_uri_folder ) . $file_name;

            if ( 'path' == $target ) {
                return $file_path;
            } elseif ( 'url' == $target || 'uri' == $target ) {
                $timestamp = ( file_exists( $file_path ) ) ? '?timestamp=' . filemtime( $file_path ) : '';
                return $css_uri . $timestamp;
            }

        }

    }

    new TW_Dynamic_Css();

}