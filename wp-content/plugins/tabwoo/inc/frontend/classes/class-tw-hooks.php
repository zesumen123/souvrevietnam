<?php

defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('TW_Hooks') ) {

    class TW_Hooks {

        var $tw_lib;

        function __construct() {

            $this->tw_lib = new TW_Library;
            
            add_filter('woocommerce_product_tabs', array( $this,'tw_woocommerce_product_custom_tab'), 99);
            
        }

        function tw_woocommerce_product_custom_tab ( $tabs ) {

            $tw_settings = get_option('tw_settings', true) ;
            $enable = isset($tw_settings['enable']) ? 'yes':'no' ;
            $default_tab_set = isset($tw_settings['default_tab_set']) && $tw_settings['default_tab_set']!='' ? $tw_settings['default_tab_set'] : 'none';

            if ( $enable == 'yes' ) {

                $tabwoo = new WP_Query( array (  
                            'post_type' => 'tabwoo',  
                            'posts_per_page' => -1  
                        )
                    );

                if ( $tabwoo->have_posts() ) {

                    $posts_array = $tabwoo->posts ; 

                    global $product ;

                    $product_id = method_exists( $product, 'get_id' ) ? $product->get_id() : $product->id ;

                    $default_tabs = $tabs;
                    $tab_priority = 0;
                    foreach ($posts_array as $tabwoo_key => $tabwoo_value) {
                        $post_id = $tabwoo_value->ID ;
                        include(TW_PATH . '/inc/frontend/variable.php' );
                        if ($default_tab_set == $post_id && $tab_priority == 0){
                            include( TW_PATH . '/inc/frontend/each_tab.php' );
                        } else {
                            include(TW_PATH . 'inc/frontend/customize_tab.php') ;
                        }
                    }
                    ?>
                    <div id="tw-main-outer-wrapper-id" class="tw-main-outer-wrapper tw-<?php echo $applied_post_id; ?> <?php echo $applied_orientation_class; ?> <?php echo $applied_template_class; ?> <?php echo $applied_enable_accordion_class; ?> <?php echo $applied_width_class; ?>" data-accordion="<?php echo $applied_enable_accordion; ?>" data-tabset-id="<?php echo $applied_post_id; ?>" data-tab-width="<?php echo $applied_tab_width; ?>">
                        <?php
                            return $tabs;
                        ?>
                    </div> <?php
                } else {
                    return $tabs; 
                }
            } else { 
                return $tabs;
            }
        }
    }
    new TW_Hooks();
}

