<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );

if ( isset( $_GET[ 'orientation' ] ) && $_GET[ 'orientation' ] != '' ) {
	$orientation = $_GET[ 'orientation' ];
	if ($orientation == 'vertical') {
		if ( isset( $_GET[ 'template' ] ) && ($_GET[ 'template' ]) != '' ) {
			$v_template = $_GET[ 'template' ];
		} else {
			$v_template = 1;
		}
	} else {
		if ( isset( $_GET[ 'template' ] ) && ($_GET[ 'template' ]) != '' ) {
			$h_template = $_GET[ 'template' ];
		} else {
			$h_template = 1;
		}
	}
}else{
	$orientation = 'horizontal';
	$v_template = 1;
	$h_template = 1;
}

$orientation_class = 'tw-tab-orientation-'.$orientation;

$template_class = ($orientation == 'vertical') ? 'tw-tab-template-template'.$v_template.' tw-tab-v-template-template'.$v_template : 'tw-tab-template-template'.$h_template.' tw-tab-h-template-template'.$h_template ;