<div class="psfw-header">
    <div>
        <div id="psfw-fb-root"></div>
        <script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>
        <script>!function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location)?'http':'https'; if (!d.getElementById(id)){js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); }}(document, 'script', 'twitter-wjs');</script>
    </div>
    <div class="psfw-header-section">
        <div class="psfw-header-left">
            <div class="psfw-title"><?php esc_html_e('TabWoo - Extra Tabs Plugin for WooCommerce', TW_TD ); ?> </div>
            <div class="psfw-version-wrap">
                <span> Version <?php echo TW_VERSION; ?> </span>
            </div>
        </div>
        <div class="psfw-header-social-link">
            <p class="psfw-follow-us"><?php esc_html_e( 'Follow us for new updates', TW_TD ); ?></p>
            <div class="fb-like" data-href="https://www.facebook.com/accesspressthemes" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
            <a href="//twitter.com/accesspressthemes" class="twitter-follow-button" data-show-count="false">Follow @accesspressthemes</a>
        </div>
    </div>
</div>
<div class="tw-container tw-about-main-wrapper">
    <div class="primary-content psfw-columns-wrapper">
        <div class="psfw-left-column">
            <h3><?php esc_html_e('How To Use', TW_TD ); ?></h3>
            <p>
                <?php esc_html_e('For customizing the plugin\'s settings, click on TabWoo menu in Wordpress left admin menu. You will notice 5 submenus in TabWoo. They are, All Tab Set, Add Tab Set, Settings, How To Use and More WordPress Stuff. ', TW_TD ); ?>
            </p>

            <p>
                <?php esc_html_e('To start using the plugin, please go to the "Settings" submenu and make sure you enable the plugin. This setting also gives you an option to select any one of the tabsets that you have created, as a default tabset, for all your products. Also, If you want to display a map inside any one of your tab, please enter your GoogLe Map API inside this setting.', TW_TD ); ?>
            </p>



            <h3 id="tabwoo-listing-page"><strong><?php esc_html_e('All Tab Set', TW_TD ); ?></strong></h3>
            <p>
                <?php esc_html_e('On clicking TabWoo menu, you will go to the plugin\'s listing page with a "Add New" button at the top. Clicking on that button will redirect you to the TabWoo page where you can create your tab set. Basically, there are three metaboxes inside the page to help you create tabs, assign them to products and provide layouts to the tabset.', TW_TD ); ?>
            </p>

            <h2 id="tabwoo-tab-items"><strong><?php esc_html_e('Tab Items', TW_TD ); ?></strong></h2>
            <p>
                <?php esc_html_e('The Tab Items metabox is for adding tabs to your tabset. You will notice four tabs already available in this metabox along with a "Add Tab Item" button. The 1st three tabs are default tabs provided by WooCommerce for their products. The fourth tab is the custom tab provided by our plugin. If you want more tabs in your tab set, please click the "Add Tab Item" button. This will add a custom tab at the end of the tab list. Also, please note that all the tab items are sortable. You can set a order for them, to display, as per your requirement.', TW_TD ); ?>
            </p>

            <p><?php esc_html_e('Each of the tab settings are explained below', TW_TD ); ?></p>

            <h4><?php esc_html_e(' Default WooCommerce Tab ', TW_TD ); ?></h4>

            <ul>
                <li><?php esc_html_e('Tab Label: Please enter a label for this tab. Note: The label you enter here will replace the default "Description", "Additional Description" and "Reviews" label of the tabs accordingly. ', TW_TD ); ?></li>
                <li><?php esc_html_e('Choose Icon Type: Please select a icon for this tab here. You can either select a icon from the available list of icons or upload your own custom icon.', TW_TD ); ?></li>
            </ul>
            <h4> <?php esc_html_e('Custom Tab ', TW_TD ); ?></h4>

            <ul>
                <li><?php esc_html_e('Tab Label: Please enter a label for this tab. ', TW_TD ); ?></li>
                <li><?php esc_html_e('Choose Icon Type: Please select a icon for this tab here. You can either select a icon from the available list of icons or upload your own custom icon.', TW_TD ); ?></li>
                <li><?php esc_html_e('Choose Component: Please select a component for this tab here. There are a total of 9 different options you can choose from for the custom tab. You can add content to your tabs using any one of the components that suits you the best. Each of them are described below:', TW_TD ); ?>
            </li>
        </ul>
        <h4 id="tabwoo-custom-tab-components"><?php esc_html_e(' Custom Tab Components', TW_TD ); ?></h4>

        <h5><i><?php esc_html_e('WYSIWYG Editor', TW_TD ); ?></i></h5>

        <p> <?php esc_html_e('This is a normal editor provided by WordPress. You can add your contents here.', TW_TD ); ?></p>

        <h5><i><?php esc_html_e('External Shortcode', TW_TD ); ?></i></h5>


        <p><?php esc_html_e('This component is the most versatile of all the components available. You can add shortcode of any plugin here and display the respective result in a tab.', TW_TD ); ?></p>

        <h5><i><?php esc_html_e('Product', TW_TD ); ?></i></h5>


        <p><?php esc_html_e('This components helps display products inside a tab. There are various options available for this component.', TW_TD ); ?>
        <ul>
            <li>
                <?php esc_html_e('Product Type: You can select the kind of products you want to display in the tab here. It could be products of any category as per your preference. The products you will filter will be displayed in a list inside the tab. Also, you will be provided with further settings.', TW_TD ); ?>
                <ul>
                    <li><?php esc_html_e('Taxonomy/Category: You can select the taxonomy to filter your products from, here', TW_TD ); ?></li>
                    <li><?php esc_html_e('Product Categories: Based on the taxonomy you have selected in the above option, all the list of items available for that particular taxonomy will be displayed here. You can select your product category from the available list of items.', TW_TD ); ?></li> 
                    <li><?php esc_html_e('OrderBy: Please select a order in which you want to display your products. It can be ordered on the basis of ID, Author, Title, Date, Parent ID, Random or Comment Count.', TW_TD ); ?></li>
                    <li><?php esc_html_e('Order: The order of the item can be either ascending or descending.', TW_TD ); ?></li>
                    <li><?php esc_html_e('Number of Post: Please enter the number of products you want to display in this tab', TW_TD ); ?> </li>
                    <li><?php esc_html_e('Display Product Excerpt: Please enable to show content', TW_TD ); ?></li>
                    <li><?php esc_html_e('Product Excerpt Length: Please enter the length of post content', TW_TD ); ?></li>
                    <li><?php esc_html_e('Display Product Link in Title: Please enable to show product link in title', TW_TD ); ?></li>
                    <li><?php esc_html_e('Link Option: Please select how you want your links to be opened. It can either be opened in a new window or same window.', TW_TD ); ?></li>
                    <li><?php esc_html_e('Price Type: Please select the kind of price you want to display in your products. It can either be sales price or actual price.', TW_TD ); ?></li>
                </ul>
            </li>
        </ul>
    </p>

    <h5><i><?php esc_html_e('Custom Link', TW_TD ); ?></i></h5>


    <p>
        <?php esc_html_e('This component helps assign a custom url to the tab title. So that, when you click on the tab, you are redirected to a custom link that you enter in the text field.', TW_TD ); ?>
    </p>
</div>
<div class="psfw-right-column">

    <h5><i><?php esc_html_e('Map', TW_TD ); ?></i></h5>

    <p>
        <?php esc_html_e('This components helps you display a map inside your tab. Please make sure that you have entered your Google API inside the main plugin setting for this feature to work. The main plugin settings is one of the sub menus of our plugin- Tabwoo. ', TW_TD ); ?>
    </p>
    <p>
        <?php esc_html_e('Once the API is set and stored, please enter the latitude, longitude and zoom level of your location to display in the map.', TW_TD ); ?>
    </p>

    <h5><i><?php esc_html_e('Downlaod', TW_TD ); ?></i></h5>

    <p>
        <?php esc_html_e('This components helps you display downloadable files inside your tab. These downloadable files can be uploaded inside this component.', TW_TD ); ?>
    </p>

    <h5><i><?php esc_html_e('FAQ', TW_TD ); ?></i></h5>

    <p> <?php esc_html_e('A list of frequently asked questionaires can be put in this component. These lists will be then displayed inside a tab.', TW_TD ); ?></p>

    <h5><i><?php esc_html_e('Photo Gallery', TW_TD ); ?></i></h5>

    <p>
        <?php esc_html_e('Any number of pictures can be uploaded in this componenent. The uploaded picture will be displayed inside a tab.', TW_TD ); ?>
    </p>

    <h5><i><?php esc_html_e('Video Gallery', TW_TD ); ?></i></h5>

    <p>
        <?php esc_html_e('Either a Youtube video or Viemo video can be uploaded in this tab. The video will be displayed inside a tab.', TW_TD ); ?>
    </p>

    <h2 id="tabwoo-assign-tab-sets"><strong><?php esc_html_e('Assign Tab Set', TW_TD ); ?></strong></h2>
    <p> <?php esc_html_e('This metabox is for assigning the above generated tab set to any product categories/tags or a list of random individual products.', TW_TD ); ?></p>

    <p><?php esc_html_e('This metabox has following fields:', TW_TD ); ?></p>
    <ul>
        <li><?php esc_html_e('Assign Tab: Please select which products, product tag or product categories you want this particular tab set to be assigned to. NOTE: Please make sure that you do not assign the same product/ product categories to multiple tab sets.

        Based on what you have seleted in this setting, the following settings will be displayed.', TW_TD ); ?>

        <ul>
            <li><?php esc_html_e('Product Categories: Please select categories of product you want to display in this tab', TW_TD ); ?></li>
            <li><?php esc_html_e('Product Tags: Please select categories of product you want to display in this tab', TW_TD ); ?></li>
            <li><?php esc_html_e('Products: Please enter name of the product that you want this tab set to be assigned to here.', TW_TD ); ?></li>
        </ul>
    </li>
</ul>

<h2 id="tabwoo-main-settings"><strong><?php esc_html_e('Main Settings ', TW_TD ); ?></strong></h2>
<?php esc_html_e('The Main Settings metabox is for basic settings of tabset. It has 3 main tabs which are described below:', TW_TD ); ?>
<h2><?php esc_html_e('General Settings', TW_TD ); ?></h2>

<ul>
    <li><?php esc_html_e(' Tab Labels Display Format: Please select how you want your tab set to be displayed. You can either choose to show tab title only or show icon and title both or show icon only', TW_TD ); ?></li>
    <li> <?php esc_html_e('Show Icon Position: Please select an icon position for the tab set. It can either be left or right or top or bottom of the tab title', TW_TD ); ?></li>
    <li><?php esc_html_e('Tab Width: Set width for the individual tabs as per your requirement. When you select "Automatic Width Calculation", the individual tab items will take a width according to the length oftab label. However, if you select "Same Width tabs", it will take the width that you enter for the tab item.', TW_TD ); ?></li>
    <li><?php esc_html_e('Enable Accordion For Responsive: Please check to enable accordion for responsive. Note: If this option is enabled, then on mobile version tab will work in accordion.', TW_TD ); ?> </li>
</ul>

<h2 id="tabwoo-display-settings"><?php esc_html_e('Display Settings', TW_TD ); ?></h2>

<ul>
    <li> <?php esc_html_e('Orientation Type: Please select an orientation for this tab. It can either be horizontal or vertical.', TW_TD ); ?></li>
    <li><?php esc_html_e('Choose Tab Template: Select a template for the tabset from the available list of templates.', TW_TD ); ?></li>
</ul>

<h2 id="tabwoo-custom-styling"><?php esc_html_e('Custom Styling', TW_TD ); ?></h2>

<p><?php esc_html_e(' This tab helps you give custom styling to the available templates as per your requirement.', TW_TD ); ?></p>


<h2 id="tabwoo-individual-product-page"><strong><?php esc_html_e('Individual Product Page Settings (TabWoo Customization Options)', TW_TD ); ?></strong></h2>

<p><?php esc_html_e('This metabox will be available in all of your individual product pages. This settings will help you overwrite the content you have set for the product or the category that this product has been set to, in the tabset.', TW_TD ); ?> </p>

<p><?php esc_html_e('For example, You have created a Tabset named "TabSet ABC" using our plugin and have assigned this particular tabset to product category "CAT XYZ". Now, Assume, your tabset has a tab whose content is a video. All the products that fall under the category CAT XYZ will have a tab with the video content, entered inside the Video component of that paticular tabset. However, if you want a different video to be displayed for any one of the product that belongs to the category CAT XYZ, you can set it here.', TW_TD ); ?></p>

<p><?php esc_html_e('Also, please note, the contents you have set for the respective tab here will not be displayed in your product page unless, this product has been assigned a tab set. To assign this product or the category this product falls under to a tab set, please go to our plugin settings.', TW_TD ); ?></p>

<p><?php esc_html_e('This documentation tries to cover as much details about plugin as possible to ease your plugin experience.', TW_TD ); ?></p>

<strong><?php esc_html_e('Also, you might want to check out,', TW_TD ); ?></strong>
<div dir="ltr"><a href="//accesspressthemes.com/wordpress-plugins/tabwoo"><u><?php esc_html_e('View Plugin Full Features', TW_TD ); ?> </u></a></div>
<div dir="ltr"><a href="//demo.accesspressthemes.com/wordpress-plugins/tabwoo"><u><?php esc_html_e('View Plugin Demo', TW_TD ); ?> </u></a></div>
</div>
</div>
</div>