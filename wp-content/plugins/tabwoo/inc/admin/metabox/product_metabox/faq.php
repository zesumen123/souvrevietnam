<?php defined('ABSPATH') or die("No script kiddies please!"); ?>
<div class="tw-each-tab-column tw-faq-wrapper">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title' >
                <span class="tw_label_keyup"><?php esc_html_e('FAQ', TW_TD) ?></span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i></span>
            </div>
        </div>
		<div class = "tw-tab-item-options clearfix" style="display:none;">
		   <div class="tw-faq-outer-wrapper tw_options_wrap">
		    	<input type="button" class="tw-add-faq-block" value="<?php esc_html_e('Add FAQ', TW_TD) ?>" data-key="<?php echo $key;?>"  data-type="product"> 
		    	<div class="tw-faq-wrapper">

		    		<?php if (!empty($single_product_settings['faq']['question'])) { 
			    			$array_value = 0;
			                foreach ($single_product_settings['faq']['question'] as $option) {
			                    ?>
			                    <div class="tw-faq-wrap">
			                    	<div class="tw-faq-inner-wrap">
			                        <span class="tw-drag fa fa-arrows-alt"></span>
		                        <a href="javascript:void(0)" class="tw-faq-remover"> <i class="fa fa-trash"></i> </a>
			                        
			                        <?php $question = (!empty($option) ? $option : "Question"  ) ?>
			                        <?php $answer = (!empty($single_product_settings['faq']['answer'][$array_value]) ? $single_product_settings['faq']['answer'][$array_value] : "Answer" ) ?>

			                        <div class="tw-faq-question">
			    						<input type="text" name="single_product_settings[faq][question][]" value="<?php echo esc_attr($question); ?>">
			    					</div>
			    					<div class="tw-faq-question">
			    						<textarea name="single_product_settings[faq][answer][]" ><?php echo esc_attr($answer) ?></textarea>
			    					</div>

			    				</div>
			                    </div>
			                    <?php
			                    $array_value++;
			                }
						} else { ?>
				    		<div class="tw-faq-wrap">
				    			<div class="tw-faq-inner-wrap">
				                <span class="tw-drag fa fa-arrows-alt"></span>
		                        <a href="javascript:void(0)" class="tw-faq-remover"> <i class="fa fa-trash"></i> </a>
				                
				    			<div class="tw-faq-question">
				    				<input type="text" name="single_product_settings[faq][question][]" value="" placeholder="Question">
				    			</div>
				    			<div class="tw-faq-question">
				    				<textarea name="single_product_settings[faq][answer][]" placeholder="Answer" ></textarea>
				    			</div>
				    		</div>
				    		</div>
			    	<?php } ?>
		    	</div>
		    </div>
		</div>
	</div>
</div>