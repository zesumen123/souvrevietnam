<?php defined('ABSPATH') or die("No script kiddies please!"); ?>

<div class="tw-each-tab-column tw-download-wrapper">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title' >
                <span class="tw_label_keyup"><?php esc_html_e( 'Download', TW_TD ) ?></span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i></span>
            </div>
        </div>
        <div class = "tw-tab-item-options clearfix" style="display:none;">
        <div class="tw-field-wrap tw-row-even">
        <label><?php esc_html_e( 'Upload Files', TW_TD ) ?> </label>
        <div class="tw-right-options"> 
            <input type="button" class='button-secondary tw-upload-download-btn ' data-key= "<?php echo $key; ?>" value='<?php _e('Upload Files', TW_TD); ?>' data-type="product" />

            <div class='tw-uploaded-files'>
                <?php if (isset($single_product_settings['download']) && !empty($single_product_settings['download'])){
                    foreach ($single_product_settings['download']as $items => $item) { ?>
                        <div class="tw-downloadable-file-preview">
                        <div class="tw-inner-downloadable-file-preview">
                            <div class="tw-each-item-actions-wrap clearfix">
                                <a href="javascript:void(0)" class="tw-move-item"><span class="dashicons dashicons-move"></span></a>
                                <a href="javascript:void(0)" class="tw-delete-item"><span class="dashicons dashicons-trash"></span></a>
                            </div>
                            <div class="tw-setting-image">
                                <input type="hidden" name='single_product_settings[download][]' class='tw-download-url tw-tab-text' value='<?php echo esc_attr( $item ); ?>' />
                                <?php 
                                    $filename = basename($item);
                                ?>
                                <span class="tw-downloadable-file"><?php echo esc_attr( $filename ); ?> </span>
                            </div>
                        </div>
                    </div>
                        <?php
                    }
                } ?>
            </div>
        </div>
        </div>
    </div>
</div>
</div>