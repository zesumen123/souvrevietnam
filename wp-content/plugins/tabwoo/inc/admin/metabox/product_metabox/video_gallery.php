<?php defined('ABSPATH') or die("No script kiddies please!");

$video_type = isset($single_product_settings['video']['type']) && $single_product_settings['video']['type']!="" ? $single_product_settings['video']['type'] :"";
$video_youtube = isset($single_product_settings['video']['youtube']) && $single_product_settings['video']['youtube']!="" ? $single_product_settings['video']['youtube'] :"";
$video_viemo = isset($single_product_settings['video']['viemo']) && $single_product_settings['video']['viemo']!="" ? $single_product_settings['video']['viemo'] :"";

?>

<div class="tw-each-tab-column tw-video-gallery-wrapper">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title' >
                <span class="tw_label_keyup"><?php esc_html_e('Video Gallery', TW_TD) ?> </span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i></span>
            </div>
        </div>
		<div class = "tw-tab-item-options clearfix" style="display:none;">
		    <div class="tw-field-wrap">
				<label> <?php esc_html_e('Video type', TW_TD ); ?></label>
				<div class="tw-right-options"> 
					<select id='tw-background-video-type' name='single_product_settings[video][type]' class='tw-ns-active tw-video-select-option'>
						<option value='youtube' <?php selected(esc_attr($video_type), 'youtube') ?> > <?php esc_html_e('Youtube', TW_TD); ?> </option>
						<option value='viemo' <?php  selected(esc_attr($video_type), 'viemo') ?> > <?php esc_html_e('Viemo', TW_TD); ?> </option>
					</select>
					<p class="description"><?php esc_html_e('Note: All the videos available for a particular product will be displayed in this tab. ',TW_TD);?></p>
				</div>
			</div>

			<div class='tw-field-wrap tw-video tw-youtube' <?php echo (esc_attr($video_type) == 'viemo')? "style='display:none;'" :'' ?>>
				<label><?php esc_html_e('Youtube Video URL', TW_TD); ?></label>
				<div class="tw-right-options"> 
				<input id='tw-background-video-youtube' type="url" name='single_product_settings[video][youtube]' value='<?php echo esc_attr($video_youtube);?>'/>
				<p class="description"><?php esc_html_e('Please enter a youtube video url here. Note: Make sure that the youtube url is a embeded url to make sure the video is displayed in the tab.',TW_TD);?></p>
				</div>
			</div>

			<div class="tw-field-wrap tw-video tw-viemo" <?php echo (esc_attr($video_type) == 'youtube')? "style='display:none;'" :'' ?>>
				<label><?php esc_html_e( 'Viemo Video URL', TW_TD ); ?></label>
				
				<div class="tw-right-options"> 
					<input id='tw-background-video-viemo' type="url" name='single_product_settings[video][viemo]' value='<?php echo esc_attr($video_viemo);?>' />
					<p class="description"><?php esc_html_e('Please enter a Vimeo video url here.',TW_TD);?></p>
				</div>
			</div>
		</div>
	</div>
</div>