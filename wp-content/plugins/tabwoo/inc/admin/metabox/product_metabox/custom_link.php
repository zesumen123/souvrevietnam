<?php defined('ABSPATH') or die('No script kiddies please!!'); 
$custom_link_url = (isset($single_product_settings['clink']['custom_link_url']) && $single_product_settings['clink']['custom_link_url'] != '')?$single_product_settings['clink']['custom_link_url']:'';
$link_target = (isset($single_product_settings['clink']['custom_link_target']) && $single_product_settings['clink']['custom_link_target'] != '')?esc_attr($single_product_settings['clink']['custom_link_target']):'';
?>

<div class="tw-each-tab-column tw-clink-wrapper">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title' >
                <span class="tw_label_keyup"><?php esc_html_e( 'Custom Link',TW_TD);?></span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i></span>
            </div>
        </div>
        <div class = "tw-tab-item-options clearfix" style="display:none;">

        <div class="tw-options_wrap tw-clink_wrapper">
            <div class="tw-field-wrap tw-row-even">
                <label><?php esc_html_e('Custom Link',TW_TD);?></label>
                <div class="tw-right-options"> 
                    <input type="text" name="single_product_settings[clink][custom_link_url]" class="tw-tab-text tw-clink-url" value="<?php echo esc_attr($custom_link_url);?>"/>
                    <p class="description"><?php esc_html_e( 'Please enter custom link for this tab', TW_TD ) ?></p>
                </div>
            </div>
        	<div class="tw-field-wrap tw-row-odd">
                <label><?php esc_html_e('Link Target',TW_TD);?></label>
                <div class="tw-right-options"> 
                    <select name="single_product_settings[clink][custom_link_target]" class="tw-ns-active ">
                        <option value="_blank" <?php selected(esc_attr($link_target),"_blank");?>>_blank</option>
                        <option value="_parent" <?php selected(esc_attr($link_target),"_parent");?>>_parent</option>
                        <option value="_self" <?php selected(esc_attr($link_target),"_self");?>>_self</option>
                        <option value="_top" <?php selected(esc_attr($link_target),"_top");?>>_top</option>      
                    </select>
                    <p class="description"><?php esc_html_e( 'Please select a link target for this tab', TW_TD ) ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>