<?php defined('ABSPATH') or die('No script kiddies please!!'); 

$product_type = isset($single_product_settings['product']['type']) && $single_product_settings['product']['type']!="" ? $single_product_settings['product']['type'] :"category";
$select_post_taxonomy = isset($single_product_settings['product']['select_post_taxonomy']) && $single_product_settings['product']['select_post_taxonomy']!="" ? $single_product_settings['product']['select_post_taxonomy'] : "select" ;
$simple_taxonomy_terms = isset($single_product_settings['product']['simple_taxonomy_terms']) && $single_product_settings['product']['simple_taxonomy_terms']!="" ? $single_product_settings['product']['simple_taxonomy_terms'] : "select" ;
$tw_select_orderby = isset($single_product_settings['product']['tw_select_orderby']) && $single_product_settings['product']['tw_select_orderby']!="" ? $single_product_settings['product']['tw_select_orderby'] : "none" ;
$tw_select_order = isset($single_product_settings['product']['tw_select_order']) && $single_product_settings['product']['tw_select_order']!="" ? $single_product_settings['product']['tw_select_order'] : "ASC" ;
$tw_post_number = isset($single_product_settings['product']['tw_post_number']) && $single_product_settings['product']['tw_post_number']!="" ? $single_product_settings['product']['tw_post_number'] : "10" ;

$show_content = isset($single_product_settings['product']['carousel']['show_content']) && $single_product_settings['product']['carousel']['show_content'] == 'yes' ? 'yes' : 'no' ;
$excerpt_length = isset($single_product_settings['product']['carousel']['excerpt_length']) && $single_product_settings['product']['carousel']['excerpt_length']!= '' ? $single_product_settings['product']['carousel']['excerpt_length'] : 30 ;
$show_link_title = isset($single_product_settings['product']['carousel']['show_link_title']) && $single_product_settings['product']['carousel']['show_link_title']== 'true' ? 'true' : 'false' ;
$link_option = isset($single_product_settings['product']['carousel']['link_option']) && $single_product_settings['product']['carousel']['link_option']== 'same_window' ? 'same_window' : 'new_window' ;
$product_price = isset($single_product_settings['product']['carousel']['product_price']) && $single_product_settings['product']['carousel']['product_price']!= '' ? $single_product_settings['product']['carousel']['product_price'] : 'actual_price' ;

?>
<div class="tw-each-tab-column tw-product-wrapper">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title'>
                <span class="tw_label_keyup"><?php esc_html_e('Products', TW_TD) ?> </span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i> </span>
            </div>
        </div>
        <div class = "tw-tab-item-options clearfix" style="display:none;">
            <div class="tw-options-wrap tw-clink_wrapper">
             <div class="tw-field-wrap">
                <label><?php esc_html_e('Product Type',TW_TD);?></label>
                <div class="tw-right-options"> 
                    <div class="tw-display-format">
                        <input class="tw_product_type" type="radio" id="tw_category" name="single_product_settings[product][type]" value="category" <?php if($product_type == "category") echo "checked";?>> 
                        <label for="tw_category"><?php esc_html_e('Category',TW_TD);?></label>
                    </div>
                </div>
            </div>
            <div class ="tw_product_type_cat" >
                <div class="tw-field-wrap">
                    <label><?php esc_html_e('Taxonomy/Category',TW_TD);?></label>
                    <div class="tw-right-options"> 
                        <select name="single_product_settings[product][select_post_taxonomy]" class="tw-ns-active tw-select-taxonomy">
                            <option value="select" <?php selected(esc_attr($select_post_taxonomy) ,'select') ?>><?php echo esc_html_e( 'Choose Taxonomy', TW_TD ); ?></option>
                            <?php
                            $product_post_type = 'product';
                            $taxonomies = get_object_taxonomies( $product_post_type, 'objects' );

                            if(is_array($taxonomies)){

                                foreach ( $taxonomies as $tax ) {
                                    $tax_name = $tax -> name;
                                    $label = $tax -> label;
                                    ?>
                                    <option value="<?php echo $tax_name; ?>" <?php selected(esc_attr($select_post_taxonomy), $tax_name) ?>><?php echo $label; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <p class="description"><?php esc_html_e( 'Please select the taxonomy here', TW_TD ); ?></p>
                        <div class="tw-loader-preview" style="display:none;">
                            <img src="<?php echo TW_IMG_DIR . '/ajax-loader-add.gif' ?>">
                        </div>
                    </div>
                </div>
                
                <div class="tw-field-wrap">
                    <label><?php esc_html_e('Product Categories',TW_TD);?></label>
                    <div class="tw-right-options"> 
                        <select name="single_product_settings[product][simple_taxonomy_terms]" class="tw-simple-taxonomy-term">
                            <option value=""><?php echo esc_html_e( 'Choose Category', TW_TD ); ?></option>
                            <?php
                            if ( ! empty( $simple_taxonomy_terms ) ) {
                                echo $this->tw_lib->tw_fetch_category_list( $select_post_taxonomy, $simple_taxonomy_terms );
                            }
                            ?>
                        </select>
                        <p class="description"><?php esc_html_e( 'Please select a category of products you want to display in this tab', TW_TD ); ?></p>

                    </div>
                </div>
                
                <div class ="tw-field-wrap">
                    <label><?php esc_html_e( 'OrderBy', TW_TD ); ?></label>
                    <div class="tw-right-options">
                        <select name="single_product_settings[product][tw_select_orderby]" class="tw-ns-active tw-select-orderby">
                            <option value="none" <?php selected(esc_attr($tw_select_orderby), 'none') ?>><?php esc_html_e( 'None', TW_TD ) ?></option>
                            <option value="ID" <?php selected(esc_attr($tw_select_orderby), 'ID') ?>><?php esc_html_e( 'ID', TW_TD ) ?></option>
                            <option value="author" <?php selected(esc_attr($tw_select_orderby), 'author')?>><?php esc_html_e( 'Author', TW_TD ) ?></option>
                            <option value="title" <?php selected(esc_attr($tw_select_orderby), 'title') ?>><?php esc_html_e( 'Title', TW_TD ) ?></option>
                            <option value="date" <?php selected(esc_attr($tw_select_orderby), 'date') ?>><?php esc_html_e( 'Date', TW_TD ) ?></option>
                            <option value="parent" <?php selected(esc_attr($tw_select_orderby), 'parent') ?>><?php esc_html_e( 'Parent ID', TW_TD ) ?></option>
                            <option value="rand" <?php selected(esc_attr($tw_select_orderby), 'rand') ?>><?php esc_html_e( 'Random', TW_TD ) ?></option>
                            <option value="comment_count" <?php selected(esc_attr($tw_select_orderby), 'comment_count') ?>><?php esc_html_e( 'Comment Count', TW_TD ) ?></option>

                        </select>
                        <p class="description"><?php esc_html_e( 'Please select a order in which you want to display your products', TW_TD ); ?></p>
                    </div>
                </div>

                <div class ="tw-field-wrap">
                    <label><?php esc_html_e( 'Order', TW_TD ); ?></label>
                    <div class="tw-right-options">
                        <select name="single_product_settings[product][tw_select_order]" class="tw-ns-active tw-select-order">
                            <option value="ASC" <?php selected(esc_attr($tw_select_order),'ASC') ?> ><?php esc_html_e( 'Ascending', TW_TD ) ?></option>
                            <option value="DESC" <?php selected(esc_attr($tw_select_order),'DESC') ?> ><?php esc_html_e( 'Descending', TW_TD ) ?></option>
                        </select>
                        <p class="description"><?php esc_html_e( 'Please select a sorting order in which you want to display your products', TW_TD ); ?></p>
                    </div>
                </div>

                <div class="tw-field-wrap">
                    <label><?php esc_html_e( 'Number of Post', TW_TD ); ?></label>
                    <div class="tw-right-options">
                        <input type="number" class="tw-post-number" min="1" name="single_product_settings[product][tw_post_number]"  value="<?php echo esc_attr($tw_post_number) ?>"/>
                        <p class="description"><?php esc_html_e( 'Please enter the number of products you want to display in this tab', TW_TD ) ?></p>
                    </div>
                </div>

                <div class="tw-field-wrap">
                    <label for="tw-content-view-check" class="tw-content-view">
                        <?php esc_html_e( 'Display Product Excerpt', TW_TD ); ?>
                    </label>
                    <div class="tw-right-options">
                        <select name="single_product_settings[product][carousel][show_content]" class="tw-show-content tw-ns-active tw-select-order">
                            <option value="yes" <?php selected(esc_attr($show_content),'yes') ?> ><?php esc_html_e( 'Yes', TW_TD ) ?></option>
                            <option value="no" <?php selected(esc_attr($show_content),'no') ?> ><?php esc_html_e( 'No', TW_TD ) ?></option>
                        </select>
                        <p class="description"><?php esc_html_e( 'Please enable to show content', TW_TD ) ?></p>
                    </div>
                </div>
                <div class ="tw-field-wrap tw-excerpt-length" <?php echo (esc_attr($show_content)=='yes') ? 'style="display:block; "':'style="display:none; "' ?>>
                    <label>
                        <?php esc_html_e( 'Product Excerpt Length', TW_TD ); ?>
                    </label>
                    <div class="tw-right-options">
                        <input type="number" class="tw-post-excerpt" min="10" name="single_product_settings[product][carousel][excerpt_length]"  value="<?php echo esc_attr($excerpt_length); ?>"/>
                        <p class="description"><?php esc_html_e( 'Please enter the length of post content', TW_TD ) ?></p>
                    </div>
                </div>

                <div class ="tw-field-wrap">
                    <label> <?php esc_html_e( 'Display Product Link in Title', TW_TD ); ?> </label>
                    <div class="tw-right-options">
                        <select class="tw-ns-active" name="single_product_settings[product][carousel][show_link_title]" class="tw-select-order">
                            <option value="true" <?php selected(esc_attr($show_link_title),'true') ?> ><?php esc_html_e( 'True', TW_TD ) ?></option>
                            <option value="false" <?php selected(esc_attr($show_link_title),'false') ?> ><?php esc_html_e( 'False', TW_TD ) ?></option>
                        </select>
                        <p class="description"><?php esc_html_e( 'Please enable to show product link in title', TW_TD ) ?></p>
                    </div>
                </div>

                <div class ="tw-field-wrap">
                    <label><?php esc_html_e( 'Link Option', TW_TD ); ?></label>
                    <div class="tw-right-options">
                        <select name="single_product_settings[product][carousel][link_option]" class="tw-ns-active tw-slide-pager">
                            <option value="same_window" <?php selected(esc_attr($link_option), 'same_window') ?> ><?php esc_html_e( 'Open in same window', TW_TD ) ?></option>
                            <option value="new_window" <?php selected(esc_attr($link_option), 'new_window') ?>  ><?php esc_html_e( 'Open in new window', TW_TD ) ?></option>
                        </select>
                        <p class="description"><?php esc_html_e( 'Please select how you want your links to be opened', TW_TD ) ?></p>

                    </div>
                </div>

                <div class ="tw-field-wrap">
                    <label><?php esc_html_e( 'Price Type', TW_TD ); ?></label>
                    <div class="tw-right-options">
                        <select name="single_product_settings[product][carousel][product_price]" class="tw-ns-active tw-slide-pager">
                            <option value="actual_price" <?php selected(esc_attr($product_price), 'actual_price') ?> ><?php esc_html_e( 'Actual Price', TW_TD ) ?></option>
                            <option value="sale_price" <?php selected(esc_attr($product_price), 'sale_price') ?>  ><?php esc_html_e( 'Sale Price', TW_TD ) ?></option>
                        </select>
                        <p class="description"><?php esc_html_e( 'Please select the kind of price you want to display in your products', TW_TD ) ?></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>