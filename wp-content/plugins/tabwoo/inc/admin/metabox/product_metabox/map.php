<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$api_key = isset($single_product_settings['gmap']['api_key']) && $single_product_settings['gmap']['api_key']!=""? $single_product_settings['gmap']['api_key'] :"";
$latitude = isset($single_product_settings['gmap']['latitude']) && $single_product_settings['gmap']['latitude']!=""? $single_product_settings['gmap']['latitude'] :"";
$longitude = isset($single_product_settings['gmap']['longitude']) && $single_product_settings['gmap']['longitude']!=""? $single_product_settings['gmap']['longitude'] :"";
$zoom_level = isset($single_product_settings['gmap']['zoom_level']) && $single_product_settings['gmap']['zoom_level']!=""? $single_product_settings['gmap']['zoom_level'] :"";

?>
<div class="tw-each-tab-column tw-map-wrapper">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title' >
                <span class="tw_label_keyup"><?php esc_html_e('Map',TW_TD);?></span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i></span>
            </div>
        </div>
        <div class = "tw-tab-item-options clearfix" style="display:none;">
            <div class="tw-field-wrap tw-row-even">
                 <label><?php esc_html_e('Latitude',TW_TD);?></label>
                 <div class="tw-right-options"> 
                 <input type="text" class="tw-tab-text" value="<?php echo esc_attr($latitude);?>" name="single_product_settings[gmap][latitude]">
                 <p class="description"><?php esc_html_e('Please enter a latitude value for the map. Please go to',TW_TD);?> <a href="//www.latlong.net/" target="_blank"><?php esc_html_e('latitude',TW_TD);?></a><?php esc_html_e(' for the details',TW_TD);?></p>
                 </div>
            </div>
            <div class="tw-field-wrap tw-row-odd">
                 <label><?php esc_html_e('Longitude',TW_TD);?></label>
                 <div class="tw-right-options"> 
                 <input type="text" class="tw-tab-text" value="<?php echo esc_attr($longitude);?>" name="single_product_settings[gmap][longitude]">
                 <p class="description"><?php esc_html_e('Please enter a latitude value for the map. Please go to',TW_TD);?> <a href="//www.latlong.net/" target="_blank"><?php esc_html_e('latitude',TW_TD);?></a><?php esc_html_e(' for the details',TW_TD);?></p>
                 </div>
            </div>
            <div class="tw-field-wrap tw-row-even">
                 <label><?php esc_html_e('Zoom Level',TW_TD);?></label>
                 <div class="tw-right-options"> 
                 <input type="text" class="tw-tab-text" value="<?php echo esc_attr($zoom_level);?>" name="single_product_settings[gmap][zoom_level]">
                 <p class="description"><?php esc_html_e('Please enter the zoom level for the map. Default zoom level is 10',TW_TD);?>
                </p>
                </div>
            </div>
        </div>
    </div>
</div>
