<?php defined('ABSPATH') or die('No script kiddies please!!'); 

$ex_shortcode= isset($single_product_settings['ex_shortcode']) && $single_product_settings['ex_shortcode']!="" ? $single_product_settings['ex_shortcode'] :"";
?>

<div class="tw-each-tab-column tw-external-sc-wrapper">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title' >
                <span class="tw_label_keyup"><?php esc_html_e('External Shortcode',TW_TD);?></span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i></span>
            </div>
        </div>
		<div class = "tw-tab-item-options clearfix" style="display:none;">
		<div class="tw_options_wrap twesc_html_external_sc_wrapper">
		     <div class="tw-field-wrap tw-row-even">
		        <label><?php esc_html_e('External Shortcode',TW_TD);?></label>
		        <div class="tw-right-options"> 
		        <input type="text" name="single_product_settings[ex_shortcode]" class="tw-tab-sc tw-tab-text" value="<?php echo esc_attr($ex_shortcode);?>"/>
		        <p class="description"><?php esc_html_e('Please fill any custom external shortcode here.',TW_TD);?></p>
		        </div>
		     </div>
		</div>
	</div>
</div>
</div>