<?php defined('ABSPATH') or die("No script kiddies please!"); ?>
<div class="tw-each-tab-column  tw-editor-wrapper">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title' >
                <span class="tw_label_keyup"><?php esc_html_e('Editor', TW_TD ) ?></span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i></span>
            </div>
        </div>
		<div class = "tw-tab-item-options clearfix" style="display:none;">
	    <div class="tw-field-wrap tw-row-even" >
			 <?php 
			   $content = (isset($single_product_settings['html_text']) && $single_product_settings['html_text'] != '') ? $single_product_settings['html_text']:'';
			   $content = wptexturize(wpautop($content));
			   $settings = array('textarea_name' => 'single_product_settings[html_text]',
			   					'media_buttons' => true, 
			   					'quicktags' => array('buttons' => 'strong,em,link,block,del,ins,img,ul,ol,li,code,close'),
			   					 'editor_class' => 'tw-html-text');

			   $editor_name_id = 'tw-html-text';
			   wp_editor($content, $editor_name_id, $settings);      
			?>
		</div>
	</div>
</div>
</div>