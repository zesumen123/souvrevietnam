<?php defined('ABSPATH') or die('No script kiddies please!!'); ?>
<div class="tw-each-tab-column tw-photo-gallery-wrapper">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title' >
                <span class="tw_label_keyup"><?php esc_html_e('Photo Gallery', TW_TD) ?></span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i></span>
            </div>
        </div>
        <div class = "tw-tab-item-options clearfix" style="display:none;">
        <div class="tw_options_wrap tw_photo_gallery_wrapper">
        	<div class="tw-field-wrap tw-row-even">
                <label><?php esc_html_e('Upload Pictures', TW_TD) ?> </label>
                <div class="tw-right-options"> 
                    <input type="button" class='button-secondary tw-upload-gallery-btn' data-key= "<?php echo $key; ?>" value='<?php _e('Upload Files', TW_TD); ?>' />

                    <div class='tw-uploaded-gallery-items'>
                        <?php if (isset($single_product_settings['gallery']) && !empty($single_product_settings['gallery'])){
                            foreach ($single_product_settings['gallery']as $image => $img) { ?>
                                <div class="tw-gallery-item-preview" >
                                <div class="tw-inner-gallery-item-preview" >

                                    <div class="tw-each-picture-actions-wrap clearfix">
                                        <a href="javascript:void(0)" class="tw-move-gallery-item"><span class="dashicons dashicons-move"></span></a>
                                        <a href="javascript:void(0)" class="tw-delete-gallery-item"><span class="dashicons dashicons-trash"></span></a>
                                    </div>
                                    <div class="tw-setting-image">
                                        <input type="hidden" id='tw-gallery-url_<?php echo $key; ?>' name='single_product_settings[gallery][]' class='tw-gallery-url tw-tab-text' value='<?php echo esc_attr( $img );?>' />
                                        <img  class="tw-picture-image" src="<?php echo esc_attr( $img ); ?>" alt="" width="250">
                                    </div>
                                </div>
                                </div>
                                <?php
                            }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>