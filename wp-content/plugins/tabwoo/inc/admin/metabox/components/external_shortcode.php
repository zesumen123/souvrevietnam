<?php defined('ABSPATH') or die('No script kiddies please!!'); 

$ex_shortcode= isset($value['ex_shortcode']) && $value['ex_shortcode']!="" ? $value['ex_shortcode'] :"";
?>
<div class="tw_options_wrap tw_external_sc_wrapper">
    <div class="tw-field-wrap">
        <label><?php _e('External Shortcode',TW_TD);?></label>
        <div class="tw-right-options"> 
        	<input type="text" name="tab_set_items[tab][<?php echo $key;?>][ex_shortcode]" class="tw-tab-sc tw-tab-text" value="<?php echo esc_attr($ex_shortcode);?>"/>
        	<p class="description"><?php _e('Please fill any custom external shortcode here.',TW_TD);?></p>
        </div>
    </div>
</div>