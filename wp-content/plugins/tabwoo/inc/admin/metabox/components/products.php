<?php defined('ABSPATH') or die('No script kiddies please!!'); 

$product_type = isset($value['product']['type']) && $value['product']['type']!="" ? $value['product']['type'] :"category";
$select_post_taxonomy = isset($value['product']['select_post_taxonomy']) && $value['product']['select_post_taxonomy']!="" ? $value['product']['select_post_taxonomy'] : "select" ;
$simple_taxonomy_terms = isset($value['product']['simple_taxonomy_terms']) && $value['product']['simple_taxonomy_terms']!="" ? $value['product']['simple_taxonomy_terms'] : "select" ;
$tw_select_orderby = isset($value['product']['tw_select_orderby']) && $value['product']['tw_select_orderby']!="" ? $value['product']['tw_select_orderby'] : "none" ;
$tw_select_order = isset($value['product']['tw_select_order']) && $value['product']['tw_select_order']!="" ? $value['product']['tw_select_order'] : "ASC" ;
$tw_post_number = isset($value['product']['tw_post_number']) && $value['product']['tw_post_number']!="" ? $value['product']['tw_post_number'] : "10" ;

?>

<div class="tw-options-wrap tw-product-wrapper">

    <div class ="tw_product_type_cat" >
        <div class="tw-field-wrap">
            <label><?php esc_html_e('Taxonomy/Category',TW_TD);?></label>
            <div class="tw-right-options"> 
                <select name="tab_set_items[tab][<?php echo $key;?>][product][select_post_taxonomy]" class="tw-select-taxonomy tw-ns-active">
                    <option value="select" <?php selected(esc_attr($select_post_taxonomy) ,'select') ?>><?php echo esc_html_e( 'Choose Taxonomy', TW_TD ); ?></option>
                    <?php
                    $product_post_type = 'product';
                    $taxonomies = get_object_taxonomies( $product_post_type, 'objects' );

                    if(is_array($taxonomies)){

                        foreach ( $taxonomies as $tax ) {
                            $tax_name = $tax -> name;
                            $label = $tax -> label;
                            ?>
                            <option value="<?php echo $tax_name; ?>" <?php selected(esc_attr($select_post_taxonomy), $tax_name) ?>><?php echo $label; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <p class="description"><?php esc_html_e( 'Please select the taxonomy here', TW_TD ); ?></p>
                <div class="tw-loader-preview" style="display:none;">
                    <img src="<?php echo TW_IMG_DIR . '/ajax-loader-add.gif' ?>">
                </div>
            </div>
        </div>
        
        <div class="tw-field-wrap">
            <label><?php esc_html_e('Product Categories',TW_TD);?></label>
            <div class="tw-right-options"> 
                <select name="tab_set_items[tab][<?php echo $key;?>][product][simple_taxonomy_terms]" id="tw-ajax" class="  tw-ns-ajax-active tw-simple-taxonomy-term">
                    <option value=""><?php echo esc_html_e( 'Choose Category', TW_TD ); ?></option>
                    <?php
                    if ( ! empty( $simple_taxonomy_terms ) ) {
                        echo $this->tw_lib -> tw_fetch_category_list( $select_post_taxonomy, $simple_taxonomy_terms );
                    }
                    ?>
                </select>
                <p class="description"><?php esc_html_e( 'Please select a category of products you want to display in this tab', TW_TD ); ?></p>

            </div>
        </div>
        
        <div class ="tw-field-wrap">
            <label><?php esc_html_e( 'OrderBy', TW_TD ); ?></label>
            <div class="tw-right-options">
                <select name="tab_set_items[tab][<?php echo $key;?>][product][tw_select_orderby]" class="tw-ns-active tw-select-orderby">
                    <option value="none" <?php selected(esc_attr($tw_select_orderby), 'none') ?>><?php esc_html_e( 'None', TW_TD ) ?></option>
                    <option value="ID" <?php selected(esc_attr($tw_select_orderby), 'ID') ?>><?php esc_html_e( 'ID', TW_TD ) ?></option>
                    <option value="author" <?php selected(esc_attr($tw_select_orderby), 'author')?>><?php esc_html_e( 'Author', TW_TD ) ?></option>
                    <option value="title" <?php selected(esc_attr($tw_select_orderby), 'title') ?>><?php esc_html_e( 'Title', TW_TD ) ?></option>
                    <option value="date" <?php selected(esc_attr($tw_select_orderby), 'date') ?>><?php esc_html_e( 'Date', TW_TD ) ?></option>
                    <option value="parent" <?php selected(esc_attr($tw_select_orderby), 'parent') ?>><?php esc_html_e( 'Parent ID', TW_TD ) ?></option>
                    <option value="rand" <?php selected(esc_attr($tw_select_orderby), 'rand') ?>><?php esc_html_e( 'Random', TW_TD ) ?></option>
                    <option value="comment_count" <?php selected(esc_attr($tw_select_orderby), 'comment_count') ?>><?php esc_html_e( 'Comment Count', TW_TD ) ?></option>

                </select>
                <p class="description"><?php esc_html_e( 'Please select a order in which you want to display your products', TW_TD ); ?></p>
            </div>
        </div>

        <div class ="tw-field-wrap">
            <label><?php esc_html_e( 'Order', TW_TD ); ?></label>
            <div class="tw-right-options">
                <select name="tab_set_items[tab][<?php echo $key;?>][product][tw_select_order]" class="tw-ns-active tw-select-order">
                    <option value="ASC" <?php selected(esc_attr($tw_select_order),'ASC') ?> ><?php esc_html_e( 'Ascending', TW_TD ) ?></option>
                    <option value="DESC" <?php selected(esc_attr($tw_select_order),'DESC') ?> ><?php esc_html_e( 'Descending', TW_TD ) ?></option>
                </select>
                <p class="description"><?php esc_html_e( 'Please select a sorting order in which you want to display your products', TW_TD ); ?></p>
            </div>
        </div>

        <div class="tw-field-wrap">
            <label><?php esc_html_e( 'Number of Post', TW_TD ); ?></label>
            <div class="tw-right-options">
                <input type="number" class="tw-post-number" min="1" name="tab_set_items[tab][<?php echo $key;?>][product][tw_post_number]"  value="<?php echo esc_attr($tw_post_number) ?>"/>
                <p class="description"><?php esc_html_e( 'Please enter the number of products you want to display in this tab', TW_TD ) ?></p>
            </div>
        </div>

        <?php include (TW_PATH . '/inc/admin/metabox/components/product/common.php'); ?>

        <?php  ?>
    </div>
</div>