<?php defined('ABSPATH') or die('No script kiddies please!!'); ?>

<div class="tw_options_wrap tw_faq_wrapper">
    <div class="tw-faq-outer-wrapper">
    	<input type="button" class="tw-add-faq-block" value="<?php esc_html_e('Add FAQ', TW_TD) ?>" data-key="<?php echo $key;?>" data-type="tab-component"> 
    	<div class="tw-faq-wrapper">
    		<?php
    		if (isset($value['faq']['question']) && !empty($value['faq']['question'])) { 
    			$array_value = 0;
                foreach ($value['faq']['question'] as $option) {
                    ?>
                    <div class="tw-faq-wrap">
                        <div class="tw-faq-inner-wrap">
                        <span class="tw-drag fa fa-arrows-alt"></span>
                        <a href="javascript:void(0)" class="tw-faq-remover"> <i class="fa fa-trash"></i> </a>

                        <?php $question = (!empty($option) ? $option : "Question"  ) ?>
                        <?php $answer = (!empty($value['faq']['answer'][$array_value]) ? $value['faq']['answer'][$array_value] : "Answer" ) ?>

                        <div class="tw-faq-question">
    						<input type="text" name="tab_set_items[tab][<?php echo $key;?>][faq][question][]" value="<?php echo esc_attr($question); ?>">
    					</div>
    					<div class="tw-faq-answer">
    						<textarea name="tab_set_items[tab][<?php echo $key;?>][faq][answer][]" ><?php echo esc_attr($answer) ?></textarea>
    					</div>
                    </div>
                    </div>
                    <?php
                    $array_value++;
                }
                
			} else {  ?>

	    		<div class="tw-faq-wrap">
                    <div class="tw-faq-inner-wrap">
	                <span class="tw-drag fa fa-arrows-alt"></span>
                    <a href="javascript:void(0)" class="tw-faq-remover"> <i class="fa fa-trash"></i> </a>

	    			<div class="tw-faq-question">
	    				<input type="text" name="tab_set_items[tab][<?php echo $key;?>][faq][question][]" value="" placeholder="Question">
	    			</div>
	    			<div class="tw-faq-answer">
	    				<textarea name="tab_set_items[tab][<?php echo $key;?>][faq][answer][]" placeholder="Answer" ></textarea>
	    			</div>
                </div>
	    		</div>
	    	<?php } ?>
    	</div>
    </div>
</div>