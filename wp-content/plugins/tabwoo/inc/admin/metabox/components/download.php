<?php defined('ABSPATH') or die('No script kiddies please!!'); ?>

<div class="tw_options_wrap tw_download_wrapper">
	<div class="tw-field-wrap">
        <label> <?php esc_html_e('Upload Files', TW_TD); ?> </label>
        <div class="tw-right-options"> 
            <input type="button" class='button-secondary tw-upload-download-btn' data-key= "<?php echo $key; ?>" value='<?php _e('Upload Files', TW_TD); ?>' data-type="tab-component" />

            <div class='tw-uploaded-files'>
                <?php if (isset($value['download']) && !empty($value['download'])){
                    foreach ($value['download']as $items => $item) { ?>
                        <div class="tw-downloadable-file-preview">
                        <div class="tw-inner-downloadable-file-preview">
                            <div class="tw-each-item-actions-wrap clearfix">
                                <a href="javascript:void(0)" class="tw-move-item"><span class="dashicons dashicons-move"></span></a>
                                <a href="javascript:void(0)" class="tw-delete-item"><span class="dashicons dashicons-trash"></span></a>
                            </div>
                            <div class="tw-setting-image">
                                <input type="hidden" name='tab_set_items[tab][<?php echo $key; ?>][download][]' class='tw-download-url tw-tab-text' value='<?php echo esc_attr( $item ); ?>' />
                                <?php 
                                    $filename= basename($item);
                                ?>
                                <span class="tw-downloadable-file"><?php echo esc_attr( $filename ); ?> </span>
                            </div>
                        </div>
                    </div>
                        <?php
                    }
                } ?>
            </div>
        </div>
    </div>
</div>