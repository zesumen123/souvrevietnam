<?php
 defined( 'ABSPATH' ) or die( "No script kiddies please!" );
$latitude = isset($value['gmap']['latitude']) && $value['gmap']['latitude']!=""? $value['gmap']['latitude'] :"";
$longitude = isset($value['gmap']['longitude']) && $value['gmap']['longitude']!=""? $value['gmap']['longitude'] :"";
$zoom_level = isset($value['gmap']['zoom_level']) && $value['gmap']['zoom_level']!=""? $value['gmap']['zoom_level'] :"";
?>

<div class="et_options_wrap tw-map-wrapper">

    <div class="tw-field-wrap">
       <label><?php esc_html_e('Latitude',TW_TD);?></label>
       <div class="tw-right-options"> 
           <input type="text" class="tw-tab-text" value="<?php echo esc_attr($latitude);?>" name="tab_set_items[tab][<?php echo $key; ?>][gmap][latitude]">
           <p class="description"><?php esc_html_e('Please enter a latitude value for the map. Please go to',TW_TD);?> <a href="//www.latlong.net/" target="_blank"><?php esc_html_e('latitude',TW_TD);?></a><?php esc_html_e(' for the details',TW_TD);?></p>
       </div>
   </div>
   <div class="tw-field-wrap">
       <label><?php esc_html_e('Longitude',TW_TD);?></label>
       <div class="tw-right-options"> 
           <input type="text" class="tw-tab-text" value="<?php echo esc_attr($longitude);?>" name="tab_set_items[tab][<?php echo $key; ?>][gmap][longitude]">
           <p class="description"><?php esc_html_e('Please enter a latitude value for the map. Please go to',TW_TD);?> <a href="//www.latlong.net/" target="_blank"><?php esc_html_e('latitude',TW_TD);?></a><?php esc_html_e(' for the details',TW_TD);?></p>
       </div>
   </div>
   <div class="tw-field-wrap">
        <label><?php esc_html_e('Zoom Level',TW_TD);?></label>
        <div class="tw-right-options"> 
           <input type="text" class="tw-tab-text" value="<?php echo esc_attr($zoom_level);?>" name="tab_set_items[tab][<?php echo $key; ?>][gmap][zoom_level]">
           <p class="description"><?php esc_html_e('Please enter the zoom level for the map. Default zoom level is 10',TW_TD);?>
            </p>
        </div>
</div>
</div>