<?php defined('ABSPATH') or die('No script kiddies please!!'); 
$custom_link_url = (isset($value['clink']['custom_link_url']) && $value['clink']['custom_link_url'] != '')?$value['clink']['custom_link_url']:'';
$link_target = (isset($value['clink']['custom_link_target']) && $value['clink']['custom_link_target'] != '')?esc_attr($value['clink']['custom_link_target']):'';
?>
<div class = "tw-options_wrap tw-clink_wrapper">
    <div class = "tw-field-wrap">
        <label><?php esc_html_e('Custom Link',TW_TD);?></label>
        <div class = "etab-right-options"> 
            <input type="text" name="tab_set_items[tab][<?php echo $key;?>][clink][custom_link_url]" class="tw-tab-text tw-clink-url" value="<?php echo esc_attr($custom_link_url);?>"/>
            <p class = "description"><?php esc_html_e( 'Please enter custom link for this tab', TW_TD ) ?></p>
        </div>
    </div>
	<div class="tw-field-wrap">
        <label><?php esc_html_e('Link Target',TW_TD);?></label>
        <div class="etab-right-options"> 
            <select class="tw-ns-active" name="tab_set_items[tab][<?php echo $key; ?>][clink][custom_link_target]">
                <option value="_blank" <?php selected(esc_attr($link_target),"_blank");?>> _blank </option>
                <option value="_parent" <?php selected(esc_attr($link_target),"_parent");?>> _parent </option>
                <option value="_self" <?php selected(esc_attr($link_target),"_self");?>> _self </option>
                <option value="_top" <?php selected(esc_attr($link_target),"_top");?>> _top </option>      
            </select>
            <p class="description"><?php esc_html_e( 'Please select a link target for this tab', TW_TD ) ?></p>
        </div>
    </div>
</div>