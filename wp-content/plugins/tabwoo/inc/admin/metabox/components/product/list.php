<?php

defined('ABSPATH') or die('No script kiddies please!!');

$img_position = isset($value['product']['list']['img_position']) && $value['product']['list']['img_position']!="" ? $value['product']['list']['img_position'] : "left" ;
?>
<label><?php esc_html_e( 'Image Position', TW_TD ); ?></label>
<div class="tw-right-options">
	<input type="radio" id="tw-list-image-left" name="tab_set_items[tab][<?php echo $key;?>][product][list][img_position]" value="left" <?php if(esc_attr($img_position) == "left") echo "checked";?>> 
	<label for="tw-list-image-left" ><?php esc_html_e('Left',TW_TD);?></label>
	<input type="radio" id="tw-list-image-right"  name="tab_set_items[tab][<?php echo $key;?>][product][list][img_position]" value="right" <?php if(esc_attr($img_position) == "right") echo "checked";?>> 
	<label  for="tw-list-image-right" ><?php esc_html_e('Right',TW_TD);?></label>
	<p class="description"><?php esc_html_e( 'Please select a position for product image', TW_TD ) ?></p>
</div>