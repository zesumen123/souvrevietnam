<?php 
defined('ABSPATH') or die('No script kiddies please!!'); 
?>
<div class="tw-field-wrap tw-inner-row" id="tw-html-<?php echo $key;?>">
 <?php 
   $content = (isset($value['html_text']) && $value['html_text'] != '')?$value['html_text']:'';
   $content = wptexturize(wpautop($content));
   $settings = array('textarea_name' => 'tab_set_items[tab]['.$key.'][html_text]',
   					'media_buttons' => true, 
   					'quicktags' => array('buttons' => 'strong,em,link,block,del,ins,img,ul,ol,li,code,close'),
   					 'editor_class' => 'tw-html-text-' . $key);

   $editor_name_id = 'tw-html-text-' . $key;
   wp_editor($content, $editor_name_id, $settings);      
?>
</div>
<input type="hidden" class="tw_key_unique" value="<?php echo $key;?>"/>