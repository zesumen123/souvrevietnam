<?php defined('ABSPATH') or die('No script kiddies please!!'); 

$video_type = isset($value['video']['type']) && $value['video']['type']!="" ? $value['video']['type'] :"";
$video_youtube = isset($value['video']['youtube']) && $value['video']['youtube']!="" ? $value['video']['youtube'] :"";
$video_viemo = isset($value['video']['viemo']) && $value['video']['viemo']!="" ? $value['video']['viemo'] :"";

?>
<div class="tw_options_wrap tw-video-gallery-wrapper">
	<div class="tw-field-wrap">
		<label> <?php esc_html_e('Video type', TW_TD ); ?></label>
		<div class="tw-right-options"> 
			<select id='tw-background-video-type' name='tab_set_items[tab][<?php echo $key;?>][video][type]' class='tw-ns-active tw-video-select-option'>
				<option value='youtube' <?php selected($video_type, 'youtube') ?> > <?php esc_html_e('Youtube', TW_TD); ?> </option>
				<option value='viemo' <?php  selected($video_type, 'viemo') ?> > <?php esc_html_e('Viemo', TW_TD); ?> </option>
			</select>
			<p class="description"><?php esc_html_e('Note: All the videos available for a particular product will be displayed in this tab. ',TW_TD);?></p>
		</div>
	</div>

	<div class='tw-field-wrap tw-video tw-youtube' <?php echo ($video_type == 'viemo')? "style='display:none;'" :'' ?>>
		<label><?php esc_html_e('Youtube Video URL', TW_TD); ?></label>
		<div class="tw-right-options">
		<input id='tw-background-video-youtube' type="url" name='tab_set_items[tab][<?php echo $key;?>][video][youtube]' value='<?php echo esc_attr($video_youtube);?>'/>
		<p class="description"><?php esc_html_e('Please enter a youtube video url here. Note: Make sure that the youtube url is a embeded url to make sure the video is displayed in the tab. ',TW_TD);?></p>
		</div>
	</div>

	<div class="tw-field-wrap tw-video tw-viemo" <?php echo ($video_type == 'youtube')? "style='display:none;'" :'' ?>>
		<label><?php esc_html_e( 'Viemo Video URL', TW_TD ); ?></label>
		<div class="tw-right-options">
		<input id='tw-background-video-viemo' type="url" name='tab_set_items[tab][<?php echo $key;?>][video][viemo]' value='<?php echo esc_attr($video_viemo);?>' />
		<p class="description"><?php esc_html_e('Please enter a Vimeo video url here ',TW_TD);?></p>
		</div>
	</div>
</div>