<?php defined('ABSPATH') or die('No script kiddies please!!'); ?>

<div class="tw_options_wrap tw_photo_gallery_wrapper">
	<div class="tw-field-wrap">
        <label> <?php esc_html_e('Upload Pictures', TW_TD) ?> </label>
        <div class="tw-right-options"> 
            <input type="button" class='button-secondary tw-upload-gallery-btn'  data-type="tab-component" data-key= "<?php echo $key; ?>" value='<?php esc_html_e('Upload Files', TW_TD); ?>' />

            <div class='tw-uploaded-gallery-items'>
                <?php if (isset($value['gallery']) && !empty($value['gallery'])){
                    foreach ($value['gallery']as $image => $img) { ?>
                        <div class="tw-gallery-item-preview" >
                        <div class="tw-inner-gallery-item-preview" >
                            <div class="tw-each-picture-actions-wrap clearfix">
                                <a href="javascript:void(0)" class="tw-move-gallery-item"><span><i class="fa fa-arrows-alt"></i></span></a>
                                <a href="javascript:void(0)" class="tw-delete-gallery-item"><span><i class="fa fa-trash"></i></span></a>
                            </div>
                            <div class="tw-setting-image">
                                <input type="hidden" id='tw-gallery-url_<?php echo $key; ?>' name='tab_set_items[tab][<?php echo $key; ?>][gallery][]' class='tw-gallery-url tw-tab-text' value='<?php echo esc_attr( $img );?>' />
                                <img  class="tw-picture-image" src="<?php echo esc_attr( $img ); ?>" alt="" width="250">
                            </div>
                            </div>
                        </div>
                        <?php
                    }
                } ?>
            </div>
        </div>
    </div>
</div>