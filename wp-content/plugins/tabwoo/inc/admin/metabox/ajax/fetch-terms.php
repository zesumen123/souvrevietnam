<?php
defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$select_tax = sanitize_text_field($_POST['select_tax']);

if ($select_tax == 'select') {
    ?>
    <option value = "select" ><?php echo esc_html_e('Choose Terms', TW_TD);
    ?></option>
    <?php
} else {
    ?>
    <option value = "select" ><?php echo esc_html_e('Choose Term', TW_TD); ?></option>
    <?php
    echo $this->tw_lib->tw_fetch_category_list( $select_tax, $psfw_option[ 'simple_taxonomy_terms' ] );
}
