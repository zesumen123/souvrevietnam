<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );

global $wpdb;
$db_table_name = $wpdb->prefix . "posts";


$query = " SELECT * FROM $db_table_name WHERE post_type = 'product' AND post_status = 'publish' AND post_title LIKE '%". $search ."%'" ;
if ( isset($exclude) && !empty($exclude)) {
	$query = $query . " AND ID NOT IN (".implode(",",$exclude).")";
}
$tw_products = $wpdb->get_results($query);

$product_list = array ();
if (!empty($tw_products)) {
	foreach ($tw_products as $product => $p) {
		$product_list[] = array (
			'value' => $p->ID,
	       	'label' => $p->post_title,
		);
	}
}else{
	$product_list[] = array (
		'value'=> "",
		'label' => "No result found",
	);
}

echo json_encode($product_list);
