<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" ); ?>
<div class="tw-gallery-item-preview ">
    <div class="tw-each-picture-actions-wrap clearfix">
        <a href="javascript:void(0)" class="tw-move-gallery-item"><span class="dashicons dashicons-move"></span></a>
        <a href="javascript:void(0)" class="tw-delete-gallery-item"><span class="dashicons dashicons-trash"></span></a>
    </div>
    <div class="tw-setting-image">
    	<?php if($type== 'tab-component'){ ?>
    		<?php /* For Tab Settings Component */ ?>
        	<input type="hidden" name='tab_set_items[tab][<?php echo $key; ?>][gallery][]' class='tw-gallery-url tw-tab-text' value='<?php echo esc_attr( $image_url ); ?>' />
        <?php }else{?>
        	<?php /* For Product Page Metabox */ ?>
        	<input type="hidden" name='single_product_settings[gallery][]' class='tw-gallery-url tw-tab-text' value='<?php echo esc_attr( $image_url ); ?>' />
    	<?php }?>
        <img  class="tw-picture-image" src="<?php echo esc_attr( $image_url ); ?>" alt="" width="250">
    </div>
</div>