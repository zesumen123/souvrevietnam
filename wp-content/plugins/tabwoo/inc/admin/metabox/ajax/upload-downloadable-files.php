<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" ); ?>
<div class="tw-downloadable-file-preview">
    <div class="tw-each-item-actions-wrap clearfix">
        <a href="javascript:void(0)" class="tw-move-item"><span class="dashicons dashicons-move"></span></a>
        <a href="javascript:void(0)" class="tw-delete-item"><span class="dashicons dashicons-trash"></span></a>
    </div>
    <div class="tw-setting-image">
    	<?php if ($type == 'tab-component'){ ?>
            <?php /* For Tab Settings Component */ ?>
        	<input type="hidden" name='tab_set_items[tab][<?php echo $key; ?>][download][]' class='tw-download-url tw-tab-text' value='<?php echo esc_attr( $file_url ); ?>' />
    	<?php }else{ ?>
            <?php /* For Product Page Metabox */ ?>
    		<input type="hidden" name='single_product_settings[download][]' class='tw-download-url tw-tab-text' value='<?php echo esc_attr( $file_url ); ?>' />
    	<?php } ?>
		<span class="tw-downloadable-file"><?php echo esc_attr( $file_name ); ?> </span>
    </div>
</div>