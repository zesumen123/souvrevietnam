<?php
defined('ABSPATH') or die("No script kiddies please!");

$postid = $post->ID;
$tw_tab_settings = get_post_meta($postid, 'tw_tab_settings', true);

?>
<div class="tw-tab-main-wrap">
    <div class="tw-add-tab-wrap">
        <input type = "button" class = "button-secondary tw-add-tab-button" value = "<?php _e( 'Add Tab Item', TW_TD ); ?>" data-action = 'add_item'>
        <span class="tw-loader-image" style="display:none;" ><img src='<?php echo TW_IMG_DIR.'/ajax-loader-add.gif'; ?>' alt='Loading...'/></span>
        <div class="tw-add-append-wrap clearfix">
            <?php 
            	if (isset($tw_tab_settings['tab']) && !empty($tw_tab_settings['tab'])) {
				    foreach ($tw_tab_settings['tab'] as $key => $value) {
				         if ($key == 'description' || $key == 'additional_information' || $key == 'reviews') {
				        //if ($key == 'descp' || $key == 'additional_info' || $key == 'review') {
				            include(TW_PATH.'/inc/admin/metabox/default-tabs/tabs/'. $key .'.php'); 
				        } else {
				            include(TW_PATH.'/inc/admin/metabox/custom-tab/individual_tab_settings.php'); 
				        }
				    }
				} else {
				    include(TW_PATH.'/inc/admin/metabox/default-tabs/default_tab.php'); 
				    $key = $this->tw_lib->generateRandomIndex();
				    include(TW_PATH.'/inc/admin/metabox/custom-tab/individual_tab_settings.php'); 
				}
            ?>
        </div>
    </div>
</div>