<?php
global $post;

$postid = $post->ID;
$tw_tab_settings = get_post_meta($postid, 'tw_tab_settings', true);

//$this->tw_lib->print_array($tw_tab_settings);

$basis = (isset($tw_tab_settings['assigned_to']['basis']) && $tw_tab_settings['assigned_to']['basis'] != '') ? esc_attr($tw_tab_settings['assigned_to']['basis']):'category';

$category = (isset($tw_tab_settings['assigned_to']['category']) && $tw_tab_settings['assigned_to']['category'] != '') ? $tw_tab_settings['assigned_to']['category']:array();

$tag = (isset($tw_tab_settings['assigned_to']['tag']) && $tw_tab_settings['assigned_to']['tag'] != '') ? $tw_tab_settings['assigned_to']['tag']:array();

$product = (isset($tw_tab_settings['assigned_to']['product']) && $tw_tab_settings['assigned_to']['product'] != '') ? $tw_tab_settings['assigned_to']['product']:array();

?>

<div class="tw-each-tab-column">
	<div class="tw-tab-item-inner">
		<div class="tw-field-wrap tw-row-even">
			<label><?php esc_html_e( 'Assign Tab', TW_TD ); ?></label>
			<div class="tw-right-options">
				<select name="tab_set_items[assigned_to][basis]" class="tw-ns-active tw-assign-to">
					<option value="category" <?php selected(esc_attr($basis), 'category'); ?> ><?php esc_html_e( 'Based On Categories', TW_TD ) ?></option>
					<option value="tag" <?php selected(esc_attr($basis), 'tag'); ?> ><?php esc_html_e( 'Based On Product Tags', TW_TD ) ?></option>
					<option value="product" <?php selected(esc_attr($basis), 'product'); ?> ><?php esc_html_e( 'Based On Products', TW_TD ) ?></option>
				</select>
				<p class="description">
                    <?php esc_html_e('Please select which products, product tag or product categories you want this particular tab set to be assigned to. NOTE: Please make sure that you do not assign the same product/ product categories to multiple tab sets. ',TW_TD);?>               
                </p>
			</div>
		</div>
		<div class="tw-category tw-assign tw-row-odd" <?php echo (esc_attr($basis) == 'category') ? "style='display:block;'":"style='display:none;'" ; ?> >
			<div class="tw-field-wrap tw-cat-show ">
		        <label><?php esc_html_e('Product Categories',TW_TD);?></label>
		        <div class="tw-right-options"> 
		            <select name="tab_set_items[assigned_to][category][]" multiple class="tw-ns-active tw-simple-taxonomy-term">
		                <option value=""><?php echo esc_html_e( 'Choose Category', TW_TD ); ?></option>
		                <?php
		                	echo $this->tw_lib -> tw_fetch_category_list( 'product_cat', $category);
		                ?>
		            </select>
		            <p class="description"><?php esc_html_e( 'Please select categories of product you want to display in this tab', TW_TD ); ?></p>

		        </div>
		    </div>
		</div>
		<div class="tw-tag tw-assign tw-row-odd" <?php echo (esc_attr($basis) == 'tag') ? "style='display:block;'":"style='display:none;'" ; ?> >
			<div class="tw-field-wrap tw-tag-show ">
		        <label><?php esc_html_e('Product Tags',TW_TD);?></label>
		        <div class="tw-right-options"> 
		            <select name="tab_set_items[assigned_to][tag][]" multiple class="tw-ns-active tw-simple-taxonomy-term">
		                <option value=""><?php echo esc_html_e( 'Choose Product Tag', TW_TD ); ?></option>
		                <?php
		                	echo $this->tw_lib -> tw_fetch_category_list( 'product_tag', $category );
		                ?>
		            </select>
		            <p class="description"><?php esc_html_e( 'Please select categories of product you want to display in this tab', TW_TD ); ?></p>

		        </div>
		    </div>
		</div>
		<div class="tw-product tw-assign tw-row-odd" <?php echo (esc_attr($basis) == 'product') ? "style='display:block;'":"style='display:none;'" ; ?>>
			<div class="tw-field-wrap tw-pro-show" >
		        <label><?php esc_html_e('Products',TW_TD);?></label>
		        <div class="tw-right-options"> 
					<input type = 'text' id='tw_search_product' placeholder='Enter your product name here'>
					<div class="tw-product-append">
					<?php
						if(isset($product) && !empty($product)){
							foreach ($product as $key => $value) {  ?>
								<div class = "tw-ind-product">
		                            <span class="tw-pro-label"> 
		                            	<?php echo get_the_title( $value ); ?>
		                            </span>
		                            <span class="tw-del-pro">Del</span>
		                            <input type="hidden" name="tab_set_items[assigned_to][product][]" value="<?php echo esc_attr($value); ?>">
			                    </div>
								<?php 
							}
						}
					?>
					</div>
		            <p class="description"><?php esc_html_e( 'Please enter name of the product that you want this tab set to be assigned to here.', TW_TD ); ?></p>
		        </div>
		    </div>
		</div>
	</div>
</div>