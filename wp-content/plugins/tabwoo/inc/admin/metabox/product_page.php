<?php
defined('ABSPATH') or die("No script kiddies please!");
global $post;
$postid = $post -> ID;
$single_product_settings = get_post_meta($postid, 'single_product_settings', true);

?>
<div class="tw-single-product-wrapper">

	<div class="tw-note"> 
		<p> <?php esc_html_e('Note: The contents you have set for the respective tab here will not be displayed in your product page unless, this product has been assigned a tab set. To assign this product or the category this product falls under to a tab set, please go to our plugin settings.', TW_TD); ?></p>
	</div>
	
<?php 

$tabs = array('map', 'external_shortcode', 'photo_gallery', 'custom_link', 'editor', 'video_gallery', 'download', 'faq', 'products');

foreach ($tabs as $key => $tab) {
	include(TW_PATH . '/inc/admin/metabox/product_metabox/'.$tab.'.php'); 
}

?>

</div>