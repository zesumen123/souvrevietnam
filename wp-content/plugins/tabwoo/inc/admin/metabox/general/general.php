<?php defined('ABSPATH') or die("No script kiddies please!"); 

$display_format = (isset($tw_tab_settings['main']['general_settings']['display_format_type']) && $tw_tab_settings['main']['general_settings']['display_format_type'] != '')?$tw_tab_settings['main']['general_settings']['display_format_type']:'show_title_only';

$icon_position  =(isset($tw_tab_settings['main']['general_settings']['icon_position']) && $tw_tab_settings['main']['general_settings']['icon_position'] != '')?$tw_tab_settings['main']['general_settings']['icon_position']:'left';

$column  = (isset($tw_tab_settings['main']['general_settings']['column']) && $tw_tab_settings['main']['general_settings']['column'] != '')?intval($tw_tab_settings['main']['general_settings']['column']):'3';

$column_width  = (isset($tw_tab_settings['main']['general_settings']['column_width']) && $tw_tab_settings['main']['general_settings']['column_width'] != '')?intval($tw_tab_settings['main']['general_settings']['column_width']):'150';

$enable_accordion = (isset($tw_tab_settings['main']['general_settings']['enable_accordion']) && $tw_tab_settings['main']['general_settings']['enable_accordion'] == true)?true :false;

?>
<div class="tw-tab-cbody">
    
    <div class="tw-field-wrap tw-row-even">
        <label><?php esc_html_e('Tab Labels Display Format',TW_TD);?></label>
        <div class="tw-right-options">
            <div class="tw-display-format">
                <input type="radio" id="tw_show_title_only" name="tab_set_items[main][general_settings][display_format_type]" value="show_title_only" <?php if(esc_attr($display_format) == "show_title_only") echo "checked";?>>
                <label for="tw_show_title_only"><?php esc_html_e('Show Tab title only',TW_TD);?></label>
            </div>
            <div class="tw-display-format">
                <input type="radio" id="tw_show_both" name="tab_set_items[main][general_settings][display_format_type]" value="show_both" <?php if(esc_attr($display_format) == "show_both") echo "checked";?>> 
                <label for="tw_show_both"><?php esc_html_e('Show icon and title both',TW_TD);?></label>
            </div>
            <div class="tw-display-format">
                <input type="radio" id="tw_show_icon_only" name="tab_set_items[main][general_settings][display_format_type]" value="show_icon_only" <?php if(esc_attr($display_format) == "show_icon_only") echo "checked";?>> 
                <label for="tw_show_icon_only"><?php esc_html_e('Show icon only',TW_TD);?></label>
            </div>
            <p class="description">
                <?php esc_html_e('Please select how you want your tab set to be displayed',TW_TD);?>
            </p>
        </div>
    </div>
    <div class="tw-field-wrap tw-row-odd">
        <label><?php esc_html_e('Show Icon Position',TW_TD);?></label>
        <div class="tw-right-options"> 
            <select name="tab_set_items[main][general_settings][icon_position]" id="tw_icon_position" class="tw-ns-active ">
                <option value="left" <?php selected(esc_attr($icon_position), 'left'); ?>><?php esc_html_e('Left of Tab Title',TW_TD);?></option>
                <option value="right" <?php selected(esc_attr($icon_position), 'right'); ?>><?php esc_html_e('Right of Tab Title',TW_TD);?></option>
                <option value="top" <?php selected(esc_attr($icon_position), 'top'); ?>><?php esc_html_e('Top of Tab Title',TW_TD);?></option>
                <option value="bottom" <?php selected(esc_attr($icon_position), 'bottom'); ?>><?php esc_html_e('Bottom of Tab Title',TW_TD);?></option>
            </select>
            <p class="description">
                <?php esc_html_e('Please select an icon position for this tab set',TW_TD);?>
            </p>
        </div>
    </div>
    <div class="tw-field-wrap tw-row-odd">
        <label><?php esc_html_e('Tab Width',TW_TD);?></label>
        <div class="tw-right-options"> 
            <select name="tab_set_items[main][general_settings][column]" class="tw-ns-active tw-tab-width">
                <option value="1" <?php selected(esc_attr($column), 1); ?>><?php esc_html_e('Automatic Width Calculation',TW_TD);?></option>
                <option value="2" <?php selected(esc_attr($column), 2); ?>><?php esc_html_e('Same width tabs',TW_TD);?></option>
                <option value="3" <?php selected(esc_attr($column), 3); ?>><?php esc_html_e('Tabset width 100 percent',TW_TD);?></option>
            </select>
            <p class="description"><?php esc_html_e('Set width for the individual tabs as per your requirement.',TW_TD);?></p>
        </div>
    </div>
    <div class="tw-field-wrap tw-width-value tw-row-odd" style="<?php $column == 2?'display:block': 'display:none'; ?>">
        <label><?php esc_html_e('Width',TW_TD);?></label>
        <div class="tw-right-options"> 
            <input type="number" name="tab_set_items[main][general_settings][column_width]" value="<?php echo esc_attr($column_width); ?>" >
            <p class="description"><?php esc_html_e('Set width for the individual tabs in px.',TW_TD);?></p>
        </div>
    </div>
    <div class="tw-field-wrap tw-row-even">
        <label for="enable_accordion"><?php esc_html_e('Enable Accordion For Responsive',TW_TD);?></label>
        <div class="tw-right-options">
            <label class="tw-switch">
                <input type="checkbox" value="true" id="enable_accordion" name="tab_set_items[main][general_settings][enable_accordion]" <?php if( $enable_accordion ){echo 'checked';}?>>
                <div class="tw-check round"></div>
            </label>
            <p class="description"><?php esc_html_e('Please check to enable accordion for responsive. Note: If this option is enabled, then on mobile version tab will work in accordion.',TW_TD);?></p>
        </div>
    </div>
</div>