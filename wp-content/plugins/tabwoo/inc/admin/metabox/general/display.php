<?php
defined('ABSPATH') or die("No script kiddies please!"); 

$orientation = (isset($tw_tab_settings['main']['display_settings']['orientation_type']) && $tw_tab_settings['main']['display_settings']['orientation_type'] == 'vertical') ? 'vertical' : 'horizontal';
$h_template = (isset($tw_tab_settings['main']['display_settings']['h_template']) && $tw_tab_settings['main']['display_settings']['h_template'] !='') ? $tw_tab_settings['main']['display_settings']['h_template'] : 'template1';
$v_template = (isset($tw_tab_settings['main']['display_settings']['v_template']) && $tw_tab_settings['main']['display_settings']['v_template'] !='') ? $tw_tab_settings['main']['display_settings']['v_template'] : 'template1';

?>

<div class="tw-tab-cbody">

    <div class="tw-field-wrap tw-row-even">
        <label><?php esc_html_e('Orientation Type', TW_TD);?></label>
        <div class="tw-right-options"> 
            <select  class= "tw-ns-active tw_orientation_type" name="tab_set_items[main][display_settings][orientation_type]" id="tw_orientation_type">
                <option value = "horizontal" <?php selected(esc_attr($orientation), 'horizontal'); ?>> <?php esc_html_e('Horizontal', TW_TD);?> </option>
                <option value = "vertical" <?php selected(esc_attr($orientation), 'vertical'); ?>> <?php esc_html_e('Vertical', TW_TD);?> </option>
            </select>
            <p class="description">
                <?php esc_html_e('Please select an orientation for this tab set', TW_TD);?>
            </p>
        </div>
    </div>
    <div class="tw-o-horizontal tw-orient tw-row-odd" <?php echo (esc_attr($orientation) == "vertical") ? "style='display:none;'" : '' ?>>
        <div class="tw-field-wrap">
            <label><?php esc_html_e('Choose Horizontal Tab Template', TW_TD);?></label>
            <div class="tw-right-options"> 
                <select name="tab_set_items[main][display_settings][h_template]" id="tw_template_type" class="tw-ns-active tw-hor-template ">
                    <?php for($i = 1; $i <= 10; $i++){?>
                        <option value="template<?php echo $i;?>" <?php selected(esc_attr($h_template), 'template'.$i); ?>><?php esc_html_e('Template '.$i,TW_TD);?></option>
                    <?php }?>
                </select> 
                <p class="description">
                    <?php esc_html_e('Please select a template for this tab set', TW_TD);?>
                </p>
            </div>
        </div>
        <div class="tw-field-wrap">
            <label><?php esc_html_e('Template Preview', TW_TD);?></label>
            <div class="tw_hor-template-preview">
                <?php for($i = 1; $i <= 10; $i++){
                    if(esc_attr($h_template) != 'template'.$i){
                        $style = 'style="display:none;"';
                    }else{
                        $style = '';
                    }?>
                    <div class="tw-horizontal-template-preview" id="<?php echo 'tw-hor-template'.$i;?>" <?php echo $style;?>>
                        <?php esc_html_e('Preview', TW_TD);?>
                        <img src="<?php echo TW_IMG_DIR;?>/tab_templates/horizontal/template<?php echo $i;?>.jpg"/>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
    <div class="tw-o-vertical tw-orient tw-row-odd" <?php echo (esc_attr($orientation) == "horizontal") ? "style='display:none;'" : '' ?>>
        <div class="tw-field-wrap">
            <label><?php esc_html_e('Choose Vetical Tab Template', TW_TD);?></label>
            <div class="tw-right-options"> 
                <select name="tab_set_items[main][display_settings][v_template]" id="tw_template_type" class="tw-ns-active tw-ver-template">
                    <?php for($i = 1; $i <= 10; $i++){?>
                        <option value="template<?php echo $i;?>" <?php selected(esc_attr($v_template), 'template'.$i); ?>><?php esc_html_e('Template '.$i,TW_TD);?></option>
                    <?php }?>
                </select> 
                <p class="description">
                    <?php esc_html_e('Please select a template for this tab set', TW_TD);?>
                </p>
            </div>
        </div>
        <div class="tw-field-wrap">
            <label><?php esc_html_e('Template Preview', TW_TD);?></label>
            <div class="tw_hor-template-preview">
                <?php for ($i = 1; $i <= 10; $i++) {
                    if (esc_attr($v_template) != 'template'.$i) {
                        $style = 'style="display:none;"';
                    } else {
                        $style = '';
                    }?>
                    <div class="tw-vertical-template-preview" id="<?php echo 'tw-ver-template'.$i;?>" <?php echo $style;?>>
                        <?php esc_html_e('Preview', TW_TD);?>
                        <img src="<?php echo TW_IMG_DIR;?>/tab_templates/vertical/template<?php echo $i;?>.jpg"/> 
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>