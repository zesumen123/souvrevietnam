<?php defined('ABSPATH') or die('No script kiddies please!!');

$enable_custom_option = (isset($tw_tab_settings['main']['custom_settings']['enable_custom_option']) && $tw_tab_settings['main']['custom_settings']['enable_custom_option'] == true) ? true : false;

// Normal
$bg_color = (isset($tw_tab_settings['main']['custom_settings']['bg_color']) && $tw_tab_settings['main']['custom_settings']['bg_color'] != '') ? $tw_tab_settings['main']['custom_settings']['bg_color'] : '';
$font_color = (isset($tw_tab_settings['main']['custom_settings']['font_color']) && $tw_tab_settings['main']['custom_settings']['font_color'] != '')?$tw_tab_settings['main']['custom_settings']['font_color'] : '';

// Hover
$bg_hover_color = (isset($tw_tab_settings['main']['custom_settings']['bg_hover_color']) && $tw_tab_settings['main']['custom_settings']['bg_hover_color'] != '') ? $tw_tab_settings['main']['custom_settings']['bg_hover_color'] : '';
$font_hover_color = (isset($tw_tab_settings['main']['custom_settings']['font_hover_color']) && $tw_tab_settings['main']['custom_settings']['font_hover_color'] != '') ? $tw_tab_settings['main']['custom_settings']['font_hover_color'] : '';

// Active
$bg_active_color = (isset($tw_tab_settings['main']['custom_settings']['bg_active_color']) && $tw_tab_settings['main']['custom_settings']['bg_active_color'] != '') ? $tw_tab_settings['main']['custom_settings']['bg_active_color'] : '';
$font_active_color = (isset($tw_tab_settings['main']['custom_settings']['font_active_color']) && $tw_tab_settings['main']['custom_settings']['font_active_color'] != '') ? $tw_tab_settings['main']['custom_settings']['font_active_color'] : '';

// OTHERES

$bg_tab_content_color  = (isset($tw_tab_settings['main']['custom_settings']['bg_tab_content_color']) && $tw_tab_settings['main']['custom_settings']['bg_tab_content_color'] != '') ? $tw_tab_settings['main']['custom_settings']['bg_tab_content_color'] : '';

$font_family = (isset($tw_tab_settings['main']['custom_settings']['font_family']) && $tw_tab_settings['main']['custom_settings']['font_family'] != '') ? $tw_tab_settings['main']['custom_settings']['font_family'] : '';
$font_size = (isset($tw_tab_settings['main']['custom_settings']['font_size']) && $tw_tab_settings['main']['custom_settings']['font_size'] != '') ? $tw_tab_settings['main']['custom_settings']['font_size'] : '';
?>
<div class="tw-tab-cbody">
    <div class="tw-field-wrap tw-row-even">
        <label for="enable_custom_option"><?php esc_html_e('Enable Custom Styling',TW_TD);?></label>
        <div class="tw-right-options"> 
            <label class="tw-switch">
                <input type="checkbox" value="true" id="enable_custom_option" name="tab_set_items[main][custom_settings][enable_custom_option]" <?php if( $enable_custom_option ){echo 'checked';}?>>
                <div class="tw-check round"></div>
            </label>
            <p class="description">
                <?php esc_html_e('Enable below custom styling for this tab.',TW_TD);?>
            </p>
        </div>
    </div>
    <div class="tw-field-wrap tw-row-odd">
        <label><?php esc_html_e('Tab Background Color',TW_TD);?></label>
        <div class="tw-right-options"> 
            <input type="text" name="tab_set_items[main][custom_settings][bg_color]" class="tw-color-picker" data-alpha="true" value="<?php echo esc_attr($bg_color);?>"/>
        </div>
    </div>
    <div class="tw-field-wrap tw-row-even">
        <label><?php esc_html_e('Tab Title Font Color',TW_TD);?></label>
        <div class="tw-right-options"> 
            <input type="text" name="tab_set_items[main][custom_settings][font_color]" class="tw-color-picker" value="<?php echo esc_attr($font_color);?>"/>
        </div>
    </div>

    <div class="tw-field-wrap tw-row-even">
        <label><?php esc_html_e('Tab Hover Background Color',TW_TD);?></label>
        <div class="tw-right-options"> 
            <input type="text" name="tab_set_items[main][custom_settings][bg_hover_color]" class="tw-color-picker" data-alpha="true" value="<?php echo esc_attr($bg_hover_color);?>"/>
        </div>
    </div>

    <div class="tw-field-wrap tw-row-odd">
        <label><?php esc_html_e('Tab Title Font Hover Color',TW_TD);?></label>
        <div class="tw-right-options"> 
            <input type="text" name="tab_set_items[main][custom_settings][font_hover_color]" class="tw-color-picker" value="<?php echo esc_attr($font_hover_color);?>"/>
        </div>
    </div>

    <div class="tw-field-wrap tw-row-odd">
        <label><?php esc_html_e('Active Tab Background Color',TW_TD);?></label>
        <div class="tw-right-options"> 
            <input type="text" name="tab_set_items[main][custom_settings][bg_active_color]" class="tw-color-picker" data-alpha="true" value="<?php echo esc_attr($bg_active_color);?>"/>
        </div>
    </div>

    <div class="tw-field-wrap tw-row-odd">
        <label><?php esc_html_e('Active Tab Title Font Color',TW_TD);?></label>
        <div class="tw-right-options"> 
            <input type="text" name="tab_set_items[main][custom_settings][font_active_color]" class="tw-color-picker" value="<?php echo esc_attr($font_active_color);?>"/>
        </div>
    </div>
    
    <div class="tw-field-wrap tw-row-odd">
        <label><?php esc_html_e('Tab Content Background Color', TW_TD);?></label>
        <div class="tw-right-options"> 
            <input type="text" name="tab_set_items[main][custom_settings][bg_tab_content_color]" class="tw-color-picker" data-alpha="true" value="<?php echo esc_attr($bg_tab_content_color);?>"/>
        </div>
    </div>
    <div class="tw-field-wrap tw-row-even">
        <label><?php esc_html_e('Font Size', TW_TD);?></label>
        <div class="tw-right-options"> 
            <input type="number" name="tab_set_items[main][custom_settings][font_size]" data-alpha="true" value="<?php echo esc_attr($font_size);?>"/>
        </div>
    </div>
    <div class="tw-field-wrap tw-row-odd">
        <label><?php esc_html_e( 'Font Family ', TW_TD ); ?></label>
        <div class="tw-right-options"> 
            <select name='tab_set_items[main][custom_settings][font_family]' class="">
                <option value ><?php esc_html_e( 'Default', TW_TD ); ?></option>
                <?php
                include( TW_PATH . '/inc/admin/metabox/general/font-family.php' ); 

                foreach ( $tw_variables['google-fonts'] as $key1 => $value1 ) { ?>
                <option value='<?php echo $value1; ?>' <?php  selected( esc_attr($font_family), $value1 ) ?>  style = 'font-family:"<?php echo $value1 ?>" ;'><?php echo $value1; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
</div>
