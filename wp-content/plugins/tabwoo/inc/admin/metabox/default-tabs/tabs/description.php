<?php  defined( 'ABSPATH' ) or die( "No script kiddies please!" );
 
$key = "description";
//$key = "descp";
$enable_tab = isset($value['enable_tab']) && $value['enable_tab']== "yes" ? true : false; 
$tab_label = isset($value['tab_label']) && $value['tab_label'] != " " ? $value['tab_label'] : "Description"; 
$tab_icon_type = (isset($value['icon_type']) && $value['icon_type'] != '')?$value['icon_type']:'';
$icon_code = (isset($value['icon']['code']) && $value['icon']['code'] != '')?$value['icon']['code']:'';
$icon_url = (isset($value['icon']['url']) && $value['icon']['url'] != '')?$value['icon']['url']:'';
$icon_width = (isset($value['icon']['width']) && $value['icon']['width'] != '')?$value['icon']['width']:'20';
$icon_height = (isset($value['icon']['height']) && $value['icon']['height'] != '')?$value['icon']['height']:'20';

?>
<div class="tw-each-tab-column">
    <div class="tw-tab-item-inner">
        <div class="tw-item-header clearfix">
            <div class='tw-item-header-title'>
                <span class="tw_label_keyup"><?php echo esc_attr($tab_label); ?></span>
                <span class="tw_woocommerce_default"> <?php esc_html_e('Default WooCommerce Tab', TW_TD) ?> </span>
            </div>
            <div class='tw-item-functions'>
                <span class='tw-tab-sort' title="Sort" ><i class="fa fa-arrows-alt"></i></span>
                <span class='tw-tab-hide-show'><i class="fa fa-caret-down"></i></span>
            </div>
        </div>
        <div class='tw-tab-item-options clearfix' style='display:none;'>
            <input type="hidden" name="tab_set_items[tab][<?php echo $key; ?>][default_tab_name]" value="<?php $key; ?>">

            <div class="tw-tab-cbody">
                <div class="tw-field-wrap tw-row-odd" >
                    <label><?php esc_html_e('Tab Label',TW_TD);?></label>
                    <div class="tw-right-options"> 
                        <input type="text" class ="tw_label_class"  name="tab_set_items[tab][<?php echo $key;?>][tab_label]" value="<?php echo esc_attr($tab_label) ?>">
                        <p class="description">
                        <?php esc_html_e("Please enter a label for this tab. Note: The label you enter here will replace the default 'Description' label of the tab",TW_TD);?>               
                    </p>
                    </div>
                </div>
                <div class="tw-field-wrap tw-row-even">
                        <label><?php esc_html_e('Choose Icon Type',TW_TD);?></label>
                        <div class="tw-right-options"> 
                            <select name="tab_set_items[tab][<?php echo $key;?>][icon_type]" class="tw-ns-active tw-tab_icon-type">
                                <option><?php esc_html_e('None',TW_TD);?></option>
                                <option value="available_icon" <?php selected(esc_attr($tab_icon_type), 'available_icon'); ?>><?php esc_html_e('Available Icon',TW_TD);?></option>
                                <option value="upload_own" <?php selected(esc_attr($tab_icon_type), 'upload_own'); ?>><?php esc_html_e('Upload Own Icon',TW_TD);?></option>
                            </select>
                            <p class="description">
                                <?php esc_html_e('Please select a icon for this tab here',TW_TD);?>               
                            </p>
                        </div>
                </div>
                <div class="tw_selection_icontype_wrapper tw-row-even">
                    <div class="tw_available_icon" <?php echo (esc_attr($tab_icon_type) == 'available_icon')?"style='display:block;'": " style='display:none;'" ?> >
                        <div class="tw-field-wrap">
                            <label for="tw-icon_<?php echo $key; ?>"><?php esc_html_e('Available Icon',TW_TD);?></label>
                            <div class="tw-right-options"> 
                                <input class="tw-icon-picker" type="hidden" id="tw-icon_<?php echo $key; ?>"
                                name='tab_set_items[tab][<?php echo $key; ?>][icon][code]' 
                                value='<?php if($icon_code != '' ){ echo esc_attr($icon_code); } ?>' />
                                <div data-target="#tw-icon_<?php echo $key; ?>" class=" button-secondary tw-button icon-picker <?php if ($icon_code !='') { $v = explode('|', $icon_code); echo $v[0] . ' ' . $v[1]; } ?> "></div>
                                <span><?php esc_html_e( 'Select Icon', TW_TD); ?></span>
                                <p class="description">
                                    <?php esc_html_e('Please select a icon from the available list of icons',TW_TD);?>               
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tw_upload_own_icon"  <?php echo (esc_attr($tab_icon_type) == 'upload_own')?"style='display:block;'": " style='display:none;'" ?>>
                        <div class="tw-field-wrap">
                            <label for="tw-upload_icon_<?php echo $key; ?>"><?php esc_html_e('Upload Own Icon',TW_TD);?></label>
                            <div class="tw-right-options"> 
                                <input type="text" id='tw-image-url_<?php echo $key; ?>' name='tab_set_items[tab][<?php echo $key; ?>][icon][url]' class='tw-image-url tw-tab-text' 
                                value='<?php if($icon_url != '' ){ echo esc_url($icon_url); } ?>' />
                                <input type="button" class='button-secondary tw-button tw-upload-icon-btn' value='<?php esc_html_e('Upload Icon', TW_TD); ?>' />

                                <div class='tw-iconpreview'>
                                    <?php if($icon_url != '') {
                                        $iconurl = $icon_url;
                                    } else {
                                        //$iconurl =  ETAB_IMAGE_DIR.'thumbnail-default.jpg';
                                    }?>
                                    <img src = '<?php echo esc_url($iconurl); ?>'/>
                                </div>
                                <p class="description">
                                    <?php esc_html_e('Please upload a icon of your choice for this tab here',TW_TD);?>               
                                </p>
                          </div>
                        </div>
                        <div class="tw-field-wrap">
                            <label for="tw-icon-width_<?php echo $key; ?>"><?php esc_html_e( 'Height (px) ', TW_TD ); ?></label>
                            <div class="tw-right-options"> 
                                <input type="number" id='ec-image-height_<?php echo $key; ?>' name='tab_set_items[tab][<?php echo $key; ?>][icon][height]' placeholder='<?php esc_html_e('Height', TW_TD); ?>' value='<?php if($icon_height != '' ){ echo esc_attr($icon_height ); } ?>' />
                                <p class="description">
                                    <?php esc_html_e('Please set a height for your uploaded icon. Note: The value must be set in px',TW_TD);?>               
                                </p>
                            </div>
                        </div>
                        <div class="tw-field-wrap">
                            <label for="tw-icon-width_<?php echo $key; ?>"><?php esc_html_e( 'Width (px) ', TW_TD ); ?></label>
                            <div class="tw-right-options"> 
                                <input type="number" id='ec-image-width_<?php echo $key; ?>' name='tab_set_items[tab][<?php echo $key; ?>][icon][width]' placeholder='<?php esc_html_e('Width', TW_TD); ?>' value='<?php if($icon_width != '' ){ echo esc_attr($icon_width ); } ?>' />
                                <p class="description">
                                    <?php esc_html_e('Please set a width for your uploaded icon. Note: The value must be set in px',TW_TD);?>               
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>