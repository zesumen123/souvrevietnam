<?php defined('ABSPATH') or die('No script kiddies please!!');

$postid = $post->ID;
$tw_tab_settings = get_post_meta($postid, 'tw_tab_settings', true);

?>
<div class="tw-setup-wrapper tw-main-settings-wrapper">
    <div class="tw-tab-header">
        <ul class="tw-nav-tabs">
            <li class="tw-tab tw-active" data-id="tw-general-select"><?php esc_html_e('General Settings',TW_TD);?></li>
            <li class="tw-tab" data-id="tw-display-select"><?php esc_html_e('Display Settings',TW_TD);?></li>
            <li class="tw-tab" data-id="tw-custom-styling"><?php esc_html_e('Custom Styling',TW_TD);?></li>
        </ul>
    </div>
    <div class="tw-tabs-content-wrap">
        <div class="tw-tab-content tw-tab-content-active" id="tw-general-select">
            <?php include( TW_PATH . '/inc/admin/metabox/general/general.php' ); ?>
        </div>
        <div class="tw-tab-content" id="tw-display-select" style="display:none;">
            <?php include( TW_PATH . '/inc/admin/metabox/general/display.php' ); ?>
        </div>
        <div class="tw-tab-content" id="tw-custom-styling" style="display:none;">
            <?php include( TW_PATH . '/inc/admin/metabox/general/custom.php' ); ?>
        </div>
    </div>
</div>
