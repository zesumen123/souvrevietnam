<?php defined( 'ABSPATH' ) or die( "No script kiddies please!" ); ?>

<div class="tw-about-main-wrapper">
    <div class="psfw-intro-wrap">
        <div class="psfw-header">
            <div>
                <div id="psfw-fb-root"></div>
                <script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>
                <script>!function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location)?'http':'https'; if (!d.getElementById(id)){js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); }}(document, 'script', 'twitter-wjs');</script>
            </div>
            <div class="psfw-header-section">
                <div class="psfw-header-left">
                    <div class="psfw-title"><?php esc_html_e('TabWoo - Extra Tabs Plugin for WooCommerce', TW_TD ); ?> </div>
                    <div class="psfw-version-wrap">
                        <span> Version <?php echo TW_VERSION; ?> </span>
                    </div>
                </div>
                <div class="psfw-header-social-link">
                    <p class="psfw-follow-us"><?php esc_html_e( 'Follow us for new updates', TW_TD ); ?></p>
                    <div class="fb-like" data-href="https://www.facebook.com/accesspressthemes" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
                    <a href="//twitter.com/accesspressthemes" class="twitter-follow-button" data-show-count="false">Follow @accesspressthemes</a>
                </div>
            </div>
        </div>
        <div class="psfw-how-to-use-container">
            <div class="psfw-column-one-wrap">
                <div class="psfw-panel-body">
                    <div class="psfw-row">
                        <div class="psfw-col-three-third">
                            <h3><?php esc_html_e( 'About Us', TW_TD ); ?></h3>
                            <div class="psfw-columns-wrapper">
                                <div class="psfw-left-column">
                                    <div class="psfw-tab-wrapper">
                                        <p><?php esc_html_e( 'AccessPress Themes is a venture of Access Keys - who has developed hundreds of Custom WordPress themes and plugins for its clients over the years. ', TW_TD ); ?>
                                        </p>
                                        <div class="psfw-halfseperator"></div>
                                        <p><strong><?php esc_html_e( 'Please visit our product page for more details here:', TW_TD ); ?></strong>
                                            <br />
                                            <a href="//accesspressthemes.com/wordpress-plugins/tabwoo/" target="_blank">//accesspressthemes.com/wordpress-plugins/tabwoo/</a>
                                        </p>
                                        <div class="psfw-halfseperator"></div>
                                        <p><strong><?php esc_html_e( 'Please visit our demo page here:', TW_TD ); ?></strong>
                                            <br />
                                            <a href="//demo.accesspressthemes.com/wordpress-plugins/tabwoo/" target="_blank">//demo.accesspressthemes.com/wordpress-plugins/tabwoo/</a>
                                        </p>

                                        <div class="psfw-halfseperator"></div>
                                        <p><strong><?php esc_html_e( 'Plugin documentation can be found here:', TW_TD ); ?></strong>
                                            <br />
                                            <a href="//accesspressthemes.com/documentation/tabwoo/" target="_blank">//accesspressthemes.com/documentation/tabwoo/</a>
                                        </p>
                                        <p>&nbsp;</p>
                                    </div>
                                    <div>
                                       <p> <strong><?php esc_html_e('Themes Compatible with the Plugin : ', TW_TD ); ?></strong></p>

                                       <p><strong><?php esc_html_e('TabWoo',  TW_TD ); ?></strong> <?php esc_html_e("works best with every WordPress WooCommerce compatible theme. It's even more remarkable when used with popular themes like VMagazine and AccessPress Parallax. ",  TW_TD ); ?></p>

                                        <p><?php esc_html_e('AND IF THIS PLUGIN HAS IMPRESSED YOU, THEN YOU WOULD ENJOY OUR OTHER PROJECTS TOO. DO CHECK THESE OUT : ',  TW_TD ); ?></p>

                                        <p><a href="//wpall.club/"><?php esc_html_e('WPAll Club ',  TW_TD ); ?></a> <?php esc_html_e('-  A complete WordPress resources club. WordPress tutorials, blogs, curated free and premium themes and plugins, WordPress deals, offers, hosting info and more. ',  TW_TD ); ?></p>

                                       <p> <a href="//themeforest.net/user/accesskeys/portfolio"><?php esc_html_e('Premium WordPress Theme ',  TW_TD ); ?></a> <?php esc_html_e('-   6 premium WordPress themes well suited for all sort of websites. Professional, well coded and highly configurable themes for you. ',  TW_TD ); ?> </p>

                                      <p>  <a href="//codecanyon.net/user/accesskeys/portfolio?Ref=AccessKeys"><?php esc_html_e('Premium WordPress Plugins ',  TW_TD ); ?></a><?php esc_html_e(' - 45+ premium WordPress plugins of many different types. High user ratings, great quality and best sellers in CodeCanyon marketplace.  ',  TW_TD ); ?></p>

                                      <p>  <a href="//accesspressthemes.com/"><?php esc_html_e('AccessPress Themes ',  TW_TD ); ?></a><?php esc_html_e(' - AccessPress Themes has 50+ beautiful and elegant, fully responsive, multipurpose themes to meet your need for free and commercial basis. ',  TW_TD ); ?></p>

                                      <p>  <a href="//8degreethemes.com/"><?php esc_html_e('8Degree Themes ',  TW_TD ); ?></a> <?php esc_html_e('- 8Degree Themes offers 15+ free WordPress themes and 16+ premium WordPress themes carefully crafted with creativity. ',  TW_TD ); ?></p>
                                    </div>
                                </div>
                                <div class="psfw-right-column">
                                    <h3 class="psfw-sub-title"><?php esc_html_e('More from AccessPress themes', TW_TD ); ?> </h3>
                                    <div class="psfw-row">
                                        <div class="psfw-col-one-third">
                                            <div class="psfw-product">
                                                <div class="psfw-logo-product">
                                                    <a href="//accesspressthemes.com/plugins/" target="_blank">
                                                        <img src="<?php echo TW_IMG_DIR; ?>/plugin.png" alt="<?php esc_attresc_html_e( 'AccessPress Social Icons', TW_TD ); ?>" />
                                                    </a>
                                                </div>
                                                <div class="psfw-productext">
                                                    <span class="prod-title"><?php esc_html_e('WordPress Plugins', TW_TD ); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="psfw-col-one-third">
                                            <div class="psfw-product">
                                                <div class="psfw-logo-product">
                                                    <a href="//accesspressthemes.com/themes/" target="_blank"><img src="<?php echo TW_IMG_DIR; ?>/theme.png" /></a>
                                                </div>
                                                <div class="psfw-productext">
                                                    <span class="prod-title"><?php esc_html_e('WordPress Themes', TW_TD ); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="psfw-col-one-third">
                                            <div class="psfw-product">
                                                <div class="psfw-logo-product">
                                                    <a href="//accesspressthemes.com/contact/" target="_blank"><img src="<?php echo TW_IMG_DIR; ?>/customize.png" /></a>
                                                </div>
                                                <div class="psfw-productext">
                                                    <span class="prod-title"><?php esc_html_e('WordPress Customization', TW_TD ); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <h3><?php esc_html_e( 'Get in touch', TW_TD ); ?></h3>
                                    <p><?php esc_html_e( 'If you have any question/feedback, please get in touch:', TW_TD ); ?></p>
                                    <p>
                                        <strong><?php esc_html_e( 'General enquiries:', TW_TD ); ?></strong> <a href="mailto:info@accesspressthemes.com">info@accesspressthemes.com</a><br />
                                        <strong><?php esc_html_e( 'Support:', TW_TD ); ?></strong> <a href="mailto:support@accesspressthemes.com">support@accesspressthemes.com</a><br />
                                        <strong><?php esc_html_e( 'Sales:', TW_TD ); ?></strong> <a href="mailto:sales@accesspressthemes.com">sales@accesspressthemes.com</a>
                                    </p>
                                    <div class="psfw-seperator"></div>
                                    <div class="psfw-dottedline"></div>
                                    <div class="psfw-seperator"></div>

                                    <div class="psfw-col-three-third">
                                        <h3><?php esc_html_e( 'Get social', TW_TD ); ?></h3>
                                        <p><?php esc_html_e( 'Get connected with us on social media. Facebook is the best place to find updates on our themes/plugins: ', TW_TD ); ?></p>

                                        <p><strong><?php esc_html_e( 'Like us on facebook:', TW_TD ); ?></strong>
                                            <br />
                                            <iframe style="border: none; overflow: hidden; width: 700px; height: 250px;" src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FAccessPress-Themes%2F1396595907277967&amp;width=842&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=1411139805828592" width="240" height="150" frameborder="0" scrolling="no"></iframe>
                                        </p>

                                        <ul class="psfw-about psfw-unstyled psfw-inlinelist">
                                            <li><a href="//www.pinterest.com/accesspresswp/" target="_blank"><img src="<?php echo TW_IMG_DIR; ?>/pinterest.png" alt="pinterest"></a>
                                            </li>
                                            <li><a href="//www.flickr.com/photos/accesspressthemes/" target="_blank"><img src="<?php echo TW_IMG_DIR; ?>/flicker.png" alt="flicker"></a>
                                            </li>
                                            <li><a href="//twitter.com/apthemes" target="_blank"><img src="<?php echo TW_IMG_DIR; ?>/twitter.png" alt="twitter"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>