<?php defined('ABSPATH') or die("No script kiddies please!");

$tw_settings = get_option('tw_settings', true);

$enable = isset($tw_settings['enable']) ? 'yes':'no';
$default_tab_set = (isset($tw_settings['default_tab_set']) && $tw_settings['default_tab_set'] != '') ? $tw_settings['default_tab_set']:'none';
$gmap_api_key = (isset($tw_settings['gmap_api_key']) && $tw_settings['gmap_api_key'] != '') ? $tw_settings['gmap_api_key']:'';

?>
<div class="psfw-header">
    <div>
        <div id="psfw-fb-root"></div>
        <script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>
        <script>!function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location)?'http':'https'; if (!d.getElementById(id)){js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); }}(document, 'script', 'twitter-wjs');</script>
    </div>
    <div class="psfw-header-section">
        <div class="psfw-header-left">
            <div class="psfw-title"><?php esc_html_e('TabWoo - Extra Tabs Plugin for WooCommerce', TW_TD ); ?> </div>
            <div class="psfw-version-wrap">
                <span> Version <?php echo TW_VERSION; ?> </span>
            </div>
        </div>

        <div class="psfw-header-social-link">
            <p class="psfw-follow-us"><?php esc_html_e( 'Follow us for new updates', TW_TD ); ?></p>
            <div class="fb-like" data-href="https://www.facebook.com/accesspressthemes" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
            <a href="//twitter.com/accesspressthemes" class="twitter-follow-button" data-show-count="false">Follow @accesspressthemes</a>
        </div>
    </div>
</div>
<div class="tw-wrapper tw-plugin-setting-wrapper">
    <?php if (isset($_GET['message']) && $_GET['message'] == 1) { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php esc_html_e('Settings saved successfully', TW_TD); ?> </p>
        </div>
    <?php } ?>
    <?php if (isset($_GET['message']) && $_GET['message'] == 2) { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php esc_html_e('Settings restored to default', TW_TD); ?></p>
        </div>
    <?php }
    ?>
    <div class="tw-backend-settings">
    	<form action="<?php echo admin_url() . 'admin-post.php' ?>"  method="post" enctype="multipart/form-data">
            <input type="hidden" name="action" value="tw_settings_save"/>
			<div class="tw-tab-item-inner">
				<div class="tw-field-wrap tw-row">
					<label><?php esc_html_e('Enable TabWoo',TW_TD);?></label>
					<div class="tw-right-options"> 
						<label class="tw-switch">
							<input type="checkbox" class="tw-show-description" value="yes" name="tw_settings[enable]" <?php echo ($enable == 'yes') ? 'checked':''; ?>>
							<div class="tw-check round"></div>
						</label>
						<p class="description">
							<?php esc_html_e("Please check enable our plugin - TabWoo.",TW_TD);?>               
						</p>
					</div>
				</div>
				<div class = "tw-field-wrap tw-row" >
					<label><?php esc_html_e( 'Default Tab Set', TW_TD ); ?></label>
					<div class="tw-right-options"> 
						<?php 
							$post_type_query  = new WP_Query(  
													    array (  
													        'post_type' => 'tabwoo',  
													        'posts_per_page' => -1  
													    )  
												);   

							if ( $post_type_query->have_posts() ) {
								$posts_array = $post_type_query->posts; 
								$post_title_array = wp_list_pluck( $posts_array, 'post_title', 'ID' );
								?> 
								<select name="tw_settings[default_tab_set]" class="tw-ns-active">
									<option value="none" <?php selected(esc_attr($default_tab_set),'none') ?>><?php echo esc_html_e( 'None', TW_TD ); ?></option>
									<?php 
									if(isset($post_title_array) && !empty($post_title_array)) {
										foreach ( $post_title_array as $key => $value) { ?>
											<option value="<?php echo $key ?>" <?php selected(esc_attr($default_tab_set), $key) ?> ><?php esc_html_e($value, TW_TD ); ?></option>
										<?php } 
									}
									?>
								</select>
						<?php 
							}
						?>
						<p class="description"><?php esc_html_e( 'Please select taxonomy first.', TW_TD ); ?></p>
					</div>
				</div>
				<div class="tw-field-wrap tw-row">
         			<label><?php esc_html_e('Google Map API Key',TW_TD);?></label>
        			<div class="tw-right-options"> 
         				<input type="text" name="tw_settings[gmap_api_key]" value="<?php echo esc_attr($gmap_api_key); ?>">
         				<p class="description"><?php esc_html_e('Please enter your API here. You can get your key from',TW_TD);?> <a href="//developers.google.com/maps/documentation/javascript/get-api-key" target="_blank"> //developers.google.com/maps/documentation/javascript/get-api-key </a><?php esc_html_e('Note: Map will not be displayed if the key is missing',TW_TD);?></p>
         			</div>
    			</div>
			</div>

			<div class="tw-save-btn">
                <?php
                wp_nonce_field('tw_nonce', 'tw_nonce_field');
                ?>
                <input type = "submit" class="button-primary tw-submit-button" name="tw_settings_submit" value="<?php esc_html_e('Save Options', TW_TD ) ?>"  />
            </div>
		</form>
	</div>
</div>