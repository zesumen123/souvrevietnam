<?php defined('ABSPATH') or die('No script kiddies please!!');

if (!class_exists('TW_Post_Type')) {
    class TW_Post_Type {
        var $tw_lib;

        function __construct() {
            $this->tw_lib = new TW_Library;

            add_action( 'init', array($this, 'tw_register_post_type')); 

            add_filter( 'manage_tabwoo_posts_columns', array( $this, 'tw_columns_head' ) ); 
            add_action( 'manage_tabwoo_posts_custom_column', array( $this, 'tw_columns_content' ), 10, 2 ); 

            add_filter( 'manage_tabwoo_posts_columns', array( $this, 'tw_columns_head_tag' ) ); 
            add_action( 'manage_tabwoo_posts_custom_column', array( $this, 'tw_columns_content_tag' ), 10, 2 ); 

            add_filter( 'manage_tabwoo_posts_columns', array( $this, 'tw_columns_head_product' ) ); 
            add_action( 'manage_tabwoo_posts_custom_column', array( $this, 'tw_columns_content_product' ), 10, 2 ); 

            add_action( 'add_meta_boxes', array($this, 'tw_add_individual_tabs'));
            add_action( 'add_meta_boxes', array($this, 'tw_tab_set_general_settings'));  
            add_action( 'add_meta_boxes', array($this, 'tw_assign_tab_set')); 

            add_action( 'save_post', array($this, 'tw_tab_meta_save')); 

            add_action( 'add_meta_boxes', array( $this, 'tw_individual_product_settings')); 
            add_action( 'save_post', array( $this, 'tw_single_product_settings_save'));

            add_filter( 'post_row_actions', array($this, 'remove_quick_edit_view'),10,1);

        }
        
        function remove_quick_edit_view( $actions){
            global $post;

            if ($post->post_type=='tabwoo') {
                unset($actions['view']);
                unset($actions['inline hide-if-no-js']);
                return $actions;
            }
            return $actions;
        }

        function tw_register_post_type() {
            $labels = array(
                'name' => _x( 'TabWoo', 'post type general name', TW_TD ),
                'singular_name' => _x( 'TabWoo', 'post type singular name', TW_TD ),
                'menu_name' => _x( 'TabWoo', 'admin menu', TW_TD ),
                'name_admin_bar' => _x( 'TabWoo', 'add new on admin bar', TW_TD ),
                'add_new' => _x( 'Add Tab Set', 'TabWoo', TW_TD ),
                'add_new_item' => __( 'Add New Tab', TW_TD ),
                'new_item' => __( 'New Tab Set', TW_TD ),
                'edit_item' => __( 'Edit Tab Set', TW_TD ),
                'view_item' => __( 'View Tab Set', TW_TD ),
                'all_items' => __( 'All Tab Set', TW_TD ),
                'search_items' => __( 'Search Tab Set', TW_TD ),
                'parent_item_colon' => __( 'Parent Tab Set', TW_TD ),
                'not_found' => __( 'No Tab sets found.', TW_TD ),
                'not_found_in_trash' => __( 'No Tab sets found in Trash.', TW_TD )
            );

            $args = array(
                'labels' => $labels,
                'description' => __('Description.', TW_TD),
                'public' => false,
                'publicly_queryable' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'menu_icon' => 'dashicons-format-gallery',
                'query_var' => true,
                'rewrite' => array('slug' => TW_TD),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => false,
                'menu_position' => null,
                'supports' => array('title')
            );
            register_post_type('tabwoo', $args);
        }

        function tw_columns_head( $columns ){
            $columns[ 'assigned_categories' ] = __( 'Assigned To Product Categories', TW_TD );
            return $columns;
        }

        public function tw_columns_content( $column, $post_ID ){
            if ($column == 'assigned_categories') {
                $post_id = $post_ID;
                $tw_tab_settings = get_post_meta($post_id, 'tw_tab_settings', true);
                $category = (isset($tw_tab_settings['assigned_to']['category']) && $tw_tab_settings['assigned_to']['category'] != '') ? $tw_tab_settings['assigned_to']['category']:array();
       
                if (!empty($category)){
                    $category_name = array();
                    foreach ($category as $cat => $category_id) {
                        if( $term = get_term_by( 'id', $category_id, 'product_cat' ) ){
                            $category_name[] = $term->name;
                        }
                    }

                    ?>
                    <textarea class="tw-category-display-value" style="resize: none;" rows="2" cols="40" readonly="readonly"><?php echo implode(", ",$category_name) ?></textarea> <?php
                }else{
                    ?>
                    <textarea class="tw-category-display-value" style="resize: none;" rows="2" cols="40" readonly="readonly">-</textarea> <?php
                }
                
            }
        }

        function tw_columns_head_tag( $columns ){
            $columns[ 'assigned_tags' ] = __( 'Assigned To Product Tags', TW_TD );
            return $columns;
        }

        public function tw_columns_content_tag( $column, $post_ID ){
            if ($column == 'assigned_tags') {
                $post_id = $post_ID;
                $tw_tab_settings = get_post_meta($post_id, 'tw_tab_settings', true);
                $tag = (isset($tw_tab_settings['assigned_to']['tag']) && $tw_tab_settings['assigned_to']['tag'] != '') ? $tw_tab_settings['assigned_to']['tag']:array();

               
                if (!empty($tag)){
                    $tag_name = array();
                    foreach ($tag as $t => $tag_id) {
                        if( $term = get_term_by( 'id', $tag_id, 'product_tag' ) ){
                            $tag_name[] = $term->name;
                        }
                    }

                    ?>
                    <textarea class="tw-category-display-value" style="resize: none;" rows="2" cols="40" readonly="readonly"><?php echo implode(", ",$tag_name) ?></textarea> <?php
                }else{
                    ?>
                    <textarea class="tw-category-display-value" style="resize: none;" rows="2" cols="40" readonly="readonly">-</textarea> <?php
                }
                
            }
        }

        function tw_columns_head_product( $columns ){
            $columns[ 'assigned_products' ] = __( 'Assigned To Products', TW_TD );
            return $columns;
        }

        public function tw_columns_content_product( $column, $post_ID ){
            if ($column == 'assigned_products') {
                $post_id = $post_ID;
                $tw_tab_settings = get_post_meta($post_id, 'tw_tab_settings', true);
                $product = (isset($tw_tab_settings['assigned_to']['product']) && $tw_tab_settings['assigned_to']['product'] != '') ? $tw_tab_settings['assigned_to']['product']:array();
                
                if (!empty($product)){
                    $product_name = array();
                    foreach ($product as $p => $product_id) {
                        $product_name[] = get_the_title( $product_id );
                    }

                    ?>
                    <textarea class="tw-category-display-value" style="resize: none;" rows="2" cols="40" readonly="readonly"><?php echo implode(", ",$product_name) ?></textarea> <?php
                }else{
                    ?>
                    <textarea class="tw-category-display-value" style="resize: none;" rows="2" cols="40" readonly="readonly">-</textarea> <?php
                }
                
            }
        }

        function tw_add_individual_tabs(){
            add_meta_box( 'tw_add_tab', __('Tabs Items', TW_TD), array($this, 'tw_tab_settings'), 'tabwoo', 'normal', 'high');
        }

        function tw_tab_settings( $post ){
            wp_nonce_field(basename(__FILE__),'tw_tab_setup_nonce');
            include(TW_PATH .'inc/admin/metabox/add_tab.php');
        }

        function tw_tab_set_general_settings(){
            add_meta_box( 'tw_general_settings', __('Main Settings', TW_TD), array($this, 'tw_main_settings'), 'tabwoo', 'normal', 'low');
        }

        function tw_main_settings( $post ){
            include(TW_PATH .'inc/admin/metabox/main_settings.php');
        }

        function tw_assign_tab_set(){
            add_meta_box( 'tw_assign_tab', __('Assign Tab Set', TW_TD), array($this, 'tw_assign_tab_sets'), 'tabwoo', 'normal', 'low');
        }

        function tw_assign_tab_sets( $post ) {
            include (TW_PATH . '/inc/admin/metabox/assign_tab_sets.php');
        }

        public function tw_tab_meta_save( $post_id ){
            $is_autosave = wp_is_post_autosave($post_id);
            $is_revision = wp_is_post_revision($post_id);
            $is_valid_nonce = ( isset($_POST['tw_tab_setup_nonce']) && wp_verify_nonce($_POST['tw_tab_setup_nonce'], basename(__FILE__)) ) ? 'true' : 'false';

            if ($is_autosave || $is_revision || !$is_valid_nonce) {
                return;
            }

            if (isset($_POST['tab_set_items'])) {
                $tw_tab_settings = $this->tw_lib->tw_sanitize_array($_POST['tab_set_items']);
                update_post_meta($post_id, 'tw_tab_settings',$tw_tab_settings);
            }
             
            return;
        }

        function tw_individual_product_settings(){
            $type = array( 'product', 'download', 'tw_product_manager' );
            add_meta_box( 'tw_individual_product_settings', __( 'TabWoo Customization Options', TW_TD ), array( $this, 'tw_single_product_settings' ), $type, 'normal', 'core' );
        }

        function tw_single_product_settings(){
            wp_nonce_field( basename( __FILE__ ), 'tw_product_nonce' );

            include(TW_PATH . '/inc/admin/metabox/product_page.php'); 
        }

        function tw_single_product_settings_save( $post_id ){
            // Checks save status
            $is_autosave = wp_is_post_autosave( $post_id );
            $is_revision = wp_is_post_revision( $post_id );
            $is_valid_nonce = ( isset( $_POST[ 'tw_product_nonce' ] ) && wp_verify_nonce( $_POST[ 'tw_product_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
            // Exits script depending on save status
            if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
                return;
            }
            if ( isset( $_POST[ 'single_product_settings' ] ) ) {
                $single_product_settings = ( array ) $_POST[ 'single_product_settings' ];
                $single_product_settings = $this->tw_lib->tw_sanitize_array( $single_product_settings );
                update_post_meta( $post_id, 'single_product_settings', $single_product_settings );
            }
            return;
        }
    }

new TW_Post_Type(); 

}