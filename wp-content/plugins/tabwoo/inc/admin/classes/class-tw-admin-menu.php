<?php defined('ABSPATH') or die('No script kiddies please!!');

if (!class_exists('TW_Admin_Menu')){

    class TW_Admin_Menu {
        var $tw_lib;
        
        function __construct() {
            $this->tw_lib = new TW_Library;

            add_action('init', array($this, 'tw_plugin_text_domain')); 
            add_action('admin_menu', array($this, 'tw_plugin_settings_page')); 
            add_action('admin_menu', array($this, 'tw_how_to_use_page')); 
            add_action('admin_menu', array($this, 'tw_about_us_page')); 

            add_action('admin_post_tw_settings_save', array($this, 'save_settings')); 

        }

        function tw_plugin_text_domain(){
            load_plugin_textdomain('tab-woo', false, basename( dirname( __FILE__ ) ) . '/languages/');
        }

        function tw_plugin_settings_page() {
            add_submenu_page( 'edit.php?post_type=tabwoo', __('Settings', TW_TD), __('Settings', TW_TD), 'manage_options', 'settings', array($this, 'tw_plugin_settings'));
        }

        function tw_plugin_settings() {
            include (TW_PATH . '/inc/admin/plugin_settings.php');
        }

        function tw_how_to_use_page() {
            add_submenu_page( 'edit.php?post_type=tabwoo', __('How To Use', TW_TD), __('How To Use', TW_TD), 'manage_options', 'how-to-use', array($this, 'tw_how_to_use'));
        }

        function tw_how_to_use() {
            include (TW_PATH . '/inc/admin/how_to_use.php');
        }

        function tw_about_us_page() {
            add_submenu_page( 'edit.php?post_type=tabwoo', __('More WordPress Stuff', TW_TD), __('More WordPress Stuff', TW_TD), 'manage_options', 'about-us', array($this, 'tw_about_us'));
        }

        function tw_about_us() {
            include (TW_PATH . '/inc/admin/about_us.php');
        }

        function save_settings() {
            if (isset($_POST['tw_nonce_field']) && wp_verify_nonce($_POST['tw_nonce_field'], 'tw_nonce')) {
                if (isset($_POST['tw_settings_submit'])) {
                    $_POST = array_map('stripslashes_deep', $_POST);
                    $tw_settings = $this->tw_lib->tw_sanitize_array($_POST['tw_settings']);
                    update_option('tw_settings', $tw_settings);
                    wp_redirect(admin_url('edit.php?post_type=tabwoo&page=settings&message=1'));
                    exit;
                }
            }
        }
    }
new TW_Admin_Menu();  
}