<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!!' );
if ( !class_exists( 'TW_Activation' ) ) {

    class TW_Activation{
 
        function __construct() {   
            register_activation_hook(__FILE__, array($this, 'default_plugin_settings'));
        }

        public function default_plugin_settings() {
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
            if (! is_plugin_active('woocommerce/woocommerce.php')) {
                wp_die(__('You need to activate WooCommerce in order to activate "TabWoo - Extra Tabs Plugin for WooCommerce ". Please make sure that you have WooCommerce installed and activated.', TW_TD ));
            }
        }
    }

    new TW_Activation();
}
