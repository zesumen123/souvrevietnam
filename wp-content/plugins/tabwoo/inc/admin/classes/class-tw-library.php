<?php defined('ABSPATH') or die('No script kiddies please!!');

if ( !class_exists('TW_Library') ) {

    class TW_Library {

        function tw_sanitize_array($array = array(), $sanitize_rule = array()) {
            if ( !is_array($array) || count($array) == 0 ) {
                return array();
            }

            foreach ( $array as $k => $v ) {
                if ( !is_array($v) ) {

                    $default_sanitize_rule = (is_numeric($k)) ? 'text' : 'html';
                    $sanitize_type = isset($sanitize_rule[ $k ]) ? $sanitize_rule[ $k ] : $default_sanitize_rule;
                    $array[ $k ] = $this->tw_sanitize_value($v, $sanitize_type);
                }
                if ( is_array($v) ) {
                    $array[ $k ] = $this->tw_sanitize_array($v, $sanitize_rule);
                }
            }

            return $array;
        }

        function tw_sanitize_value($value = '', $sanitize_type = 'text') {
            switch ( $sanitize_type ) {
                case 'html':
                    return wp_kses_post(stripslashes_deep($value));
                    break;
                default:
                    return sanitize_text_field($value);
                    break;
            }
        }

        function tw_get_product_json() {

            $query = new WC_Product_Query( 
                array(
                    'orderby' => 'date',
                    'order' => 'DESC',
                ) 
            );

            $products = $query->get_products();

            foreach ( $products as $product ) {
                $product_list[] = array(
                    'id' => $product->get_id(), 
                    'product_name' => $product->get_title() 
                );
            }

            $product_list = json_encode($product_list);

            return $product_list;

        }

        public static function generateRandomIndex($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        //Other Functions

        function print_array( $array ) {
            echo '<pre>';
            print_r( $array );
            echo '</pre>';
        }

        public function tw_fetch_category_list( $taxonomy, $term_id ) {
            $option_html = "";
            $taxonomies_array[] = $taxonomy;
            $terms = get_terms( $taxonomy, array( 'hide_empty' => false ) );
            $categoryHierarchy = array();
            if (is_array($terms)){

                $this-> tw_sort_terms_hierarchicaly( $terms, $categoryHierarchy );

                if (count( $categoryHierarchy) > 0 ) { //condition check if the taxonomy has atleast single term
                    $terms_exclude = array();
                    $option_html .= $this -> tw_print_option( $categoryHierarchy, 1, '', '', $term_id );
                }
            }

            return $option_html;
        }

        function tw_sort_terms_hierarchicaly( Array &$cats, Array &$into, $parentId = 0 ) {

            if(is_array($cats)){

                foreach ( $cats as $i => $cat ) {
                    if ( $cat -> parent == $parentId ) {
                        $into[ $cat -> term_id ] = $cat;
                        unset( $cats[ $i ] );
                    }
                }
            }

            foreach ( $into as $topCat ) {
                $topCat -> children = array();
                $this -> tw_sort_terms_hierarchicaly( $cats, $topCat -> children, $topCat -> term_id );
            }
        }

        function tw_print_option( $terms, $hierarchical = 1, $form = '', $field_title = '', $selected_term = array() ) {
            global $post;
            $post_id = $post -> ID;

            if(is_array($terms)){
                foreach ( $terms as $term ) {
                    $space = $this -> tw_check_parent( $term );
                    $option_value = $term -> term_id;

                    if ( is_array( $selected_term ) ) {
                        $selected = (in_array( $option_value, $selected_term )) ? 'selected="selected"' : '';
                    } else {
                        $selected = ($selected_term == $option_value) ? 'selected="selected"' : '';
                    }
                    $form .= '<option value="' . $option_value . '" ' . $selected . '>' . $space . $term -> name . '</option>';
                    if ( ! empty( $term -> children ) ) {
                        $form .= $this -> tw_print_option( $term -> children, $hierarchical, '', $field_title, $selected_term );
                    }
                }
            }
            return $form;
        }

        function tw_check_parent( $term, $space = '' ) {
            if ( is_object( $term ) ) {
                if ( $term -> parent != 0 ) {
                    $space .= str_repeat( '&nbsp;', 2 );
                    $parent_term = get_term_by( 'id', $term -> parent, $term -> taxonomy );
                    $space .= $this -> tw_check_parent( $parent_term, $space );
                }
            }
            return $space;
        }

        function tw_fetch_category( $post_id, $post, $category ) {

            $categories = get_the_terms( $post, $category );
            $separator = ' ';
            $output = '';
            if ( ! empty( $categories ) ) {
                foreach ( $categories as $category ) {
                    $output .= '<a href="' . esc_url( get_category_link( $category -> term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', TW_TD ), $category -> name ) ) . '">' . esc_html( $category -> name ) . '</a>' . $separator;
                }
                return trim( $output, $separator );
            }
        }

        function tw_fetch_content( $post, $excerpt_length ){
            return substr( get_the_excerpt(), 0, $excerpt_length );
        }

        function tw_new_product_tab_content($key, $tab) {
            include(TW_PATH . '/inc/frontend/custom_tabs/tab_content.php');
        }

    }
}