<?php defined('ABSPATH') or die('No script kiddies please!!');

if ( !class_exists('TW_Enqueue') ) {

    class TW_Enqueue {

        function __construct() {

            add_action('admin_enqueue_scripts', array( $this, 'tw_register_admin_assets'));
            add_action('wp_enqueue_scripts', array( $this, 'tw_register_assets'), 99);

        }

        function tw_register_assets() {

            $tw_settings = get_option('tw_settings', true);

            $enable = isset($tw_settings['enable']) ? 'yes':'no';

            if ( $enable == 'yes' ) {

                $gmap_api_key = (isset($tw_settings['gmap_api_key']) && $tw_settings['gmap_api_key'] != '') ? $tw_settings['gmap_api_key']:'' ;

                wp_enqueue_style( 'dashicons' );
                wp_enqueue_style( 'tw-font', '//fonts.googleapis.com/css?family=Bitter|Hind|Playfair+Display:400,400i,700,700i,900,900i|Open+Sans:400,500,600,700,900|Lato:300,400,700,900|Montserrat|Droid+Sans|Roboto|Lora:400,400i,700,700i|Roboto+Slab|Rubik|Merriweather:300,400,700,900|Poppins|Ropa+Sans|Playfair+Display|Rubik|Source+Sans+Pro|Roboto+Condensed|Roboto+Slab:300,400,700|Amatic+SC:400,700|Quicksand|Oswald|Quicksand:400,500,700', false );

                wp_enqueue_style( 'scroll-style', TW_CSS_DIR . '/jquery.mCustomScrollbar.css', false, TW_VERSION );

                wp_enqueue_style( 'fontawesome1-style', TW_CSS_DIR . '/fontawesome.css', false, TW_VERSION );
                wp_enqueue_style( 'fontawesome', TW_CSS_DIR . '/font-awesome.min.css', false, TW_VERSION );
                
                wp_enqueue_style( 'linear-style', TW_CSS_DIR . '/linear-style.css', false, TW_VERSION );
                wp_enqueue_style( 'elegant-icons', TW_CSS_DIR . '/elegant-icons.css', false, TW_VERSION );
                wp_enqueue_style( 'fa-brands-style', TW_CSS_DIR . '/fa-brands.css', false, TW_VERSION );
                wp_enqueue_style( 'fa-regular-style', TW_CSS_DIR . '/fa-regular.css', false, TW_VERSION );
                wp_enqueue_style( 'fa-solid-style', TW_CSS_DIR . '/fa-solid.css', false, TW_VERSION );

                if ( class_exists( 'WooCommerce' ) ) {
                    global $woocommerce;
                    wp_enqueue_script( 'wc-add-to-cart-variation', $woocommerce -> plugin_url() . '/assets/js/frontend/add-to-cart-variation.js', array( 'jquery' ), '1.6', true );
                }
                
                wp_register_script( 'tw_google_map', 'https://maps.googleapis.com/maps/api/js?key=' . $gmap_api_key, array( 'jquery' ) );
                wp_enqueue_script( 'tw_google_map' );
                wp_enqueue_script( 'scroll-script', TW_JS_DIR . '/jquery.mCustomScrollbar.js', array( 'jquery' ), TW_VERSION );
                
                wp_enqueue_style( 'tw-frontend-styles', TW_CSS_DIR . '/tw-frontend.css', array( 'tw-font', 'tw-scroll-style' ), TW_VERSION, true );
                if ( class_exists( 'WooCommerce' ) ) {
                    $js_list = array( 'jquery', 'scroll-script', 'wc-add-to-cart-variation', 'wp-util' );
                } else {
                    $js_list = array( 'jquery', 'scroll-script','wp-util' );
                }

                wp_enqueue_script( 'tw-frontend-script', TW_JS_DIR . '/tw-frontend.js', $js_list, TW_VERSION );

                
                $frontend_ajax_nonce = wp_create_nonce( 'tw-frontend-ajax-nonce' );
                $frontend_ajax_object = array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 
                                               'ajax_nonce' => $frontend_ajax_nonce 
                                        );

                wp_localize_script( 'tw-frontend-script', 'tw_frontend_js_params', $frontend_ajax_object );
                
            }
        }

        function tw_register_admin_assets( $hook ){

            global $post;
            wp_enqueue_media();
            wp_enqueue_script( 'thickbox' );
            wp_enqueue_script( 'wp-color-picker' );
            wp_enqueue_script( 'jquery-ui-core' );
            wp_register_script( 'icon_picker', TW_JS_DIR.'/icon-picker.js', array('jquery'), TW_VERSION, true );
            wp_enqueue_script( 'tw-nice-select', TW_JS_DIR . '/jquery.nice-select.min.js', false);
           
            wp_enqueue_script( 'icon_picker');
            wp_enqueue_script( 'tw-admin-script', TW_JS_DIR . '/tw-admin-script.js' , array( 'jquery', 'wp-color-picker', 'jquery-ui-sortable', 'jquery-ui-core','icon_picker', 'jquery-ui-autocomplete' ), TW_VERSION );
            
            wp_enqueue_style( 'fontawesome1-style', TW_CSS_DIR . '/fontawesome.css', false, TW_VERSION );
            wp_enqueue_style( 'linear-style', TW_CSS_DIR . '/linear-style.css', false, TW_VERSION );
            wp_enqueue_style( 'elegant-icons', TW_CSS_DIR . '/elegant-icons.css', false, TW_VERSION );
            wp_enqueue_style( 'fa-brands-style', TW_CSS_DIR . '/fa-brands.css', false, TW_VERSION );
            wp_enqueue_style( 'fa-regular-style', TW_CSS_DIR . '/fa-regular.css', false, TW_VERSION );
            wp_enqueue_style( 'fa-solid-style', TW_CSS_DIR . '/fa-solid.css', false, TW_VERSION );
                
            wp_enqueue_style( 'thickbox' );
            wp_enqueue_style( 'wp-color-picker' );
            wp_enqueue_style( 'dashicons' );
            wp_enqueue_style( 'nice-select', TW_CSS_DIR . '/nice-select.css', false);
            wp_register_style( 'icon-picker', TW_CSS_DIR.'/icon-picker.css', false, TW_VERSION );
            wp_enqueue_style( 'icon-picker');
            wp_enqueue_style( 'tw-font', TW_CSS_DIR . '/font-awesome.min.css', false, TW_VERSION );
            wp_enqueue_style( 'tw-backend-style', TW_CSS_DIR . '/tw-backend-style.css', false, TW_VERSION );
            wp_enqueue_style( 'tw-jquery-ui-style', TW_CSS_DIR . '/jquery-ui-css-1.12.1.css', false, TW_VERSION );

            $backend_js_obj = array('ajax_url' => admin_url('admin-ajax.php'),
                                    'ajax_nonce' => wp_create_nonce('tw_backend_ajax_nonce'),
                                    'delete_message'=> __('Confirm Delete?',TW_TD)
                            );

            wp_localize_script('tw-admin-script', 'tw_backend_js_obj', $backend_js_obj);

        }
    }
    new TW_Enqueue();
}