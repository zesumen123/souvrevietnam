<?php
defined('ABSPATH') or die('No script kiddies please!!');
if ( !class_exists('TW_Admin_Ajax') ) {

    class TW_Admin_Ajax {

        var $tw_lib;

        function __construct() {
            $this->tw_lib = new TW_Library;

            add_action( 'wp_ajax_tw_add_tab', array($this, 'tw_add_tab') ); 
            add_action( 'wp_ajax_nopriv_tw_add_tab', array($this, 'tw_add_tab') );

            add_action('wp_ajax_tw_multi_upload_image', array( $this, 'tw_multi_upload_image' )); 
            add_action('wp_ajax_nopriv_tw_multi_upload_image', array( $this, 'tw_multi_upload_image' ));

            add_action('wp_ajax_tw_multi_upload_file', array( $this, 'tw_multi_upload_file' )); 
            add_action('wp_ajax_nopriv_tw_multi_upload_file', array( $this, 'tw_multi_upload_file' ));

            add_action('wp_ajax_tw_selected_taxonomy_terms', array($this, 'tw_selected_taxonomy_terms')); 
            add_action('wp_ajax_nopriv_tw_selected_taxonomy_terms', array( $this, 'tw_selected_taxonomy_terms' ));

            add_action('wp_ajax_tw_search_product', array($this, 'tw_search_product')); 
            add_action('wp_ajax_nopriv_tw_search_product', array( $this, 'tw_search_product' )); 

        }

        function tw_add_tab(){
            if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'tw_backend_ajax_nonce')) {
                include_once(TW_PATH . '/inc/admin/metabox/ajax/add-tab.php');
                die();
            }
        }

        function tw_multi_upload_image(){
            if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'tw_backend_ajax_nonce')) {
                $image_url = sanitize_text_field( $_POST[ 'image_url' ]);
                $image_id = sanitize_text_field( $_POST[ 'image_id' ]);
                $key = sanitize_text_field( $_POST[ 'key' ]);
                $type = sanitize_text_field( $_POST[ 'type' ]);
                include_once(TW_PATH . '/inc/admin/metabox/ajax/upload-pictures.php');
                die();
            }
        }

        function tw_multi_upload_file(){
            if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'tw_backend_ajax_nonce')) {
                $file_url = sanitize_text_field( $_POST[ 'file_url' ] );
                $file_id = sanitize_text_field( $_POST[ 'file_id' ] );
                $file_name =sanitize_text_field( $_POST[ 'file_name' ] );
                $key = sanitize_text_field( $_POST[ 'key' ] );
                $type = sanitize_text_field( $_POST[ 'type' ] );
                include_once(TW_PATH . '/inc/admin/metabox/ajax/upload-downloadable-files.php');
                die();
            }
        }

        function tw_selected_taxonomy_terms(){
            if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'tw_backend_ajax_nonce')) {
                global $wpdb;
                include_once(TW_PATH . '/inc/admin/metabox/ajax/fetch-terms.php');
                die();
            }
        }

        function tw_search_product(){
            if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'tw_backend_ajax_nonce')) {
                $search = sanitize_text_field( $_POST[ 'search' ] );
                $exclude = $this->tw_lib->tw_sanitize_array( $_POST[ 'exclude' ] );
                include_once(TW_PATH . '/inc/admin/metabox/ajax/search-product.php');
                die();
            }
        }
    }
    new TW_Admin_Ajax();
}
