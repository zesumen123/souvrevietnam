<?php

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2017 Buttonizer
*/
namespace Buttonizer\Licensing;

# No script kiddies
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
class obFsNull {
    public function is__premium_only() {
        return true;
    }
    public function can_use_premium_code() {
        return true;
    }
    public function is_anonymous() {
        return true;
    }
    public function is_activation_mode() {
        return false;
    }
    public function get_reconnect_url() {
        return '';
    }
}
class License
{
    private  $oButtonizer = 'null' ;
    public function init()
    {
        
        if ( $this->oButtonizer == 'null' ) {
            require_once BUTTONIZER_DIR . '/freemius/start.php';
            // Some data for Buttonizer to be freemium and paid.
            // We are paid so we can maintain the plugin
            // If you don't want to pay for the plugin, you can allways use the
            $this->oButtonizer = new obFsNull();
        }
        
        return;
    }
    
    public function get()
    {
        return $this->oButtonizer;
    }
    
    private function getDaysLeft()
    {
        return round( 2000 / 8 - 243 );
    }
    
    // Default data
    private function initButtonizerData()
    {
    }

}