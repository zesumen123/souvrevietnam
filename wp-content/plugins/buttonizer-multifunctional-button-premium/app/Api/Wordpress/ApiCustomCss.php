<?php

namespace Buttonizer\Api\Wordpress;

use Buttonizer\Utils\Maintain;

/**
 * CustomCss API
 * 
 * @endpoint /wp-json/buttonizer/wordpress/custom_css
 * @methods GET POST
 */
class ApiCustomCss {
    /**
     * Register route
     */
    public function registerRoute() {
        register_rest_route('buttonizer', '/wordpress/custom_css', [
            [
                'methods'  => ['GET'],
                'args' => [
                    'nonce' => [
                        'validate_callback' => function($value) {
                            return wp_verify_nonce($value, 'wp_rest');
                        },
                        'required' => true
                    ]
                ],
                'callback' => [$this, 'get'],
                'permission_callback' => function() {
                    return Maintain::hasPermission();
                }
            ],
            [
                'methods'  => ['POST'],
                'args' => [
                    'data' => [
                        'required' => true,
                        'type' => "object"
                    ],
                    'nonce' => [
                        'validate_callback' => function($value) {
                            return wp_verify_nonce($value, 'wp_rest');
                        },
                        'required' => true
                    ],
                ],
                'callback' => [$this, 'post'],
                'permission_callback' => function() {
                    return Maintain::hasPermission();
                }
            ]
        ]);
    }

    /**
     * Get time schedules
     */
    public function get() {
        if(ButtonizerLicense()->is__premium_only()) {
            if (ButtonizerLicense()->can_use_premium_code()) {
              return [
                'status' => 'success',
                'data' => wp_get_custom_css()
              ];
            }else{
                return \Buttonizer\Api\Api::needButtonizerPremium();
            }
        }else{
            return \Buttonizer\Api\Api::needButtonizerPremium();
        }
    }

    /**
     * Save time schedule data
     */
    public function post($request) {
        if(ButtonizerLicense()->is__premium_only()) {
            if (ButtonizerLicense()->can_use_premium_code()) {

                $r = wp_update_custom_css_post($request->get_param('data')['styles']);

                if ($r instanceof WP_Error ) {
                    return [
                        'status' => 'error',
                        'message' => $r,
                        'data' => $request->get_param('data')
                    ];
                }
                return [
                    'status' => 'success',
                    'data' => $request->get_param('data')
                ];
            }else{
                return \Buttonizer\Api\Api::needButtonizerPremium();
            }
        }else{
            return \Buttonizer\Api\Api::needButtonizerPremium();
        }
    }
}