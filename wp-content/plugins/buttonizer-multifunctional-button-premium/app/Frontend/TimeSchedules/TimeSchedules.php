<?php

namespace Buttonizer\Frontend\TimeSchedules;

use Buttonizer\Frontend\Buttonizer;
use Buttonizer\Frontend\TimeSchedules\Schedule\Schedule;
use Buttonizer\Utils\Maintain;

class TimeSchedules
{
    private $timeSchedulesData;
    private $timeSchedules;

    private static $timezone = null;
    private $hour = null;
    private $minute = null;

    private static $timestampToday = null;
    private static $dayOfWeek = null;
    private static $currentHour = null;
    private static $currentMinutes = null;

    private static $currentDate = null;

    /**
     * TimeSchedules constructor.
     */
    public function __construct()
    {
        // Set timezone
        self::$timezone = new \DateTimeZone(Maintain::getTimezone());

        // Get time scchedules data
        $this->timeSchedulesData = get_option(Buttonizer::getSettingName('buttonizer_schedules'));

        // Initialize day and time
        $this->init();
    }

    /**
     * Initialize day and time
     */
    private function init()
    {
        if(!is_array($this->timeSchedulesData) || count($this->timeSchedulesData) < 0) {
            return;
        }

        // Get correct timezone
        $currentTime = new \DateTime("G", self::$timezone);

        // Set day of week
        self::$dayOfWeek = $this->returnDay($currentTime->format('N'));

        // Set current time
        self::$currentHour = $currentTime->format("G");
        self::$currentMinutes = $currentTime->format("i");
        self::$currentDate = $currentTime->format("Y-m-d");
        self::$timestampToday = strtotime(self::$currentDate . ' 00:00:00');

        // For each time schedule
        foreach($this->timeSchedulesData as $schedule)
        {
            $this->timeSchedules[$schedule['id']] = new Schedule($schedule);
        }
    }

    /**
     * Get time schedule by ID
     *
     * @param $id
     * @return null
     */
    public function getSchedule($id)
    {
        return (isset($this->timeSchedules[$id]) ? $this->timeSchedules[$id] : null);
    }

    /**
     * Day-number to text
     *
     * @param $dayNumber
     * @return string
     */
    private function returnDay($dayNumber) {
        switch($dayNumber) {
            case 1:
                return 'monday';
                break;
            case 2:
                return 'tuesday';
                break;
            case 3:
                return 'wednesday';
                break;
            case 4:
                return 'thursday';
                break;
            case 5:
                return 'friday';
                break;
            case 6:
                return 'saturday';
                break;
            case 7:
                return 'sunday';
                break;
            default:
                return 'sunday';
                break;
        }
    }

    /**
     * Get weekday
     *
     * @return string
     */
    public static function getDay()
    {
        return self::$dayOfWeek;
    }

    /**
     * Get hour
     *
     * @return string
     */
    public static function getHour()
    {
        return self::$currentHour;
    }

    /**
     * Get minutes
     *
     * @return string
     */
    public static function getMinutes()
    {
        return self::$currentMinutes;
    }

    /**
     * Get current timezone
     *
     * @return string
     */
    public static function getTimezone()
    {
        return self::$timezone;
    }

    /**
     * Get current date (starting from 00:00 today)
     *
     * @return \DateTime|null
     */
    public static function getDate()
    {
        return self::$currentDate;
    }

    /**
     * @return false|int
     */
    public static function getTimestamp()
    {
        return self::$timestampToday;
    }
}