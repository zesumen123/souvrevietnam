<?php
namespace Buttonizer\Frontend\TimeSchedules\Schedule;
use Buttonizer\Frontend\TimeSchedules\TimeSchedules;

/**
 * Class Schedule
 * This file is a part of Buttonizer
 * Please do not redistribute without any written commitment
 *
 * Copyrights 2019 www.buttonizer.pro
 */
class Schedule
{
    private $calculated = false;
    private $show = false;
    private $data = [];

    /**
     * Schedule constructor.
     * @param $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Do we need to show this button?
     *
     * @return bool
     */
    public function calculate()
    {
        // Cache the calculated
        if($this->calculated) {
            return $this->show;
        }

        // Awesome, a start date has been set up
        if(isset($this->data["start_date"]))
        {
            // Oh no... We didn't reach the start date yet... STOOOOPPP!!!!11!
            if(strtotime($this->data["start_date"] . ' 00:00:00') > TimeSchedules::getTimestamp()) {
                $this->closed();
                return $this->show;
            }
        }

        // Awesome, a end date has been set up
        if(isset($this->data["end_date"]) && $this->data["end_date"] != 'false')
        {
            // Oh no... We did reach the end date!! STOOOOPPP!!!!11!
            if(TimeSchedules::getTimestamp() > strtotime($this->data["end_date"] . ' 00:00:00')) {
                $this->closed();
                return $this->show;
            }
        }

        // First check weekdays
        $this->checkWeekdays();

        // Check custom dates
        if(isset($this->data['dates']))
        {
            $this->checkDates();
        }

        return $this->show;
    }

    /**
     * We're open
     */
    private function open()
    {
        $this->calculated = true;
        $this->show = true;
        return true;
    }

    /**
     * We're closed
     */
    private function closed()
    {
        $this->calculated = true;
        $this->show = false;
        return false;
    }

    /**
     * Check the weekdays
     */
    private function checkWeekdays()
    {
        // Not even one day open, it doesn't even exists
        if(!isset($this->data['weekdays']) || count($this->data['weekdays']) === 0) {
            $this->closed();
            return;
        }

        foreach ($this->data['weekdays'] as $weekday)
        {
            // This is today
            if($weekday['weekday'] === TimeSchedules::getDay())
            {
                // Opened? NOPE!
                if(!isset($weekday['opened']) || filter_var($weekday['opened'], FILTER_VALIDATE_BOOLEAN, ['options' => ['default' => false ]]) === false)
                {
                    $this->closed();
                    return;
                }

                if($this->timeChecker($weekday)) {
                    $this->open();
                }else{
                    $this->closed();
                }
                return;
            }
        }
    }

    /**
     * Check custom dates that has been setup
     *
     * @return bool
     */
    private function checkDates()
    {
        foreach ($this->data['dates'] as $day) {
            if(TimeSchedules::getDate() === $day['date']) {
                // Opened? NOPE!
                if(!isset($day['opened']) || filter_var($weekday['opened'], FILTER_VALIDATE_BOOLEAN, ['options' => ['default' => false ]]) === false)
                {
                    $this->closed();
                    return true;
                }


                if($this->timeChecker($day)) {
                    $this->open();
                }else{
                    $this->closed();
                }
                return;
            }
        }

        return false;
    }

    /**
     * Get timezone + check
     *
     * @param $day
     * @return bool
     */
    public function timeChecker($day)
    {
        // Get the opening and closing time from today.
        $hourOpening = explode( ':', (isset($day['open']) ? $day['open'] : '10:00' ) );
        $hourClosing = explode( ':', (isset($day['close']) ? $day['close'] : '17:00' ) );

        // Check if the company is opened/closed
        if (TimeSchedules::getHour() < $hourOpening[0] || TimeSchedules::getHour() == $hourOpening[0] && TimeSchedules::getMinutes() < $hourOpening[1] || TimeSchedules::getHour() > $hourClosing[0] || TimeSchedules::getHour() == $hourClosing[0] && TimeSchedules::getMinutes() > $hourClosing[1] - 1) {
            return false;
        } else {
            return true;
        }
    }
}