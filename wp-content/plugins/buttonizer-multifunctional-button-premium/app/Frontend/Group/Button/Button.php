<?php
namespace Buttonizer\Frontend\Group\Button;

use Buttonizer\Frontend\Buttonizer;

class Button
{
    private $groupObject;
    private $data;

//    private $styling = StylingObject;

    /**
     * Buttons constructor.
     * @param $groupObject
     * @param $data
     */
    public function __construct($groupObject, $data)
    {
        $this->groupObject = $groupObject;
        $this->data = $data;
    }

    /**
     * Return option data
     * 
     * @param $key
     * @param $default null
     * @return string
     */
    private function getOption($key, $default = '')
    {
        return isset($this->data[$key]) ? $this->data[$key] : $default;
    }

    /**
     * Return option data as boolean
     * 
     * @param $key
     * @param $default false
     * @return boolean
     */
    public function getBoolean($key, $default = false)
    {
        return isset($this->data[$key]) ? filter_var($this->data[$key], FILTER_VALIDATE_BOOLEAN, ['options' => ['default' => false ]]) === true : $default;
    }

    /**
     * Show buttons, based on their settings
     * @return bool
     */
    public function showButton()
    {
        // Button not in use
        if(!$this->getBoolean("show_desktop") && !$this->getBoolean("show_mobile"))
        {
            Buttonizer::addEvent([
                "id" => $this->getOption('id', null),
                "group_id" => $this->groupObject->getId(),
                "name" => $this->getOption('name', "Unnamed"),
                "button_type" => "button",
                "message" => __('The button is hidden on all devices', 'buttonizer-multifunctional-button'),
                "type" => "all_devices_hidden"
            ]);

            return false;
        }

        if(ButtonizerLicense()->is__premium_only()) {
            if (ButtonizerLicense()->can_use_premium_code()) {
                // Time schedules: We're closed. Do not show the button
                if (
                    $this->getOption("selected_schedule") != '' &&
                    $this->getOption("selected_schedule") != -1
                ) {
                    if (
                        ($this->getBoolean("show_on_schedule_trigger", true) !== false && !Buttonizer::isOpened($this->getOption("selected_schedule"))) ||
                        ($this->getBoolean("show_on_schedule_trigger", true) === false && Buttonizer::isOpened($this->getOption("selected_schedule")))
                    ) {
                        Buttonizer::addEvent([
                            "id" => $this->getOption('id', null),
                            "name" => $this->getOption('name', "Unnamed"),
                            "group_id" => $this->groupObject->getId(),
                            "button_type" => "button",
                            "message" => (
                                $this->getBoolean("show_on_schedule_trigger", true) !== false ?
                                __('The button is hidden because the attached time schedule is <b>triggered</b>.', 'buttonizer-multifunctional-button') : 
                                __('The button is hidden because the attached time schedule is <b>not triggered</b> .', 'buttonizer-multifunctional-button')
                            ),
                            "type" => "time_schedule"
                        ]);

                        return false;
                    }
                }

                // Page rules: The person is not on the right page. Do not show the button
                if (
                    ($this->getOption("selected_page_rule") != '' && $this->getOption("selected_page_rule") != -1)
                ) {
                    if (
                        ($this->getBoolean("show_on_rule_trigger", true) !== false && !Buttonizer::isActive($this->getOption("selected_page_rule"))) ||
                        ($this->getBoolean("show_on_rule_trigger", true) === false && Buttonizer::isActive($this->getOption("selected_page_rule")))
                    ) {
                        Buttonizer::addEvent([
                            "id" => $this->getOption('id', null),
                            "name" => $this->getOption('name', "Unnamed"),
                            "group_id" => $this->groupObject->getId(),
                            "button_type" => "button",
                            "message" => (
                                $this->getBoolean("show_on_rule_trigger", true) !== false ?
                                __('The button is hidden because the attached page rule is <b>triggered</b>.', 'buttonizer-multifunctional-button') : 
                                __('The button is hidden because the attached page rule is <b>not triggered</b> .', 'buttonizer-multifunctional-button')
                            ),
                            "type" => "page_rule"
                        ]);

                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function generate()
    {
        return $this->data;
    }
}