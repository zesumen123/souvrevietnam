<?php
namespace Buttonizer\Frontend;

use Buttonizer\Utils\Maintain;
use Buttonizer\Frontend\Group\Button\Button;
use Buttonizer\Frontend\Group\Group;
use Buttonizer\Frontend\PageRules\PageRules;
use Buttonizer\Frontend\TimeSchedules\TimeSchedules;

class Buttonizer
{
    private $buttonGroups;
    
    private $groupReturns = [];

    // Time schedule object
    private static $timeSchedules = null;
    private static $pageRules = null;

    private static $logs = [];
    private static $pageData = null;

    // Current page
    private static $currentPage = -1;
    private static $currentBlogPost = -1;
    private static $currentCategories = [];
    private static $currentUrl = '';
    private static $currentPageTitle = '';
    private static $currentPageIs404 = false;
    private static $currentPageIsFrontPage = false;
    private static $currentUserRoles = '';

    private static $published = '_published';

    /**
     * Buttons constructor.
     */
    public function __construct($noAjax = false)
    {
        if(isset($_GET['preview']) && $_GET['preview'] === '1' && Maintain::hasPermission()) self::$published = '';

        // Get groups
        $this->buttonGroups = get_option(self::getSettingName('buttonizer_buttons'));

        if(ButtonizerLicense()->is__premium_only()) {
            if (ButtonizerLicense()->can_use_premium_code()) {
                if($noAjax) {
                    $this->getData__premium_only();
                }else{
                    $this->getDataAjax__premium_only();
                }

                self::$timeSchedules = new TimeSchedules();
                self::$pageRules = new PageRules();
            }
        }

        if(ButtonizerLicense()->is__premium_only()) {
            if (ButtonizerLicense()->can_use_premium_code()) {
                // Create groups
                foreach ($this->buttonGroups as $buttonGroup) {
                    $this->createGroup($buttonGroup);
                }
            }else{
                $this->createGroup($this->buttonGroups[0]);
            }
        }else{
            $this->createGroup($this->buttonGroups[0]);
        }
    }

    /**
     * Get current page data
     */
    private function getData__premium_only() {
        global $wp;

        // Get page ID
        self::$currentPage = get_the_ID();

        // Get page categories
        $categories = [];
        foreach (get_the_category() as $category) {
            $categories[] = $category->cat_ID;
        }

        self::$currentCategories = $categories;

        // Current blog post ID
        self::$currentBlogPost = get_current_blog_id();

        // Current url
        self::$currentUrl = $_SERVER['REQUEST_URI'];

        // 
        self::$currentPageTitle = get_the_title();

        // 
        self::$currentPageIs404 = is_404();

        // f
        self::$currentPageIsFrontPage = is_front_page();
            
    }

    /**
     * Get data of page/post/category via ajax
     */
    private function getDataAjax__premium_only()
    {
        // Data not set
        if(!isset($_GET['data']) || !is_array($_GET['data']) || count($_GET['data']) === 0)
        {
            self::addWarning('Warning: Missing data parameter (on this page). It won\'t show buttons on specific conditions if you have page rules set up for this page');
            return;
        }

        $pageData = $_GET['data'];

        // Page
        if(isset($pageData['page']) && is_numeric($pageData['page']))
        {
            self::$currentPage = $pageData['page'];
        }

        // Blog categories
        if(isset($pageData['categories']) && is_array($pageData['categories']))
        {
            self::$currentCategories = $pageData['categories'];
        }

        // Current blog post
        if(isset($pageData['blog']) && is_numeric($pageData['blog']))
        {
            self::$currentBlogPost = $pageData['blog'];
        }

        // Current page URL
        if(isset($pageData['url']))
        {
            self::$currentUrl = $pageData['url'];
        }

        // Current page title
        if(isset($pageData['title']))
        {
            self::$currentPageTitle = $pageData['title'];
        }

        // Is this page an 404?
        if(isset($pageData['is_404']) && is_bool($pageData['is_404']))
        {
            self::$currentPageIs404 = $pageData['is_404'] == 'true';
        }

        // Is this the front page?
        if(isset($pageData['is_front_page']) && is_bool($pageData['is_front_page']))
        {
            self::$currentPageIsFrontPage = $pageData['is_front_page'] == 'true';
        }
    }

    /**
     * @param $data
     */
    private function createGroup($data)
    {
        $group = new Group($data['data']);

        // Add buttons to group
        foreach ($data['buttons'] as $button) {
            $group->add(new Button($group, $button));
        }

        if ($group->show()) {
            $this->groupReturns[] = $group->fix();
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public static function isOpened($id)
    {
        if (ButtonizerLicense()->is__premium_only()) {
            if (ButtonizerLicense()->can_use_premium_code()) {
                if(self::$timeSchedules->getSchedule($id) !== null) {
                    return self::$timeSchedules->getSchedule($id)->calculate();
                }else {
                    return false; // Schedule not found!
                }
            }
        }

        return true;
    }

    /**
     * Page rules
     *
     * @param $id
     * @return bool
     */
    public static function isActive($id)
    {
        if (ButtonizerLicense()->is__premium_only()) {
            if (ButtonizerLicense()->can_use_premium_code()) {
                if(self::$pageRules->getRule($id) !== null) {
                    return self::$pageRules->getRule($id)->calculate();
                }
            }
        }
        return true;
    }

    /**
     * Returns array
     *
     * @return array
     */
    public function returnArray()
    {
        return $this->groupReturns;
    }

    /**
     * @param $message
     * @return bool
     */
    public static function addWarning($message)
    {
        self::$logs[] = [
            'type' => 'warning',
            'message' => $message
        ];
        return true;
    }

    /**
     * @param array $message
     * @return bool
     */
    public static function addEvent($messageData)
    {
        self::$logs[] = $messageData;
        return true;
    }

    /**
     * Get setting name
     *
     * @param string $string
     * @return string
     */
    public static function getSettingName($string = '')
    {
        return $string . self::$published;
    }

    /**
     * Is the user in preview mode?
     * @return boolean
     */
    public static function isPreview()
    {
        return self::$published === "";
    }

    /**********************************
     * Current page data
     */

    /**
     * @return array
     */
    public static function getLogs()
    {
        // Not in preview
        if(!self::isPreview()) return [];

        return self::$logs;
    }

    /**
     * Get current page url
     * @param bool $baseUrl
     * @return string
     */
    public static function getUrl($baseUrl = true)
    {
        if($baseUrl === false)
        {
            return str_replace(get_site_url(), "", self::$currentUrl);
        }
        return self::$currentUrl;
    }

    /**
     * Get current page
     */
    public static function getPage()
    {
        return self::$currentPage;
    }

    /**
     * Get current post
     */
    public static function getBlogPost()
    {
        return self::$currentBlogPost;
    }

    /**
     * Get current post categories
     */
    public static function getCategories()
    {
        return self::$currentCategories;
    }

    /**
     * Is the current page a front page?
     */
    public static function getTitle()
    {
        return strtolower(self::$currentPageTitle);
    }

    /**
     * Is the current page a 404?
     */
    public static function is_404()
    {
        return self::$currentPageIs404;
    }

    /**
     * Is the current page a front page?
     */
    public static function isFrontPage()
    {
        return self::$currentPageIsFrontPage;
    }

    /**
     * Return the user role of the current user
     */
    public static function getUserRoles()
    {
        if(self::$currentUserRoles === "") {
            $userRoles = get_userdata(get_current_user_id())->roles;

            // If not logged in, add guest role in roles
            if(!is_user_logged_in()) $userRoles[] = "guest";

            self::$currentUserRoles = $userRoles;
        }

        return self::$currentUserRoles;
    }
}