<?php

namespace Buttonizer\Frontend\PageRules;

use Buttonizer\Frontend\Buttonizer;
use Buttonizer\Frontend\PageRules\Rule\Rule;

class PageRules
{
    private $pageRulesData;
    private $pageRules;

    /**
     * PageRules constructor.
     */
    public function __construct()
    {
        // Get page rules data
        $this->pageRulesData = get_option(Buttonizer::getSettingName('buttonizer_rules'));

        // Initialize day and time
        $this->init();
    }

    /**
     * Initialize page rules
     */
    private function init()
    {
        if(!is_array($this->pageRulesData) || count($this->pageRulesData) === 0) {
            return;
        }

        foreach($this->pageRulesData as $schedule)
        {
            $this->pageRules[$schedule['id']] = new Rule($schedule);
        }
    }

    /**
     * Get page rule by ID
     *
     * @param $id
     * @return null
     */
    public function getRule($id)
    {
        return (isset($this->pageRules[$id]) ? $this->pageRules[$id] : null);
    }
}