<?php
/*
* Plugin Name: Buttonizer - Smart Floating Action Button (Premium)
* Plugin URI:  https://buttonizer.pro
* Description: The Buttonizer is a new way to give a boost to your number of interactions, actions and conversions from your website visitor by adding one or multiple Customizable Smart Floating Button in the corner of your website.
* Version:     2.2.1
* Author:      Buttonizer
* Author URI:  https://buttonizer.pro
* License:     GPL2
* License URI: https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: buttonizer-multifunctional-button
* Domain Path: /languages
*
* @fs_premium_only /src/, /app/Frontend/TimeSchedules/, /app/Frontend/PageRules/, /assets/dashboard__premium_only.js, /assets/dashboard__premium_only.css, /assets/frontend__premium_only.js, /assets/frontend__premium_only.css
*/
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2017 Buttonizer
*/

define('BUTTONIZER_NAME', 'buttonizer');
define('BUTTONIZER_DIR', dirname(__FILE__));
define('BUTTONIZER_SLUG', basename(BUTTONIZER_DIR));
define('BUTTONIZER_PLUGIN_DIR', __FILE__ );
define('BUTTONIZER_VERSION', '2.2.1');
define('BUTTONIZER_DEBUG', false);
define('BUTTONIZER_LAST_MIGRATION', 4);

define('FONTAWESOME_CURRENT_VERSION', 'v5.14.0');
define('FONTAWESOME_CURRENT_INTEGRITY', 'sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay');

# No script kiddies
defined( 'ABSPATH' ) or die('No script kiddies please!');

/* ================================================
 *     WELCOME TO THE BUTTONIZER SOURCE CODE!
 *
 *      We like to see that you are courious
 *        how the code is written. When you
 *       are here to try to resolve problems
 *        you must be careful, anything
 *          can get broken you know...
 *
 *            -- KNOWLEDGE BASE --
 *        Did you know you can use our
 *              knowledge base?
 *               That's free!
 *
 *				     VISIT:
 * https://community.buttonizer.pro/knowledgebase
 *
 *             -- BUGS FOUND? --
 *	    Are you here to look for a bug?
 *		 Cool! If you found something
 *         you can report it to us!
 *
 *       Maybe you get a FREE license
 *            for 1 website ;)
 *
 * ================================================
 */

// DEBUG ONLY!!
if(BUTTONIZER_DEBUG) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

/**
 * Autoloader
 */
spl_autoload_register(function ($class_name)
{
    try {
        if(substr($class_name, 0, 10) === 'Buttonizer')
        {
            $class_name = substr($class_name, 10) ;

            require BUTTONIZER_DIR .  '/app' . str_replace("\\", "/", $class_name) . '.php';
        }
    }
    catch (\Exception $e) {
        exit("Error: " . $e->getMessage());
    }
});

// Fixes bug with multiple buttonizer installs
if(!defined('BUTTONIZER_DEFINED')) {
    /*
    * License setup
    */
    $oButtonizer = new Buttonizer\Licensing\License();
    $oButtonizer->init();

    if(!function_exists("ButtonizerLicense")) {
        function ButtonizerLicense() {
            global $oButtonizer;

            return $oButtonizer->get();
        }
    }

    /*
    * Installation, removing and initiallization
    */
    $oButtonizerMaintain = new Buttonizer\Utils\Maintain(true);

    /*
    * Buttonizer Admin Dashboard
    */
    if (is_admin()) {
        // Load Admin page
        new Buttonizer\Admin\Admin();
    }

    /**
     * Create Buttonizer API endpoints
     */
    add_action( 'rest_api_init', function() {
        new Buttonizer\Api\Api();
    });

    /**
     * Frontend 
     */
    new Buttonizer\Frontend\Ajax();

    // Localization
    add_action('init', 'buttonizer_load_plugin_textdomain');
    
    function buttonizer_load_plugin_textdomain() {
        load_plugin_textdomain( 'buttonizer-multifunctional-button', false, dirname(plugin_basename(__FILE__)) . '/languages' ); 
    }

    // System, buttonizer is loaded
    do_action('buttonizer_loaded');

    // Ok, define
    define('BUTTONIZER_DEFINED','1.0');
}