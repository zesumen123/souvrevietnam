<?php

/* Template Name: PRODUCT PAGE */

get_header();

?>
<?php
$taxonomy     = 'product_cat';
$orderby      = 'slug';
$show_count   = 0;      // 1 for yes, 0 for no
$pad_counts   = 0;      // 1 for yes, 0 for no
$hierarchical = 1;      // 1 for yes, 0 for no  
$title        = '';
$empty        = 0;

$args = array(
	'taxonomy'     => $taxonomy,
	'orderby'      => $orderby,
	'show_count'   => $show_count,
	'pad_counts'   => $pad_counts,
	'hierarchical' => $hierarchical,
	'title_li'     => $title,
	'hide_empty'   => $empty
);
$all_categories = get_categories($args);
?>

<form class="invisible" id="searchform" method="get" action="<?php echo esc_url(home_url('/')); ?>">
	<input type="text" class="search-field" name="s" placeholder="Search" value="<?php echo get_search_query(); ?>">
	<select name="cat" id="select-category">
		<?php
		foreach ($all_categories as $category) {
			$name = $category->name;
			$id = $category->slug;
			echo '<option value=' . $id . '>' . $name . '</option>';
		}
		?>
	</select>
	<input id="submit-form-button" type="submit" value="Search">
</form>
<div class="row th-search px-4">

	<div class="col-4">
		<div class="mb-4">
			<div class="th-search-title">Danh mục</div>
			<?php

			echo "<div class='d-flex flex-column'>";

			foreach ($all_categories as $cat) {
				if ($cat->category_parent == 0) {
					$category_id = $cat->term_id;
					$slug = $cat->slug;
					$args = "'" . $slug . "'";
					echo '<div onClick="handleChangeCategory(' . $args . ')" class="th-search-filter-item" href="' . get_term_link($cat->slug, 'product_cat') . '">' . $cat->name . '</div>';
				}
			}
			echo "</div>";
			?>
		</div>
		<div class="col-8 p-0 mt-4">
			<div>
				<div class="th-search-title">Hot Deals </div>
				<div class="search-slider">
					<?php
					$args = array(
						'post_type'      => 'product',
						'posts_per_page' => 10,

					);

					$query = new WP_Query($args);
					?>
					<?php
					if ($query->have_posts()) :
					?>
					<?php
						// Start the Loop.
						while ($query->have_posts()) :
							$query->the_post();
							echo "<div>";
							get_template_part('template-parts/product/single',  $query->get_post_format());
							echo "</div>";
						endwhile;
					endif;
					?>
				</div>
			</div>

		</div>
	</div>
	<div class="col-8">
		<div class="row">

			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$cat_query = $_GET['cat'];
			$args = array(
				'post_type'      => 'product',
				'posts_per_page' => 3,
				'paged' => $paged,
				'post_status' => 'publish',
				'orderby'     => 'title',
				'order'       => 'ASC',
				'cc_search_post_title' => get_search_query(),
				'product_cat' => $cat_query
			);


			add_filter('posts_where', 'cc_post_title_filter', 10, 2);
			$query = new WP_Query($args);
			remove_filter('posts_where', 'cc_post_title_filter', 10);
			?>
			<?php
			if ($query->have_posts()) :
			?>
			<?php
				// Start the Loop.
				while ($query->have_posts()) :
					$query->the_post();

					echo "<div class='col-4'>";
					get_template_part('template-parts/product/single',  $query->get_post_format());
					echo "</div>";
				endwhile;

				$total_pages = $query->max_num_pages;
				if ($total_pages > 1) {

					$current_page = max(1, get_query_var('paged'));
					echo "<div class='th-search-pagination'>";
					echo paginate_links(array(
						'base' => get_pagenum_link(1) . '%_%',
						'format' => '/page/%#%',
						'current' => $current_page,
						'total' => $total_pages,
						'prev_text'    => __('Trang trước'),
						'next_text'    => __('Trang sau'),
					));
					echo "</div>";
				}
			endif;
			?>
		</div>
	</div>

</div>
<?php
get_footer();
