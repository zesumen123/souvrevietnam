<?php

/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the page header div.
 *
 * @package Neve
 * @since   1.0.0
 */
?>
<!DOCTYPE html>
<?php

/**
 * Filters the header classes.
 *
 * @param string $header_classes Header classes.
 *
 * @since 2.3.7
 */
$header_classes = apply_filters('nv_header_classes', 'header');

/**
 * Fires before the page is rendered.
 */
do_action('neve_html_start_before');

?>
<html <?php language_attributes(); ?>>

<head>
	<?php
	/**
	 * Executes actions after the head tag is opened.
	 *
	 * @since 2.11
	 */
	do_action('neve_head_start_after');
	?>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<?php if (is_singular() && pings_open(get_queried_object())) : ?>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

	<?php
	/**
	 * Executes actions before the head tag is closed.
	 *
	 * @since 2.11
	 */
	do_action('neve_head_end_before');
	?>
</head>

<body <?php body_class(); ?> <?php neve_body_attrs(); ?>>
	<?php
	/**
	 * Executes actions after the body tag is opened.
	 *
	 * @since 2.11
	 */
	do_action('neve_body_start_after');
	?>
	<?php wp_body_open(); ?>
	<div class="wrapper">
		<?php
		/**
		 * Executes actions before the header tag is opened.
		 *
		 * @since 2.7.2
		 */
		do_action('neve_before_header_wrapper_hook');
		?>

		<header class="<?php echo esc_attr($header_classes); ?>" role="banner">
			<a class="neve-skip-link show-on-focus" href="#content" tabindex="0">
				<?php echo __('Skip to content', 'neve'); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped 
				?>
			</a>
			<?php

			/**
			 * Executes actions before the header ( navigation ) area.
			 *
			 * @since 1.0.0
			 */
			do_action('neve_before_header_hook');

			if (apply_filters('neve_filter_toggle_content_parts', true, 'header') === true) {
				do_action('neve_do_header');
			}

			/**
			 * Executes actions after the header ( navigation ) area.
			 *
			 * @since 1.0.0
			 */
			do_action('neve_after_header_hook');
			?>
		</header>

		<?php
		/**
		 * Executes actions after the header tag is closed.
		 *
		 * @since 2.7.2
		 */
		do_action('neve_after_header_wrapper_hook');
		?>


		<?php
		/**
		 * Executes actions before main tag is opened.
		 *
		 * @since 1.0.4
		 */
		do_action('neve_before_primary');
		?>

		<main id="content" class="neve-main container" role="main">
			<?php if (is_home()) {
				echo do_shortcode('[smartslider3 slider="3"]');
			} else {
				echo "<div class='th-breadcrumb'>";
				echo "<h2 class='th-breadcrumb-title'>";
				echo get_the_title();
				echo "</h2>";
				echo yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
				echo "</div>";
			}
			?>


			<?php
			/**
			 * Executes actions after main tag is opened.
			 *
			 * @since 1.0.4
			 */
			do_action('neve_after_primary_start');
