<div class="card-blog">
    <a href="<?php echo get_post_permalink(get_the_Id()); ?>">
        <img class="card-img-top" src="<?php
                                        echo get_the_post_thumbnail_url(get_the_ID(), 'medium');
                                        ?>" alt="Card image cap">
    </a>
    <div class="card-content">
        <a href="<?php echo get_post_permalink(get_the_Id()); ?>">
            <h2>
                <?php
                echo get_the_title();
                ?>
            </h2>
        </a>
       
            <p class="<?php (is_home() || is_single()) ? '' : 'd-none d-lg-block' ?>" style="margin-top : 10px !important">
                <?php
                echo plPostExcerpt(
                    get_post(),
                    25
                );
                ?>
            </p>
      
    </div>
</div>