<div class="align-items-center product-item-container">
    <?php
		global $product;
		$pid = $product->get_id();
		$rating_count = $product->get_rating_count();
		$review_count = $product->get_review_count();
		$average      = $product->get_average_rating();
	?>
	
	<?php
    echo '<a class="d-flex flex-column align-items-center w-100 product-item-content" href="' . get_permalink() . '">
    <div class="product-item-image">' . woocommerce_get_product_thumbnail() . '</div    > 
    <div class="product-item-title"> ' . get_the_title() . '</div>
    <div class="product-item-price"> VNĐ ' . $product->price . '</div>
    </a>';
    ?>
	
	<div class="woocommerce-product-rating">
	<?php echo wc_get_rating_html( $average, $rating_count ); // WPCS: XSS ok. ?>
	</div>

	<a href="<?php echo do_shortcode( '[add_to_cart_url id=' . $pid . ']' ) ?>" class="product-btn d-flex flex-column align-items-center">Mua ngay</a>
	
</div>
