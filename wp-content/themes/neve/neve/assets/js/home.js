$(document).ready(function () {
  $(".home-slider").slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: true,
  });
  $(".search-slider").slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
  });

  const interval = setInterval(() => {
    const galey = document.querySelector(".html5gallery-elem-img-1 a");
    if (galey) {
      galey.style.display = "none";
      clearInterval(interval);
    }
  }, 100);
});

function handleChangeCategory(slug) {
  const selectDom = $("#select-category");
  const submitButton = document.getElementById("submit-form-button");
  selectDom.val(slug).change();
  submitButton.click();
}
