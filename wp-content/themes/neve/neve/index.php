<?php

/**
 * Index template.
 *
 * @package Neve
 */

/**
 * Filters the container classes.
 *
 * @param string $classes Container classes.
 *
 * @since 1.0.0
 */
$container_class = apply_filters('neve_container_class_filter', 'container', 'blog-archive');

get_header();

?>
<div class="<?php echo esc_attr($container_class); ?> archive-container">
    <div class="row flex-wrap">
        <div class="col-12">
            <div class="my-4">
                <p class="text-center th-section-sub-title">Nature’s Touch</p>
                <h2 class="text-center th-section-title">BEAUTY ESSENTIALS</h2>
            </div>
            <div class="row">
                <?php
				$args = array(
					'post_type'      => 'product',
					'posts_per_page' => 10,
				);

				$query = new WP_Query($args);
				?>
                <?php
				if ($query->have_posts()) :
				?>
                <?php
					// Start the Loop.
					while ($query->have_posts()) :
						$query->the_post();
						echo "<div class='col-md-4'>";
						get_template_part('template-parts/product/single',  $query->get_post_format());
						echo "</div>";
					endwhile;
				endif;
				?>
            </div>

        </div>
        <div class="col-12">
            <div class="mb-4" style="margin-top : 5em">
                <p class="text-center th-section-sub-title">Define Beauty</p>
                <h2 class="text-center  th-section-title">VIDEO NỔI BẬT</h2>
            </div>
            <div style="margin-bottom : 5em">
                <?php 
				echo do_shortcode("[new_royalslider id='1']"); 
				?>
            </div>
        </div>
        <div class="col-12">
            <div class="top-3-collection-item">
                <div class="row align-items-center">
                    <div class="col-sm-7 col-lg-6">
                        <div class="sec-header text-center">
                            <p class="sec-header-small">Waken Your Skin</p>
                            <h2 class="sec-header-title"><a href="">3 STEP MASK</a></h2>
                        </div>
                        <div class="sec-content">
                            <p class="top-3-collection-item-text">Với những thành phần được chiết xuất từ quả lựu, dâu
                                tây kết hợp với lá chè, hoa cúc La Mã, sẽ giúp da mặt của bạn bừng tỉnh.</p>
                            <a class="top-3-collection-item-price" href="#"><span class="btn-icon-right">TÌM HIỂU THÊM<i
                                        class="fas fa-long-arrow-alt-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-5 col-lg-6" href='#'><span to="/collection/123123"
                            class="top-3-collection-item-img"
                            style="background-image: url(https://i.imgur.com/8LsPn1Q.png);"></span></div>
                </div>
            </div>
            <div class="top-3-collection-item">
                <div class="row align-items-center">
                    <div class="col-sm-7 col-lg-6">
                        <div class="sec-header text-center">
                            <p class="sec-header-small">Make Your Skin Timeless</p>
                            <h2 class="sec-header-title"><a href="#">LIFT CAPSULE</a></h2>
                        </div>
                        <div class="sec-content">
                            <p class="top-3-collection-item-text">Viên nang nâng cơ chống lão hóa Timeless<br>Thành phần
                                hoạt chất chống lão hóa Argireline® chống lại các nguyên nhân cấu trúc dẫn đến lão hóa
                                da một cách bền vững.</p>
                            <a class="top-3-collection-item-price" href=""><span class="btn-icon-right">TÌM HIỂU THÊM<i
                                        class="fas fa-long-arrow-alt-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-5 col-lg-6" href='#'><span class="top-3-collection-item-img"
                            style="background-image: url(https://i.imgur.com/jWTuVLQ.png);"></span></div>
                </div>
            </div>
            <div class="top-3-collection-item">
                <div class="row align-items-center">
                    <div class="col-sm-7 col-lg-6">
                        <div class="sec-header text-center">
                            <p class="sec-header-small">Comprehensive Care</p>
                            <h2 class="sec-header-title"><a href="#">PHYTOCOMPLEX TIMELESS SET</a></h2>
                        </div>
                        <div class="sec-content">
                            <p class="top-3-collection-item-text">
                                Bộ sản phẩm chăm sóc da chuyên sâu<br>Chống lão hóa,
                                chống nếp nhăn, khởi tạo tế bào mới từ bộ sản phẩm Phytocomplex, làn da của bạn sẽ được
                                chăm sóc một cách kỹ càng.</p>
                            <a class="top-3-collection-item-price" href='#'><span class="btn-icon-right">TÌM HIỂU THÊM<i
                                        class="fas fa-long-arrow-alt-right"></i>
                            </a>
                        </div>

                    </div>
                    <div class="col-sm-5 col-lg-6"><span class="top-3-collection-item-img"
                            style="background-image: url(https://i.imgur.com/Pgi0rPm.png);"></span></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="th-home-introduce d-flex justify-content-center align-items-center flex-column">
                <p class="text-center th-section-sub-title">Timeless Product. Timeless Beauty</p>
                <h2 class="th-section-title">Precious Substances</h2>
                <p class="my-4 text-center">Our products are of high quality, precious and exquisite. You‘ll never find
                    anything synthetic or toxic. We follow a strict methodology in sourcing and using the ingredients in
                    our products.</p>
                <a class="th-home-introduce-link">
                    Các sản phẩm của SOUVRE
                </a>

            </div>
        </div>

        <div class="col-12">
            <div class="my-4">
                <p class="text-center th-section-sub-title">Reveal the beauty within yourself!</p>
                <h2 class="text-center  th-section-title">Sản phẩm mới nhất</h2>
            </div>
            <div class="row">
                <?php
				$args = array(
					'post_type'      => 'product',
					'posts_per_page' => 10,
				);

				$query = new WP_Query($args);
				?>
                <?php
				if ($query->have_posts()) :
				?>
                <?php
					// Start the Loop.
					while ($query->have_posts()) :
						$query->the_post();
						echo "<div class='col-md-3'>";
						get_template_part('template-parts/product/single',  $query->get_post_format());
						echo "</div>";
					endwhile;
				endif;
				?>
            </div>
        </div>
        <div class="col-12" style="margin : 5em 0em">
            <div>
                <p class="text-center th-section-sub-title">Souvre Feed</p>
                <h2 class="text-center  th-section-title">TIN TỨC MỚI</h2>
            </div>
            <div>
                <div class="home-slider">
                    <?php
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'orderby' => 'post_modified',
                'order' => 'DESC',
                'posts_per_page' => 10,
            );
            $query = new WP_Query($args);
            ?>

                    <?php
            if ($query->have_posts()) :
            ?>
                    <?php
                // Start the Loop.
                while ($query->have_posts()) :
                    $query->the_post();
                    echo "<div>";
                    get_template_part('template-parts/posts/single',  $query->get_post_format());
                    echo "</div>";
                endwhile;
            endif;
            ?>
                </div>

            </div>
        </div>
    </div>
</div>
<?php
get_footer();