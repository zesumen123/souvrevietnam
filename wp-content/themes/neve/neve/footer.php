<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the "wrapper" div and all content after.
 *
 * @package Neve
 * @since   1.0.0
 */

/**
 * Executes actions before main tag is closed.
 *
 * @since 1.0.4
 */
do_action('neve_before_primary_end'); ?>

</main>
<!--/.neve-main-->
<div class="th-footer">

	<div class="footer-feature-wrapper">
		<div class="container">
			<div class="footer-features">
				<div class="row">
					<div class="col-sm-3">
						<div class="d-flex flex-row justify-content-between align-items-center mb-4 mb-sm-0">
							<div class="footer-feature-icon"><i class="fas fa-truck"></i></div>
							<div class="footer-feature-info">
								<h6>Vận chuyển</h6>
								<p>Toàn quốc</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="d-flex flex-row justify-content-between align-items-center mb-4 mb-sm-0">
							<div class="footer-feature-icon"><i class="fas fa-envelope"></i></i></div>
							<div class="footer-feature-info">
								<h6>Email</h6>
								<p>souvrevietnam@gmail.com</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="d-flex flex-row justify-content-between align-items-center mb-4 mb-sm-0">
							<div class="footer-feature-icon"><i class="fas fa-headset"></i></div>
							<div class="footer-feature-info">
								<h6>24/7</h6>
								<p>Hỗ trợ khách hàng</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="d-flex flex-row justify-content-between align-items-center">
							<div class="footer-feature-icon"><i class="fas fa-undo"></i></i></div>
							<div class="footer-feature-info">
								<h6>Hoàn trả</h6>
								<p>và Đổi hàng</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php

	/**
	 * Executes actions after main tag is closed.
	 *
	 * @since 1.0.4
	 */
	do_action('neve_after_primary');

	/**
	 * Filters the content parts.
	 *
	 * @since 1.0.9
	 *
	 * @param bool   $status Whether the component should be displayed or not.
	 * @param string $context The context name.
	 */
	if (apply_filters('neve_filter_toggle_content_parts', true, 'footer') === true) {

		/**
		 * Executes actions before the footer was rendered.
		 *
		 * @since 1.0.0
		 */
		do_action('neve_before_footer_hook');

		/**
		 * Executes the rendering function for the footer.
		 *
		 * @since 1.0.0
		 */
		do_action('neve_do_footer');

		/**
		 * Executes actions after the footer was rendered.
		 *
		 * @since 1.0.0
		 */
		do_action('neve_after_footer_hook');
	}
	?>
</div>
</div>
<!--/.wrapper-->
<?php

wp_footer();

/**
 * Executes actions before the body tag is closed.
 *
 * @since 2.11
 */
do_action('neve_body_end_before');

?>
</body>

</html>