<?php /* Template Name: CONTACT PAGE */
get_header();
?>

<div class="th-beauty-contact">
    <div class="col-12">
        <div class="row">
            <div class="col-sm-3">
                <div class="contact-item">
                    <div class="contact-item-icon"><i class="fas fa-location-arrow"></i></div>
                    <div class="contact-item-title">Địa chỉ</div>
                    <div class="text-center">VP tại Hà Nội: 40C Hàng Mành, Hàng Gai, Hoàn Kiếm, Hà Nội<br>VP tại Ba Lan:
                        Bakalarska 24 , Warszawa , Ba Lan</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="contact-item">
                    <div class="contact-item-icon"><i class="fas fa-envelope"></i></div>
                    <div class="contact-item-title">Email</div>
                    <div class="text-center">souvrevietnam@gmail.com</div>

                </div>
            </div>
            <div class="col-sm-3">
                <div class="contact-item">
                    <div class="contact-item-icon"><i class="fas fa-phone"></i></div>
                    <div class="contact-item-title">Hotline</div>
                    <div class="text-center">0904908025/(+48)666666268</div>

                </div>
            </div>
            <div class="col-sm-3">
                <div class="contact-item">
                    <div class="contact-item-icon"><i class="fas fa-briefcase"></i></div>
                    <div class="contact-item-title">Giờ làm việc</div>
                    <div class="text-center">available at 08:00 - 22:00</div>

                </div>
            </div>

        </div>
    </div>
    <div class="contact-address">
        <div class="mapouter">
            <div class="gmap_canvas"><iframe width="100%" height="500" id="gmap_canvas"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3108.9945364494265!2d105.8456862906509!3d21.031481141041766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abbe23827769%3A0x98fdf4e38ef6de5b!2zNDBjIEjDoG5nIE3DoG5oLCBIw6BuZyBHYWksIEhvw6BuIEtp4bq_bSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1631208743232!5m2!1svi!2s"
                    s frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                <style>
                .mapouter {
                    position: relative;
                    text-align: right;
                    height: 600px;
                    width: 100%;
                }
                </style>
                <style>
                .gmap_canvas {
                    overflow: hidden;
                    background: none !important;
                    height: 600px;
                    width: 100%;
                }
                </style>
            </div>
        </div>
    </div>
    <div class="contact-form">
        <h3>Liên hệ với </h3>
        <?php
        echo do_shortcode('[contact-form-7 id="47" title="Form liên hệ"]');
        ?>
    </div>
</div>
<?php
get_footer()
?>