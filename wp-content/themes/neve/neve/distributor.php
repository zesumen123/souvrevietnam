<?php /* Template Name: DISTRIBUTOR PAGE */
get_header();
?>
<?php
$urlTemp = get_template_directory_uri() . '/assets/img';
?>
<div class="th-beauty-distributor">
    <h4>Trở thành đại lý</h4>
    <div class="row">
        <div class="col-8">
            <div>
                <?php
                echo do_shortcode('[contact-form-7 id="125" title="Form đối tác"]');
                ?>
            </div>
        </div>
        <div class="col-4">
            <img class="image-form" src="<?php echo $urlTemp ?>/become-distributor-form.jpg" />
        </div>
    </div>
</div>
<?php
get_footer()
?>